TIPS, DO'S and DON'TS
=====================

1. Import module specification.
   If a lib or module is in the source directories, always use fully qualified notation. This is generally not
   necessary for global site-packages. For source directory, though, we have seen many issues with naming that depends
   on sys.path, or creates localized namespaces; modules not found in the field; module-level objects not updated as
   expected; etc. For example:

   Do:
   import pygame
   from pygame import Rect
   from pygame.locals import QUIT
   from gamelib import settings
   from pyknic.mathematics import Vec3 as Vec, Point3 as Point

   Don't:
   from settings import blah
   from pyknic import mathematics

2. Local settings for debugging and development.
   See the two comments in gamelib/settings.py, one near the top marked "IMPORTANT IMPORTANT..." and one at the end of
   the file for useful details. Ask Gumm if you have questions. He designed the thing.

3. Put raw assets in the repo root directory /assets. Make subdirectories as desired. Only put assets that the game
   needs into the entry/data/ subdirectories.

4. SFX and music.

   gamelib/resource_sound.py: resource ids, sfx and music resource definitions
   gamelib/settings.py: EVT_* are the events that fire; a handler will play the sound or song
   gamelib/client.py: SfxHandler
