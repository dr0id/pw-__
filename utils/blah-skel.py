"""pygame_boilerplate.py - boilerplate for a simple game prototype"""

import pygame
from pygame.locals import *


class Settings:
    resolution = 1024, 768


class Timer(object):
    def __init__(self, interval, func):
        """a simple timer that invokes a function when the time interval elapses

        :param interval: seconds between callbacks
        :param func: function to call
        """
        self.interval = interval
        self.func = func
        self.remaining = self.interval

    def update(self, dt):
        self.remaining -= dt
        if self.remaining <= 0.0:
            self.remaining += self.interval
            self.func()


class Game(object):
    def __init__(self):
        """a basic game context with a metered run loop, caption, event handling, game update, and game rendering

        Example usage:
            pygame.init()
            pygame.display.set_mode((1024, 768))
            Game().run()
        """
        self.screen = pygame.display.get_surface()
        self.screen_rect = self.screen.get_rect()
        self.clear_color = Color('darkblue')

        self.clock = pygame.time.Clock()
        self.max_fps = 60
        self.dt = 1.0 / self.max_fps

        self.caption_timer = Timer(1.0, self.update_caption)
        self.timers = [
            Timer(1.0, self.update_caption),
        ]

        self.running = False

        self.sprites = pygame.sprite.Group()
        # self.sprites = pygame.sprite.OrderedUpdates()
        # self.sprites = pygame.sprite.LayeredUpdates()

    def run(self):
        """run loop"""
        self.running = True
        while self.running:
            self.clock.tick(self.max_fps)
            for timer in self.timers:
                timer.update(self.dt)
            self.handle_events()
            self.update(self.dt)
            self.draw()

    def update(self, dt):
        """one frame to update the game model"""
        self.sprites.update(dt)

    def draw(self):
        """one frame to render the game"""
        self.screen.fill(self.clear_color)
        self.sprites.draw(self.screen)
        pygame.display.flip()

    def update_caption(self):
        """update the caption in the title bar"""
        pygame.display.set_caption('{} fps'.format(int(self.clock.get_fps())))

    def handle_events(self):
        """handle incoming events"""
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key == K_ESCAPE:
                    self.running = False
            elif e.type == QUIT:
                self.running = False


if __name__ == '__main__':
    pygame.init()
    pygame.display.set_mode(Settings.resolution)
    Game().run()
