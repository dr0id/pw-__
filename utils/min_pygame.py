import math
import pygame
from pygame.locals import *
pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((400, 400))
screen_rect = screen.get_rect()
origin = screen_rect.center
radius = 50
angle = 0
while 1:
    clock.tick(30)
    screen.fill((0, 0, 0))
    [quit() for e in pygame.event.get() if e.type == KEYDOWN]
    x = math.cos(angle) * radius + origin[0]
    y = math.sin(angle) * radius + origin[1]
    pygame.draw.line(screen, Color('white'), origin, (x,y))
    angle = (angle + 0.1) % (2 * math.pi)
    pygame.display.flip()
