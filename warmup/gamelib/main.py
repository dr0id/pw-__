# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging

import pygame

from gamelib.context_a import ContextA

logger = logging.getLogger(__name__)


def _display_init(display_info, driver_info, wm_info):
    # frequency, mixer_size, channels, buffer_size = self._display_init_cb(display_info, driver_info, wm_info)
    pygame.display.set_mode((1024, 763))
    mixer_frequency = 0  # default:22050
    mixer_size = -16  # default: -16
    mixer_channels = 2  # default: 2 stereo or 1 mono
    mixer_buffer_size = 16  # default: 4096
    return mixer_frequency, mixer_size, mixer_channels, mixer_buffer_size


def main():
    import pyknic.pyknic_pygame
    pygame_init_context = pyknic.pyknic_pygame.context.PygameInitContext(_display_init, )
    music_player = pyknic.pyknic_pygame.sfx.MusicPlayer(1.0, 999)
    app_context = pyknic.pyknic_pygame.context.AppInitContext(music_player, "data/icon.png", "Worm up game")
    context_a = ContextA(1.0, 0.2, 30)

    pyknic.context.push(pygame_init_context)
    pyknic.context.push(app_context)
    pyknic.context.push(context_a)

    while pyknic.context.length():
        top_context = pyknic.context.top()
        top_context.update()


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
