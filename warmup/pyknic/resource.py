# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module is about resources and how they are loaded.
"""
from __future__ import print_function, division
import logging
from pyknic import generators

logger = logging.getLogger(__name__)
logger.debug("importing...")

# TODO: use logger

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module


class AbstractResourceLoader(object):
    # TODO: won't work! -> introduce loaded event so the resource class can register on it and know when something has been loaded
    # TODO: how to check that it is loaded only once from disk?? caching...
    def load(self, *args, **kwargs):
        raise NotImplementedError("Abstract method!")

    def unload(self, res):
        raise NotImplementedError("Abstract method!")


class ResourceLoaderNotFoundException(Exception):
    pass


class ResourceLoaderAlreadyExistsException(Exception):
    pass


class Resources(object):
    RESOURCE_INFO_TYPE_KEY = "type"
    RESOURCE_INFO_ARGS_KEY = "args"

    def __init__(self, id_generator=None):
        self._resource_loaders = {}  # {resource_type: loader}
        self._id_generator = id_generator
        if id_generator is None:
            self._id_generator = generators.IdGenerator(0)
        self.resources = {}  # {id: res]
        self._resource_info = {}  # {id: {"type": resource_type, "args": args}

    def register_loader(self, resource_type, loader):
        if resource_type in self._resource_loaders:
            message = "ResourceLoader already registered for type: " + str(resource_type)
            raise ResourceLoaderAlreadyExistsException(message)
        self._resource_loaders[resource_type] = loader

    # TODO: change to: load(self, resource_type, lazy_load, *args, **kwargs)
    # TODO: add specific methods through introspection and eval
    # TODo: use double dispatch pattern to allow to register a member var (e.g. resources.image.load(a, b, c))
    def load(self, resource_type, *args, **kwargs):
        """
        Loads an resource and returns a corresponding id.
        :param resource_type: The type of resource.
        :param args: The arguments needed by the loader to load the resource. Look up the load method of the loader.
        :param kwargs: keyword arguments, used to address arguments like 'bla=2'
        :return: resource_id
        """
        # TODO: cache so a res is not loaded twice from disk, at least a warning in debug
        if resource_type not in self._resource_loaders:
            raise ResourceLoaderNotFoundException("Could not find ResourceLoader for type: " + str(resource_type))
        res = self._resource_loaders[resource_type].load(*args, **kwargs)
        return self.set_resource(resource_type, res, *args)

    # TODO: if lazy_load, check if the resource has been loaded, otherwise load it
    def get_resource(self, resource_id):
        return self.resources[resource_id]

    def get_resource_info(self, resource_id):
        """
        Returns a dictionary with keys: Resources.RESOURCE_INFO_TYPE_KEY and Resources.RESOURCE_INFO_ARGS_KEY
        :param resource_id: the resource id
        :return: dict {self.RESOURCE_INFO_TYPE_KEY: resource_type, self.RESOURCE_INFO_ARGS_KEY: args}
        """
        return self._resource_info[resource_id]

    def set_resource(self, resource_type, res, *args):

        res_id = self._id_generator.next()
        self.resources[res_id] = res
        # TODO: replace dict with resource info object?
        self._resource_info[res_id] = {self.RESOURCE_INFO_TYPE_KEY: resource_type, self.RESOURCE_INFO_ARGS_KEY: args}
        return res_id

    def unload(self, resource_id):
        resource_type = self._resource_info[resource_id][self.RESOURCE_INFO_TYPE_KEY]
        self._resource_loaders[resource_type].unload(self.resources[resource_id])
        del self.resources[resource_id]
        del self._resource_info[resource_id]

logger.debug("imported")
