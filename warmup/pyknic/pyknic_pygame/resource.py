# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Various resource loaders for pygame.
"""
from __future__ import print_function, division

import logging
import os
from os.path import abspath as _abspath
from os.path import join as _join
from os.path import normpath as _normpath

import pygame

from pyknic.pyknic_pygame.audio import Song
from pyknic.resource import Resources, AbstractResourceLoader

# from ... import spritesheetlib

logger = logging.getLogger(__name__)
logger.debug("importing...")

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2015"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module 

IMAGE = 1
SOUND = 2
SONG = 3
MUSIC = 3
FONT = 4
SPRITESHEET = 5

resources = Resources()


class ImageLoader(AbstractResourceLoader):
    def load(self, filename, scale=1.0, angle=0, width=100, height=50, color=(255, 0, 0, 255)):
        """
        Loads an image from disk and applies scaling and/or rotation to it. If the file is not found a default surface
        is created using the optional arguments width, height and color.
        :param filename: name of the file to load
        :param scale: initial scaling, defaults to 1.0
        :param angle: initial angle, defaults to 0
        :param width: optional argument, defaults to 100
        :param height: optional argument, defaults to 50
        :param color:  optional argument, defaults to (255, 0, 0, 255)
        :return: pygame.Surface
        """
        try:
            img = pygame.image.load(filename).convert_alpha()
            logger.info("Resource image loaded: " + filename)
        except pygame.error:
            msg = "Loading file '{0}', using a surface instead: {1}x{2} {3}".format(filename, width, height, color)
            logger.error(msg)
            img = self._create_surface(width, height, color, filename)
        if scale != 1.0 or angle != 0:
            img = pygame.transform.rotozoom(img, angle, scale)
        return img

    def _create_surface(self, width, height, color, file_name):
        img = pygame.Surface((width, height))
        img.fill(color)

        self._put_name_on_surf(file_name, img)

        # noinspection PyArgumentList
        img = img.convert_alpha()
        return img

    # noinspection PyMethodMayBeStatic
    def _put_name_on_surf(self, file_name, img):
        try:
            pygame.font.init()
            if pygame.font.get_init():
                import os
                text = os.path.basename(file_name)
                font = pygame.font.Font(None, 15)
                name_surf = font.render(text, 0, (255, 255, 255))
                img.blit(name_surf, (0, 0))
        except Exception as ex:
            logger.error("Could not put name '{0}' on image: {1}".format(file_name, str(ex)))

    def unload(self, res):
        logger.info("Resource image unloaded: " + str(res))
        pass


class SoundLoader(AbstractResourceLoader):
    def load(self, filename, volume):
        sfx = self._DummySound(filename)
        try:
            if pygame.mixer.get_init() is not None:
                sfx = pygame.mixer.Sound(filename)
                logger.info("Resource sound loaded: " + filename + " volume: " + str(volume))
            else:
                logger.info("Resource Dummy sound loaded: " + filename)
        except Exception as ex:
            logger.error("Error loading sound {0}: {1}".format(filename, str(ex)))
        sfx.set_volume(volume)
        return sfx

    def unload(self, res):
        pass

    class DummyChannel(object):

        def __init__(self, file_name):
            self.file_name = file_name

        def _log_method(self, name, *args, **kwargs):
            logger.debug("{0}<{4}>.{1}({2}, {3})".format(self.__class__.__name__, name, args, kwargs, self.file_name))

        def __getattr__(self, item):
            return lambda *args, **kwargs: self._log_method(item, *args, **kwargs)

    class _DummySound(object):
        def __init__(self, file_name, volume=1.0):
            self.file_name = file_name
            self._volume = volume

        def play(self, loops=0, maxtime=0, fade_ms=0):
            msg = "{0}<{1}>.play({2}, {3}, {4})".format(self.__class__.__name__, self.file_name, loops, maxtime,
                                                        fade_ms)
            logger.debug(msg)
            return SoundLoader.DummyChannel(self.file_name)

        def stop(self):
            logger.debug("{0}<{1}>.stop()".format(self.__class__.__name__, self.file_name))

        def fadeout(self, time):
            logger.debug("{0}<{1}>.fadeout({2})".format(self.__class__.__name__, self.file_name, time))

        def set_volume(self, value):
            self._volume = value
            logger.debug("{0}<{1}>.set_volume({2})".format(self.__class__.__name__, self.file_name, value))

        def get_volume(self):
            logger.debug("{0}<{1}>.get_volume()".format(self.__class__.__name__, self.file_name))
            return self._volume

        def get_num_channels(self):
            logger.debug("{0}<{1}>.get_num_channels()".format(self.__class__.__name__, self.file_name))
            return 0

        def get_length(self):
            logger.debug("{0}<{1}>.get_length()".format(self.__class__.__name__, self.file_name))
            return 0

        def get_raw(self):
            logger.debug("{0}<{1}>.get_raw()".format(self.__class__.__name__, self.file_name))
            return bytes("")  # py3


class SongLoader(AbstractResourceLoader):
    def load(self, name, filename, volume):
        song = Song(name, filename, volume)
        logger.info("Resource song loaded: " + filename)
        return song

    def unload(self, res):
        logger.info("Resource song unloaded: " + str(res))
        pass


class FontLoader(AbstractResourceLoader):
    # TODO: load(self, filename, size)
    def load(self, filename, size):
        _font = pygame.font.Font(filename, size)
        logger.info("Resource font loaded: {0} size: {1}".format(filename, size))
        return _font

    def unload(self, res):
        logger.info("Resource font unloaded: " + str(res))
        pass


# class SpritesheetLoader(AbstractResourceLoader):
#     def load(self, filename, zoom=1.0):
#         sprites = spritesheetlib.SpritesheetLib10().load_spritesheet(spritesheetlib.FileInfo(filename))
#         if zoom != 1.0:
#             for spr in sprites:
#                 spr.image = pygame.transform.rotozoom(spr.image, 0.0, zoom)
#         logger.info("Resource spritesheet loaded: {0} zoom: {1}".format(filename, zoom))
#         return sprites
#
#     def unload(self, res):
#         pass

resources.register_loader(IMAGE, ImageLoader())
resources.register_loader(SOUND, SoundLoader())
resources.register_loader(SONG, SongLoader())
resources.register_loader(FONT, FontLoader())


# resources.register_loader(SPRITESHEET, SpritesheetLoader())


class LoaderNotFoundException(Exception):
    pass


class LoaderAlreadyRegisteredException(Exception):
    pass


class ResourceAbsolutePathNotAllowedException(Exception):
    pass


class UnknownConfigAttributeException(Exception):
    pass


class DuplicateResourceIdException(Exception):
    pass


class ResourceLoader(object):

    def __init__(self):
        self._loaders = {}  # {loader_id: loader}
        self._resources_by_id = {}  # {res_id: res}
        self._resource_cache = {}  # {key: res}
        self._config_prefix = "config_"
        self._config_attr_names = []
        self.config_base_dir = os.curdir  # todo: document
        self.config_place_holder = False  # todo: document

    def register(self, loader):
        if loader.id in self._loaders:
            raise LoaderAlreadyRegisteredException("Another loader with id {} is already registered.".format(loader.id))
        self._loaders[loader.id] = loader

    @property
    def types(self):
        return self._loaders.keys()

    def load(self, resource_definition):
        # todo: report progress: callback? time-sliced?
        resource_definition = dict(resource_definition)  # work with a copy

        # set config_* attributes
        for config_attr_name, config_attr_value in resource_definition.items():
            if config_attr_name.startswith(self._config_prefix):
                self._set_attribute(self, config_attr_name, config_attr_value, True)
                del resource_definition[config_attr_name]  # remove config_* attributes
                self._config_attr_names.append(config_attr_name)

        if not os.path.isabs(self.config_base_dir):
            self.config_base_dir = os.path.join(os.curdir, self.config_base_dir)
        self.config_base_dir = os.path.abspath(self.config_base_dir)

        for loader_id, _resources_dict in resource_definition.items():
            loader = self._loaders.get(loader_id, None)
            if loader is None:
                raise LoaderNotFoundException("Loader not found for: " + loader_id)

            self._load_with_loader(_resources_dict, loader)

        return dict(self._resources_by_id)

    def _load_with_loader(self, _resources_dict, loader):
        # set global config_* attributes to the loader first
        for _n in self._config_attr_names:
            self._set_attribute(loader, _n, getattr(self, _n), False)

        # set specific loader config_* attributes (possibly overwriting global attribute values)
        for _kk, _vv in _resources_dict.items():
            if isinstance(_kk, str) and _kk.startswith(self._config_prefix):
                self._set_attribute(loader, _kk, _vv, True)
                del _resources_dict[_kk]

        # load resources
        for res_id, args in _resources_dict.items():
            if res_id in self._resources_by_id:
                raise DuplicateResourceIdException(
                    "Duplicate resource id '{0}' detected for resource type '{1}'".format(res_id, loader.id))

            self._correct_resource_path(args)

            res = self._load_cached(args, loader)

            self._resources_by_id[res_id] = res

    def _load_cached(self, args, loader):
        res_key = (loader.id, tuple(args.items()))
        res = self._resource_cache.get(res_key, None)
        if res is None:
            res = loader.load(args, self._load_cached)
            self._resource_cache[res_key] = res
        return res

    def _correct_resource_path(self, args):
        if os.path.isabs(args['name']):
            _msg = "Absolute paths are not allowed (try to use \'./\' in front): {0}".format(
                str(args['name']))
            raise ResourceAbsolutePathNotAllowedException(_msg)
        args['name'] = _abspath(_normpath(_join(self.config_base_dir, args['name'])))

    @staticmethod
    def _set_attribute(obj, k, v, strict):
        if strict and not hasattr(obj, k):
            raise UnknownConfigAttributeException("Config attribute '{0}' is invalid on {1} ".format(k, obj))
        setattr(obj, k, v)


logger.debug("imported")


