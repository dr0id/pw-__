# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_init_pygame.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging
import os

import pygame

from pyknic import context

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["PygameInitContext"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


def _pre_init():
    os.environ['SDL_VIDEO_CENTERED'] = '1'


class PygameInitContext(context.Context):

    def __init__(self, display_init_callback, pre_init_cb=_pre_init):
        """
        A call back to initialize the pygame screen.

        :param display_init_callback: A callback method that will be called with like this:
            'frequency, mixer_size, channels, buffer_size = self._display_init_cb(display_info, driver_info, wm_info)'
            It should return the mixer parameters frequency, mixer_size, channels, buffer_size
        :param pre_init_cb: optional callback called before any pygame module is initialized. This should be used to set
            environment variables for SDL or similar (e.g. os.environ['SDL_VIDEO_CENTERED'] = '1' )
        """
        context.Context.__init__(self)
        self._display_init_cb = display_init_callback
        self._pre_init = pre_init_cb

    def enter(self):
        self.init_pygame()

    def exit(self):
        pygame.quit()

    def update(self, *args):
        self.pop()

    def init_pygame(self):
        logger.info("%s pygame init %s", "#" * 10, "#" * 10)
        logger.info("----- pre init stuff -----")
        self._pre_init()
        imported_modules = self._import_modules()
        delayed_init = self._initialize_modules(imported_modules)
        display_info, driver_info, wm_info = self._get_display_info()

        # application specific
        frequency, mixer_size, channels, buffer_size = self._display_init_cb(display_info, driver_info, wm_info)
        # end application specific

        # check if a surface can be retrieved
        pygame.display.get_surface()

        self._initialize_delayed_modules(delayed_init, imported_modules, frequency, mixer_size, channels, buffer_size)
        logger.info("%s pygame init DONE %s", "#" * 10, "#" * 10)

    def _initialize_delayed_modules(self, delayed_init, imported_modules, frequency, mixer_size, channels, buffer_size):
        msg = "----- setting pre mixer pre init values: " \
              "frequency: %s, mixer_size: %s, channels: %s, buffer_size: %s -----"
        logger.info(msg, frequency, mixer_size, channels, buffer_size)
        pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)

        logger.info("----- init delayed modules (after display set_mode) -----")
        for _name, _module in imported_modules:
            if any(s in _name for s in delayed_init):
                self._init_module(_module)

    @staticmethod
    def _get_display_info():
        logger.info("----- display info -----")
        display_info = pygame.display.Info()
        logger.info("display info: %s", str(display_info))
        driver_info = pygame.display.get_driver()
        logger.info("display driver: %s", driver_info)
        wm_info = pygame.display.get_wm_info()
        logger.info("display window manager info: %s", str(wm_info))
        return display_info, driver_info, wm_info

    def _initialize_modules(self, imported_modules):
        logger.info("----- initialize modules -----")
        delayed_init = ["mixer", "scrap"]
        for _name, _module in imported_modules:
            if not any(s in _name for s in delayed_init):
                self._init_module(_module)
            else:
                logger.debug("delay init for module %s", _name)
        return delayed_init

    @staticmethod
    def _import_modules():
        logger.info("----- import modules -----")
        # num_pass, num_fail = pygame.init()
        # logger.info("pygame.init(): pass: %s  failed: %s", num_pass, num_fail)
        # if num_fail != 0:
        #     logger.error("pygame failed init pass:%s failed:%s", num_pass, num_fail)
        #
        # if num_pass <= 0:
        #     logger.error("pygame didn't initialize anything!")
        import pkgutil
        import importlib
        imported_modules = []
        skip_modules = ["examples", "tests", "docs", "threads"]
        try:
            modules = pkgutil.walk_packages(pygame.__path__, "pygame.")
            for module_info in modules:
                _finder, name, is_pkg = module_info
                try:

                    if any(s in name for s in skip_modules):
                        logger.info("skipping import of excluded package/module '%s'", name)
                        continue  # skip

                    if is_pkg:
                        logger.info("skipping package: %s", name)
                    else:
                        logger.info("importing module: %s", name)
                        _module = importlib.import_module(name)
                        imported_modules.append((name, _module))

                except ImportError as ie:
                    logger.warning("import error while importing '%s': %s", name, ie)
                except AttributeError as ae:
                    logger.error("attribute error while importing '%s': %s", name, ae)
                except Exception as ex:
                    logger.error("exception while importing '%s': %s", name, ex)
        except Exception as fatal_ex:
            logger.fatal("fatal pygame import error: %s", fatal_ex)
            raise fatal_ex
        finally:
            del pkgutil
            del importlib
        return imported_modules

    @staticmethod
    def _init_module(m):
        if hasattr(m, 'init'):
            try:
                logger.info("initializing module %s", m)
                logger.info("  init() %s", m)
                m.init()
                if m.get_init():
                    logger.info("    get_init() True %s", m)
                else:
                    logger.error("    get_init() False %s", m)
            except AttributeError as ae:
                # module has not get_init() method
                logger.warning("    missing get_init() in module %s: %s", m, ae)
            except Exception as ex:
                logger.error("    init() raised an exceptions: %s", ex)
        else:
            logger.info("module has no init(): %s", m)


if __name__ == '__main__':
    def _standalone_display_init():
        pygame.display.set_mode((800, 600))
        MIXER_FREQUENCY = 22050  # default:22050
        MIXER_SIZE = -16  # default: -16
        MIXER_CHANNELS = 2  # default: 2 stereo or 1 mono
        MIXER_BUFFER_SIZE = 128  # default: 4096
        return MIXER_FREQUENCY, MIXER_SIZE, MIXER_CHANNELS, MIXER_BUFFER_SIZE


    PygameInitContext(_standalone_display_init).init_pygame()

logger.debug("imported")
