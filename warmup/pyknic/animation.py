# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'animation.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module should facilitate the animation of things. Either traditional sprite based animations or model animations
interpolating in between keyframes states.


Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

from pyknic.events import Signal

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["Animation"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Animation(object):
    """
    The base animation class. All it does is change a number (may be an index) in a certain fps and loop back.
    The start index can be different from 0 with the idea, that if you load all images in a image strip,
    animation A starts at frame x and ends at x + c, where c is the frame count, e.g.::

            first image                         last image
              v                                  v
         ...+----+----+----+----+----+----+----+----+----+...
            | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 |
         ...+----+----+----+----+----+----+----+----+----+...
              ^              ^     ^                  ^
          start index     last     current        end index (start index + frame count)

    This animation would start at index 10 and have 8 frames (0 based indexing, so images 10-17).

    Apart of the current_index also the last_index is tracked. This are two ordinary attributes. The last_index
    will be set always to the last value that current_index had before it was updated.

    In general, all the attributes should be treated as read only. Setting values directly to them might generate
    strange effects.

    For the animations to work they need a scheduler. To make things simpler Animation.scheduler is present. Just
    updated this instance and all your animations will get updated appropriately, e.g. in your main loop call:
    Animation.update(dt).

    For more flexibility and to conform with the dependency injection pattern, each animation object can be passed in
    an individual scheduler. This one will then be used instead (don't forget to update this instance then).

    Example::

        ...
        anim = Animation(10, 10, 20)
        ...
        while running:
            ...
            # update
            Animation.scheduler.update(dt)
            ...

    There is no draw method since this class should work as a base class for more concrete animations.

    To play the animation only once one must know when the animation has reached the last index. For this purpose the
    event_animation_end even exists. Just register a handler with the following signature: my_handler(animation)

    Example::

        def stop_animation_after_play(anim):
            anim.stop()

        ...
        anim = Animation(...)
        anim.event_animation_end += stop_animation_after_play
        ...
        anim.start()
        ...

    With that event it is possible to know when an animation has reached the end. It is recommended to
    register only one handler. But of course many handlers could be registered to inform other logic.
    """

    def __init__(self, start_index, frame_count, fps, scheduler_to_use, name=None, logger_to_use=None):

        """
        The animation constructor.
        :param start_index: The start index of the animation.
        :param frame_count: The number of frames in this animation.
        :param fps: The frame per second this animation should play.
        :param name: The name of this animation. If None, then the id() is used.
        :param logger_to_use: The logger instance to use. If None, then the module.logger instance is used.
        :param scheduler_to_use: The scheduler to use. If None then Animation.scheduler is used.
        """
        self.scheduler = scheduler_to_use

        self._fps = fps
        self._start_index = start_index
        self._frame_count = frame_count
        self._end_index = start_index + frame_count
        self._schedule_id = 0
        self._last_update_time = 0
        self._play_once = False

        # public, should be treated as read only
        self.name = id(self) if name is None else name
        self.event_animation_end = Signal("animation end event for " + str(self.name))
        self.event_index_changed = Signal("animation index changed event for " + str(self.name))
        self.logger = logger if logger_to_use is None else logger_to_use
        self.current_index = start_index
        self.last_index = self.current_index
        self.is_running = False
        self.fps = fps

        _msg = "Created animation '%s' with start index %s, frame count %s, fps %s"
        self.logger.info(_msg, self.name, self._start_index, self._frame_count, self.fps)

    @property
    def is_looping(self):
        return not self._play_once

    @is_looping.setter
    def is_looping(self, value):
        self._play_once = not value

    @property
    def fps(self):
        """
        The fps property getter.
        :return: The currently set fps.
        """
        return self._fps

    @fps.setter
    def fps(self, value):
        """
        The fps property setter.
        :param value: The new fps to set. If the animation is running, then a reschedule with the new fps is done.
        """
        self.logger.debug("setting fps to {0} for animation '{1}'", value, self.name)
        self._fps = value
        if self.is_running:  # update the schedule immediately
            self.logger.debug("re-scheduling animation '%s' due to new fps settings", self.name)
            self.stop()
            self.start()

    @property
    def interpolation(self):
        """
        The interpolation getter.
        :return: The interpolation value in the range [0.0, 1.0)
        """
        if self.is_running:
            alpha = float((self.scheduler.current_time - self._last_update_time) * self._fps)
            return alpha if alpha < 1.0 else 1.0

        return 0.0

    def set_current_index(self, value):
        """
        The current_index setter. Use only this method to set the current_index. Only this method will check that the
        index is in the right range (and move it to the right range if needed).
        :param value: The index it should point to. The index will be moved into the range
        [start_index, start_index + frame_count) if it is outside.
        """
        self.logger.info("Setting current_index to %s on animation '%s' with start_index %s and frame count %s",
                         value, self.name, self._start_index, self._frame_count)
        self.last_index = self.current_index
        if value < self._start_index:
            while value < self._start_index:
                value += self._frame_count
            self.current_index = value
        else:
            self.current_index = value % self._frame_count + self._start_index
        if self.last_index != self.current_index:
            self.event_index_changed.fire(self)
        self.logger.debug("current_index changed to %s for animation '%s'", self.current_index, self.name)

    def start(self):
        """
        Start playing the animation. If the animation is already playing then nothing will happen.
        Once the animation has been started it will loop the current index. To play the animation only once see
        the module description.
        """
        if self.is_running:
            self.logger.info("Trying to start animation '%s' but is already running", self.name)
            return

        if self._fps <= 0:
            self.logger.info("Trying to start animation '%s' but fps '%s' should be > 0.0 ", self.name, self._fps)
            return

        if self._frame_count > 1:
            logger.debug("start anim: old schedule id: {0} | {1}", self._schedule_id, self)
            self._schedule_id = self.scheduler.schedule(self._update, 1.0 / self._fps)
            logger.debug("start anim: new schedule id: {0} | {1}", self._schedule_id, self)

        self.is_running = True
        self.logger.info("Animation.start on '%s'", self.name)

    def _update(self):
        assert self.is_running, "animation _update called while is_running == False!! {0}".format(self)
        self.last_index = self.current_index
        self.current_index += 1
        self._last_update_time = self.scheduler.current_time
        if self.current_index >= self._end_index:
            if self.event_animation_end.has_observers:
                self.event_animation_end.fire(self)

            if self._play_once:
                return 0  # stop re-scheduling

            if self.is_running:
                self.current_index = self._start_index
            else:
                self.current_index = self.last_index
                return 0  # stop re-scheduling, this happens if 'stop' was called in the event_animation_end

        self.event_index_changed.fire(self)
        return 1.0 / self._fps

    def stop(self):
        """
        Stop playing the animation. If the animation is not playing, then nothing will happen.
        """
        if not self.is_running:
            self.logger.info("Trying to stop animation '%s' but is already stopped", self.name)
            return

        if self._frame_count > 1:
            logger.debug("anim stop remove schedule id: {0} | {1}", self._schedule_id, self)
            self.scheduler.remove(self._schedule_id)

        self.is_running = False
        self.logger.info("Animation.stop on '%s'", self.name)

    def reset(self):
        """
        Reset the animation to the start_index.
        """
        self.current_index = self._start_index
        self.logger.info("Animation.reset on '%s'", self.name)


logger.debug("imported")
