# -*- coding: utf-8 -*-

"""
This module should provide information about the pyknic module.

besides of:

__version__
__author__

__copyright__
__credits__
__license__
__maintainer__

Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]
                   |
                   +-|* 0 for alpha (status)
                     |* 1 for beta (status)
                     |* 2 for release candidate
                     |* 3 for (public) release

    For instance:
        * 1.2.0.1 instead of 1.2-a
        * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
        * 1.2.2.3 instead of 1.2-rc (release candidate)
        * 1.2.3.0 instead of 1.2-r (commercial distribution)
        * 1.2.3.5 instead of 1.2-r5 (commercial distribution with many bug fixes)


.. versionchanged:: 4.0.2.0
    renamed test to tests
    many improvements in the timing module: introduced Timer, FrameCap and made classes compatible in update signature
    moved pyknicpygame to pyknic.pygame, needs to be imported separately
    added more helper classes: simplestatemachine, animation, tweening, ...


.. versionchanged:: 3.0.1.1
    removed: VERSION, VERSIONNUMBER
    added: version, version_info, __versionnumber__
    __versionnumber__ is a tuple containing ints according to the versioning scheme.


"""
from __future__ import print_function, division

import logging
import sys
import warnings

logger = logging.getLogger(__name__)
logger.debug("importing...")

__versionnumber__ = (5, 0, 0, 1)
__version__ = ".".join([str(num) for num in __versionnumber__])
__version_info__ = __versionnumber__

__author__ = "DR0ID (C) 2011-2017"

__copyright__ = "Copyright 2011-2017, DR0ID"
__credits__ = ["DR0ID"]
__license__ = "New BSD license"
__maintainer__ = "DR0ID"

__all__ = ["version", "version_info", "check_version", "log_info"]

version = __version__
version_info = __versionnumber__


class VersionMismatchError(Exception):
    pass


# check python version
def check_version(library_name, lower_version, actual_version, upper_version, excluded_versions=()):
    """
    Check version compatibility.

    ::

        # check python version
        check_version('python', (2, 7), tuple(sys.version_info[:2]), (3, 7))

    :param library_name: the name of the library to check
    :param lower_version: the lowest supported version
    :param actual_version: the actual found version
    :param upper_version: the next, upper unsupported version
    :param excluded_versions: excluded version in the range of the lower to upper supported version
    """
    logger.debug("checking version: {0}: {1} <= {2} <= {3}", library_name, lower_version, actual_version, upper_version)

    version_types = [type(lower_version), type(actual_version), type(upper_version)] +[type(v) for v in excluded_versions]
    if len(set(version_types)) > 1:
        raise ValueError("all provided versions should have same type (e.g. int, tuples, etc")

    if not upper_version > actual_version:
        msg_string = "checking version: {0}: {1} <= {2} <= {3}! The lib has not been tested with the version you are " \
                     "trying to use. It might work or not. Try at your own risk"
        msg = msg_string.format(library_name, lower_version, actual_version, upper_version)
        logger.error(msg)
        warnings.warn(msg_string)

    if not actual_version >= lower_version:
        msg = "unsupported version {0} for '{1}', should be > {2} ".format(actual_version, library_name, lower_version)
        version_exception = VersionMismatchError(msg)
        logger.error(version_exception)
        raise version_exception

    for exclude_version in excluded_versions:
        if not exclude_version >= lower_version or not exclude_version <= upper_version:
            out_or_range_msg = "excluded version should be in version range of [{0}, {1}]".format(lower_version,
                                                                                                  upper_version)
            raise ValueError(out_or_range_msg)
        if not actual_version != exclude_version:
            msg_string = "unsupported version {0} for '{1}', should be in range {2} <= version < {3} and not in [{4}]"
            excluded_versions_str = str.join(", ", (str(e) for e in excluded_versions))
            msg = msg_string.format(actual_version, library_name, lower_version, upper_version, excluded_versions_str)
            version_exception = VersionMismatchError(msg)
            logger.error(version_exception)
            raise version_exception


def log_info(log_separator_width=50):
    logger.info("*" * log_separator_width)
    logger.info("*" * log_separator_width)
    logger.info("*" * log_separator_width)
    _format_string = "%-" + str(log_separator_width) + "s"
    logger.info(_format_string % ("pyknic v" + version + " (c) DR0ID 2011-2018"))

    try:
        logger.info("using python version:   %s   ", sys.version)
        logger.info("copyright:   %s   ", sys.copyright)
        logger.info("argv:   %s   ", sys.argv)
        logger.info("executable:   %s   ", sys.executable)
        logger.info("__debug__:   %s    ", __debug__)

        try:
            # py >3.2
            logger.info("abiflags:   %s   ", sys.abiflags)
        except Exception as e:
            logger.error(e)

        try:
            # py >3.2
            logger.info("exec_prefix:   %s   ", sys.exec_prefix)
        except Exception as e:
            logger.error(e)

        try:
            # py >3.3
            logger.info("base_exec_prefix:   %s   ", sys.base_exec_prefix)
        except Exception as e:
            logger.error(e)

        try:
            # py >3.3
            logger.info("base_prefix:   %s   ", sys.base_prefix)
        except Exception as e:
            logger.error(e)

        try:
            # py >3.3
            logger.info("prefix:   %s   ", sys.prefix)
        except Exception as e:
            logger.error(e)
        logger.info("flags:   %s   ", sys.flags)
        logger.info("byteorder:   %s   ", sys.byteorder)
        logger.info("builtin_module_names:   %s   ", sys.builtin_module_names)
        logger.info("dllhandle:   %s   ", sys.dllhandle)
        logger.info("dont_write_bytecode:   %s   ", sys.dont_write_bytecode)
        logger.info("float_info:   %s   ", sys.float_info)

        try:
            # py >3.1
            logger.info("float_repr_style:   %s   ", sys.float_repr_style)
        except Exception as e:
            logger.error(e)

        try:
            logger.info("getswitchinterval():   %s   ", sys.getswitchinterval())
        except Exception as e:
            logger.info("getcheckinterval():   %s   ", sys.getcheckinterval())
        logger.info("getdefaultencoding():   %s   ", sys.getdefaultencoding())
        logger.info("getfilesystemencoding():   %s   ", sys.getfilesystemencoding())

        try:
            logger.info("getdlopenflags():   %s   ", sys.getdlopenflags())
        except Exception as e:
            logger.error(e)

        try:
            logger.info("getfilesystemencodeerrors():   %s   ", sys.getfilesystemencodeerrors())
        except Exception as e:
            logger.error(e)

        logger.info("getrecursionlimit():   %s   ", sys.getrecursionlimit())
        logger.info("getprofile():   %s   ", sys.getprofile())
        logger.info("gettrace():   %s   ", sys.gettrace())

        try:
            # py windows
            logger.info("getwindowsversion():   %s   ", sys.getwindowsversion())
        except Exception as e:
            logger.error(e)

        try:
            # py >=3.2
            logger.info("hash_info:   %s   ", sys.hash_info)
        except Exception as e:
            logger.error(e)

        logger.info("hexversion:   %s   ", sys.hexversion)

        try:
            # py >= 3.3
            logger.info("implementation:   %s   ", sys.implementation)
        except Exception as e:
            logger.error(e)

        try:
            # py >= 3.1
            logger.info("int_info:   %s   ", sys.int_info)
        except Exception as e:
            logger.error(e)
        logger.info("maxsize:   %s   ", sys.maxsize)
        logger.info("maxunicode:   %s   ", sys.maxunicode)
        logger.info("meta_path:   %s   ", sys.meta_path)
        logger.info("modules:")
        # for mod in sorted(sys.modules):
        #     logger.info("      %s   ", mod)

        logger.info("path:   %s   ", sys.path)
        logger.info("path_hooks:   %s   ", sys.path_hooks)
        logger.info("path_importer_cache:   %s   ", sys.path_importer_cache)
        logger.info("platform:   %s   ", sys.platform)

        try:
            logger.info("ps1:   %s   ", sys.ps1)
        except Exception as e:
            logger.error(e)

        try:
            logger.info("ps2:   %s   ", sys.ps2)
        except Exception as e:
            logger.error(e)

        try:
            # py >= 3.3
            logger.info("thread_info:   %s   ", sys.thread_info)
        except Exception as e:
            logger.error(e)

        try:
            logger.info("tracebacklimit:   %s   ", sys.tracebacklimit)
        except Exception as e:
            logger.info("tracebacklimit default:   1000   ")
            logger.error(e)
        logger.info("version:   %s   ", sys.version)
        logger.info("api_version:   %s   ", sys.api_version)
        logger.info("version_info:   %s   ", sys.version_info)
        logger.info("warnoptions:   %s   ", sys.warnoptions)

        try:
            # windows only
            logger.info("winver:   %s   ", sys.winver)
        except Exception as e:
            logger.error(e)

        try:
            # py >=3.2
            logger.info("_xoptions:   %s   ", sys._xoptions)
        except Exception as e:
            logger.error(e)
    except Exception as ee:
        logger.error(ee)

logger.debug("imported")
