﻿Improbable Mission Force
========================

Pyweek 28 / Sep 2019

CONTACT:

Homepage: https://pyweek.org/e/pw---__/
Name: Improbable Mission Force
Team: pw---__
Members: DR0ID, gummbum, mit-mit, Axiom, erglim


DEPENDENCIES:

You might need to install some of these before running the game:

  Python:     http://www.python.org/
  PyGame:     http://www.pygame.org/


RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run_game.py


HOW TO PLAY THE GAME:

Move the cursor around the screen with the mouse.

Perform these in-game actions with the mouse and keyboard:
LMB to fire the primary weapon
RMB to fire the secondary weapon
WASD to move
E to use an door
Shift to crouch
Space to jump
tab/scroll up to change primary weapon


LICENSE:

This game and skellington have the same license as pyknic.


TIPS AND HACKS:

1. The tactical HUD displays the hero's:
Location as Floor and Room
Primary and secondary weapon
Health

2. Read the door labels:

f:Qn (e.g. 3:Q1) quest room; f is the current floor; n is the quest number
f:Ln (e.g. 1:L1) lobby; f is the current floor; n is the lobby number
f:En (e.g. 1:E1) elevator; f is the floor where the elevator lets out; n is the elevator number
f:Sn (e.g. 2:S1) stairwell; f is the floor where the stairs let out; n is the stairwell number

3. The following tunables may be useful, and can be edited in gamelib/settings.py:

master_volume = 0.5             # min: 0.0; max: 1.0
music_volume = 1.0              # resulting level = master_volume * music_volume
sfx_volume = 1.0                # resulting level = master_volume * sfx_volume
mission_chatter_speed = 4.0     # seconds between emits
hero_starting_health = 100      # hero health at start of mission and healing rooms
fx_enable_tactical_hud_flicker = True   # False: turn off the twitchy tactical HUD effect if it bothers you
