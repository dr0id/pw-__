======
pyknic
======

.. .. image:: https://badge.fury.io/py/pyknic.png
    :target: http://badge.fury.io/py/pyknic

.. .. image:: https://travis-ci.org/dr0id/pyknic.png?branch=master
        :target: https://travis-ci.org/dr0id/pyknic

.. .. image:: https://pypip.in/d/pyknic/badge.png
        :target: https://crate.io/packages/pyknic?version=latest


pyknic is a collection of useful modules and classes for creating games. Most of them are independent of any library.
There are some adapters or pygame specific implementations too.

* Free software: BSD license

.. .. todo:: * Documentation: http://pyknic.rtfd.org.
.. .. todo:: * Discussion: https://dr0id.bitbucket.org/TODO


About
-----

* skellington - easily start a new project and make sure your game is executed with max performance
* pyknic - game utilities

  * timing
  * events
  * context stack
  * state machine template
  * tweening
  * flexible context management with transition effect template
  * pygame - pygame specific code for pyknic

    * powerful spritesystem
    * transition effects for context changes


How to use it
-------------

skellington
===========

Copy the directory structure to a new location. Start implementing your code in gamelib/main.py in the method main().
Replace the line 'raise NotImplementedError("Implement your code here!")' with your code!

To run the code then use either

* run.py - runs the code optimized (using the -OO flag)
* run.pyw - runs the code without a console, but still optimized (using the -OO flag)
* run_debug.py - runs the code in debug mode

Running any of the above files will create a corresponding log file beside them. E.g. run.py creates run.py.log,
run.pyw creates run.pyw.log and run_debug.py creates run_debug.py.log.

To run these files either double click them or use a console or command window to execute them.

Happy coding!


pyknic
======

The recommended way to use the pyknic package is to use it as a local package, imported by a relative import. This is
the simplest way to use it. E.g.

::

    pyknic
    mycode.py
    other.py

Do avoid relative import like '*from . import pyknic*' due to the weird python relative import mechanics
(python treats the same module imported from two different relative paths as
this where two different modules!).

Recommended directory structure is:

::

    game
        pyknic
        other_lib1
        other_lib2
        gamelib
            __init__.py
            your_game_code.py
            main.py
        run.py

And import all modules from the root, e.g.

.. code-block::
    python

    # gamelib.__init__.py
    from gamelib import main

    # run.py
    import gamelib
    gamelib.main.main()

    # main.py
    import pyknic
    import other_lib1
    import other_lib2

    def main():
        # put here your code
        pass



