# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'resource_gfx.py' is part of pw---__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The gfx resources.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import os

from gamelib import settings

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class _Loader(object):

    def __init__(self):
        self.data = None
        self.fps = 0

    def get_path(self, path):
        assert '\\' not in path, "only forward slashes are allowed, will convert with os.path.join"
        return os.path.join(*path.split('/'))


class SpriteSheet(_Loader):

    def __init__(self, path_to_file, tile_size, row_count, column_count, fps, frames_slice=None):
        _Loader.__init__(self)
        self.slice = frames_slice
        self.fps = fps
        self.path_to_file = self.get_path(path_to_file)
        self.tile_size = tile_size
        self.row_count = row_count
        self.column_count = column_count


class FileList(_Loader):

    def __init__(self, path_to_dir, names, fps=0):
        _Loader.__init__(self)
        self.fps = fps
        self.path_to_dir = self.get_path(path_to_dir)
        self.names = names


class Image(_Loader):

    def __init__(self, path_to_file):
        _Loader.__init__(self)
        self.path_to_file = self.get_path(path_to_file)


class DirectoryAnimation(_Loader):

    def __init__(self, path_to_dir, fps, extension="png"):
        _Loader.__init__(self)
        self.extension = extension
        self.fps = fps
        self.path_to_dir = self.get_path(path_to_dir)


class FakeImage(_Loader):

    def __init__(self, width, height, fill_color, *fill_args):
        _Loader.__init__(self)
        self.width = width
        self.height = height
        self.fill_color = fill_color
        self.fill_args = fill_args


image_names = {
    settings.KIND_EXPLOSION: SpriteSheet("data/fx/explode_tiles.png", (192, 192), 1, 6, 8),
    settings.KIND_GRENADE: SpriteSheet("data/fx/grenade_tiles.png", (32, 32), 1, 4, 4),
    (settings.KIND_HERO, 0): DirectoryAnimation("data/fx/hero/frames_crouch", 4),
    (settings.KIND_HERO, 1): DirectoryAnimation("data/fx/hero/frames_jump", 12),
    (settings.KIND_HERO, 2): DirectoryAnimation("data/fx/hero/frames_stand", 4),
    (settings.KIND_HERO, 3): DirectoryAnimation("data/fx/hero/frames_walk", 24),
    (settings.KIND_HERO, 8): DirectoryAnimation("data/fx/hero/frames_climb", 4),
    # note: the hero's arm is rendered on top of hero (different sub-layer)
    (settings.KIND_HERO, 4): DirectoryAnimation("data/fx/hero/frames_arm_jump", 12),
    (settings.KIND_HERO, 5): DirectoryAnimation("data/fx/hero/frames_arm_stand", 4),
    (settings.KIND_HERO, 6): DirectoryAnimation("data/fx/hero/frames_arm_walk", 24),
    (settings.KIND_HERO, 7): DirectoryAnimation("data/fx/hero/frames_arm_shoot", 2),
    (settings.KIND_HERO, 9): DirectoryAnimation("data/fx/hero/frames_arm_climb", 4),
    (settings.KIND_HERO, 10): DirectoryAnimation("data/fx/hero/frames_arm_crouch", 4),
    settings.KIND_ROPE_BULLET: FakeImage(10, 10, (0, 255, 255)),
    settings.KIND_BLASTER_BULLET: Image("data/fx/bullet.png"),
    settings.KIND_ROCKET: SpriteSheet("data/fx/rocket.png", (40, 15), 1, 4, 4),
    "mouse-shadow": FakeImage(3, 3, (50, 50, 50, 80)),
    "mouse-aim": FakeImage(4, 4, (255, 255, 255, 255)),
    (settings.KIND_BADDY, 0): DirectoryAnimation("data/fx/baddie/frames_crouch", 4),
    (settings.KIND_BADDY, 1): DirectoryAnimation("data/fx/baddie/frames_jump", 12),
    (settings.KIND_BADDY, 2): DirectoryAnimation("data/fx/baddie/frames_stand", 4),
    (settings.KIND_BADDY, 3): DirectoryAnimation("data/fx/baddie/frames_walk", 24),
    (settings.KIND_BADDY, 8): DirectoryAnimation("data/fx/baddie/frames_climb", 4),
    # note: the baddie's arm is rendered on top of baddie (different sub-layer)
    (settings.KIND_BADDY, 4): DirectoryAnimation("data/fx/baddie/frames_arm_jump", 12),
    (settings.KIND_BADDY, 5): DirectoryAnimation("data/fx/baddie/frames_arm_stand", 4),
    (settings.KIND_BADDY, 6): DirectoryAnimation("data/fx/baddie/frames_arm_walk", 24),
    (settings.KIND_BADDY, 7): DirectoryAnimation("data/fx/baddie/frames_arm_shoot", 2),
    (settings.KIND_BADDY, 9): DirectoryAnimation("data/fx/baddie/frames_arm_climb", 4),
    (settings.KIND_BADDY, 10): DirectoryAnimation("data/fx/baddie/frames_arm_crouch", 4),

    #"stairwell": FakeImage(500, 500, (100, 255, 255)),
    "stairwell": Image("data/scenes/stairs.png"),
    "elevator": Image("data/scenes/elevator.png"),
    "wall_hit": SpriteSheet("data/fx/bulletwallhit.png", (20, 20), 1, 3, 10),
    "hurt": SpriteSheet("data/fx/hurt.png", (50, 50), 1, 5, 12),
    settings.KIND_ELEVATOR_EXIT: SpriteSheet("data/fx/elevatordoors188x181x16.png", (188, 181), 1, 16, 12),
    settings.KIND_STAIRWELL_EXIT: SpriteSheet("data/fx/doors126x181.png", (126, 181), 1, 4, 0),
    settings.KIND_WEAPON_BLASTER: Image("data/fx/blaster.png"),
    settings.KIND_WEAPON_ROCKET_LAUNCHER: Image("data/fx/rocketlauncher.png"),
    "quest_room": Image("data/scenes/questroom.png"),

}

logger.debug("imported")
