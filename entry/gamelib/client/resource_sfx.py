# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'resource_sfx.py' is part of pw-__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import os

from gamelib import settings
from pyknic.generators import IdGenerator
from pyknic.pyknic_pygame.sfx import SoundData, MusicData

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

_gen_id = IdGenerator(1).next

SFX_GUNFIRE1 = _gen_id()
SFX_ELEVATOR_DOOR = _gen_id()
SFX_EXPLODE = _gen_id()
SFX_THROW = _gen_id()
SFX_ROCKET = _gen_id()

sfx_data = {
    SFX_GUNFIRE1: SoundData(os.path.join('data', 'sound', 'gunfire.ogg'), settings.sfx_volume * settings.master_volume),
    SFX_ELEVATOR_DOOR: SoundData(os.path.join('data', 'sound', 'elevator_door.ogg'),
                                 settings.sfx_volume * settings.master_volume),
    SFX_EXPLODE: SoundData(os.path.join('data', 'sound', 'grenade1.ogg'),
                                 settings.sfx_volume * settings.master_volume),
    SFX_THROW: SoundData(os.path.join('data', 'sound', 'throw_rope.ogg'),
                                 settings.sfx_volume * settings.master_volume),
    SFX_ROCKET: SoundData(os.path.join('data', 'sound', 'rocket_slow.ogg'),
                                 settings.sfx_volume * settings.master_volume),
}

songs = [
    MusicData(os.path.join('data', 'music', 'mainaction_loop.ogg'), 1.0),
    MusicData(os.path.join('data', 'music', 'tension_intro.ogg'), 1.0),
    MusicData(os.path.join('data', 'music', 'tension_loopA.ogg'), 1.0),
    MusicData(os.path.join('data', 'music', 'tension_loopB.ogg'), 1.0),
    MusicData(os.path.join('data', 'music', 'tutorial.ogg'), 1.0),
    MusicData(os.path.join('data', 'music', 'victory.ogg'), 1.0),
    MusicData(os.path.join('data', 'music', 'elevator.ogg'), 1.0)
]

#songtimes = [115.0, 18.3333, 23.3333, 55.0]

logger.debug("imported")
