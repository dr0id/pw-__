# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'client.py' is part of pw---__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The client class.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import os
import random
from collections import defaultdict

import pygame
from pyknic.pyknic_pygame import spritesystem

from gamelib import settings, pygametext
from gamelib.client import resource_sfx, resource_gfx, sprites
from gamelib.client.resource_gfx import image_names
from gamelib.client.sfx_handler import SfxHandler
from gamelib.client.sprites import RectSprite, RopeSprite, GummExitSprite, AnimatedSprite
from gamelib.entities import hero
from gamelib.huds import IMFHUD
from gamelib.settings import gameplay_event_map, ACTION_TOGGLE_DEBUG_RENDER, music_ended_pygame_event, \
    ACTION_HERO_AIM, EVT_ACTION, EVT_HERO_CREATED, EVT_HERO_STATE_CHANGED, EVT_BULLET_CREATED, \
    EVT_BULLET_REMOVED, EVT_ENTER_ELEVATOR, EVT_EXIT_ELEVATOR, EVT_EXIT_CREATED, \
    EVT_MUSIC_ACTION, EVT_MUSIC_TENSIONA, EVT_MUSIC_TENSIONB, EVT_USE_OBJECT_CREATED, \
    EVT_BULLET_COLLIDE_WALL, EVT_ROPE_CREATED, EVT_EXPLOSION_CREATED, EVT_HERO_FACING_CHANGED, EVT_COMMS_EMIT, \
    LAYER_EXIT, LAYER_HERO, LAYER_FLOOR, LAYER_HERO_ARM, LAYER_EXIT_SHIELDS, EVT_WEAPON_SLO1_SWITCHED
from pyknic.mathematics import Vec3 as Vec, Point3 as Point
# todo
from pyknic.pyknic_pygame.spritesystem import Camera, DefaultRenderer, VectorSprite
from pyknic.timing import Scheduler
from pyknic.tweening import Tweener

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Client(object):
    img_cache = {}

    def __init__(self, music_player, space, start_info, event_dispatcher):
        self.scheduler = Scheduler()
        self.music_player = music_player
        self.space = space
        self.start_info = start_info
        self.event_dispatcher = event_dispatcher
        self._debug_render = False
        self.cam = Camera((0, 0, settings.screen_width, settings.screen_height))
        self.cam.set_position(Point(settings.screen_width // 2, settings.screen_height // 2))  # avoid initial scroll
        self.renderer = DefaultRenderer()
        self.renderer.always_sort = True
        self.entity_sprite = defaultdict(dict)  # {ent: {id: sprite}}

        self.player = None

        self.tweener = Tweener()
        self.clock = pygame.time.Clock()
        self.images = {}

        self._debug_sprites = []

        # fixme: register them elsewhere?
        # TODO
        # registrations
        event_dispatcher.register_event_type(EVT_HERO_CREATED)
        event_dispatcher.register_event_type(EVT_HERO_STATE_CHANGED)
        event_dispatcher.register_event_type(EVT_BULLET_CREATED)
        event_dispatcher.register_event_type(EVT_BULLET_REMOVED)
        event_dispatcher.register_event_type(EVT_BULLET_COLLIDE_WALL)
        event_dispatcher.register_event_type(EVT_ROPE_CREATED)
        event_dispatcher.register_event_type(EVT_USE_OBJECT_CREATED)

        event_dispatcher.register_event_type(EVT_EXPLOSION_CREATED)

        event_dispatcher.register_event_type(EVT_ENTER_ELEVATOR)
        event_dispatcher.register_event_type(EVT_EXIT_ELEVATOR)

        event_dispatcher.register_event_type(EVT_MUSIC_ACTION)
        event_dispatcher.register_event_type(EVT_MUSIC_TENSIONA)
        event_dispatcher.register_event_type(EVT_MUSIC_TENSIONB)
        event_dispatcher.register_event_type(EVT_HERO_FACING_CHANGED)
        event_dispatcher.register_event_type(settings.EVT_HIT)
        event_dispatcher.register_event_type(settings.EVT_DIED)

        event_dispatcher.register_event_type(EVT_COMMS_EMIT)
        event_dispatcher.register_event_type(EVT_EXIT_CREATED)
        event_dispatcher.register_event_type(EVT_WEAPON_SLO1_SWITCHED)

        # add listeners
        event_dispatcher.add_listener(EVT_HERO_CREATED, self._on_hero_created)
        event_dispatcher.add_listener(EVT_HERO_STATE_CHANGED, self._hero_state_changed)
        event_dispatcher.add_listener(EVT_BULLET_CREATED, self._on_bullet_created)
        event_dispatcher.add_listener(EVT_BULLET_REMOVED, self._on_bullet_removed)
        event_dispatcher.add_listener(EVT_BULLET_COLLIDE_WALL, self._on_bullet_wall)
        event_dispatcher.add_listener(EVT_ROPE_CREATED, self._on_rope_created)
        event_dispatcher.add_listener(EVT_USE_OBJECT_CREATED, self._on_use_object_created)
        event_dispatcher.add_listener(EVT_EXPLOSION_CREATED, self._on_explode_created)
        event_dispatcher.add_listener(EVT_HERO_FACING_CHANGED, self._on_hero_facing_changed)
        event_dispatcher.add_listener(settings.EVT_HIT, self._on_bullet_hit)
        event_dispatcher.add_listener(settings.EVT_DIED, self._on_player_died)
        event_dispatcher.add_listener(EVT_COMMS_EMIT, self._on_comms_emit)
        event_dispatcher.add_listener(EVT_EXIT_CREATED, self._on_exit_created)
        event_dispatcher.add_listener(EVT_WEAPON_SLO1_SWITCHED, self._on_weapon_switch)

        self.sfx_handler = SfxHandler(music_player)
        self.sfx_handler.register_events(event_dispatcher)

        self._load_gfx_resources()
        self._make_info_hud()
        self._mouse_spr = sprites.AnimatedSprite(self.images["mouse-shadow"].data, Point(0, 0, 0), self.scheduler,
                                                 z_layer=settings.LAYER_MOUSE)
        self.renderer.add_sprite(self._mouse_spr)
        self._mouse_aim = sprites.AnimatedSprite(self.images["mouse-aim"].data, Point(0, 0, 0), self.scheduler,
                                                 z_layer=settings.LAYER_MOUSE)
        self.renderer.add_sprite(self._mouse_aim)


        pygame.mouse.set_visible(False)
        # TODO
        # self.images["locked"] = pygame.image.load(path.join(gfx_path, image_names["locked"])).convert_alpha()

        # TODO
        # listeners
        # event_dispatcher.add_listener(EVT_UI_SWITCHED_TO_SELECTION_MOUSE_MODE, self.on_switch_to_selection_mode)

        # TODO
        # buttons

    def init(self):
        # Start up main game music loop
        self.music_player.fill_music_carousel([resource_sfx.songs[0]])
        self.music_player.start_music_carousel()

    def resume(self):
        # Start up main game music loop
        self.music_player.fill_music_carousel([resource_sfx.songs[0]])
        self.music_player.start_music_carousel()

    def exit(self):
        self.entity_sprite.clear()
        self.renderer.clear()
        spritesystem.Sprite._dirty_sprites.clear()  # hack: needed to clear any dirty sprites

    def _load_gfx_resources(self):
        if Client.img_cache:  # load resources only once
            self.images = image_names
            return
        for k, n in image_names.items():
            # p = path.join(gfx_path, n)
            # img = pygame.image.load(p).convert_alpha()
            # img = pygame.transform.rotozoom(img, 0.0, settings.widget_scale)
            # self.images[k] = img
            self.images[k] = n  # fixme: avoid this by using image_names directly everywhere?

            if isinstance(n, resource_gfx.SpriteSheet):
                img = Client.img_cache.get(n.path_to_file, None)
                if img is None:
                    img = pygame.image.load(n.path_to_file).convert_alpha()
                    Client.img_cache[n.path_to_file] = img
                n.data = []
                r = pygame.Rect((0, 0), n.tile_size)
                # iw, ih = img.get_size()
                # x_count = iw // r.width  # alternate way to find row and column counts
                # y_count = ih // r.height
                for y in range(0, n.row_count):
                    for x in range(0, n.column_count):
                        area = r.move(x * r.width, y * r.height)
                        s = pygame.Surface(r.size, flags=pygame.SRCALPHA)
                        s.blit(img, (0, 0), area=area)
                        # todo: cache this images too? which key?
                        n.data.append(s)
            elif isinstance(n, resource_gfx.Image):
                img = Client.img_cache.get(n.path_to_file, None)
                if img is None:
                    img = pygame.image.load(n.path_to_file).convert_alpha()
                    Client.img_cache[n.path_to_file] = img
                n.data = img
            elif isinstance(n, resource_gfx.DirectoryAnimation):
                import glob
                pathname = os.path.join(n.path_to_dir, "*.{0}".format(n.extension))
                file_names = glob.glob(pathname)
                file_names.sort()  # todo: does this always sort correctly?
                n.data = []
                for file_name in file_names:
                    img = Client.img_cache.get(file_name, None)
                    if img is None:
                        img = pygame.image.load(file_name).convert_alpha()
                        Client.img_cache[file_name] = img
                    n.data.append(img)
            elif isinstance(n, resource_gfx.FileList):
                n.data = []
                for name in n.names:
                    file_name = os.path.join(n.path_to_dir, name)
                    img = Client.img_cache.get(file_name, None)
                    if img is None:
                        img = pygame.image.load(file_name).convert_alpha()
                        Client.img_cache[file_name] = img
                    n.data.append(img)
            elif isinstance(n, resource_gfx.FakeImage):
                key = (n.width, n.height, n.fill_color, n.fill_args)
                img = Client.img_cache.get(key, None)
                if img is None:
                    img = pygame.Surface((n.width, n.height), flags=pygame.SRCALPHA)
                    img.fill(n.fill_color)
                    for a in n.fill_args:
                        img.fill(*a)
                    Client.img_cache[key] = img
                n.data = img
        self.images[settings.KIND_QUEST_ROOM] = self.images[settings.KIND_STAIRWELL_EXIT]
        image_names[settings.KIND_QUEST_ROOM] = image_names[settings.KIND_STAIRWELL_EXIT]

    def _make_info_hud(self):
        # TODO: feed live mission info to IMFHUD
        # TODO: remove these messages when we get real-time chatter working
        self.hud = IMFHUD('Camouflage Flamingo', 'Snickers to the Angry Man, Floor 2')
        # self.hud.add_chatter('Go to the lobby and get the dozen eggs from the spike-haired courier,' +
        #                      ' then to floor 80 to drop the eggs,' +
        #                      ' then to floor 31 to get the flamethrower,' +
        #                      ' then return to the lobby to fry the eggs.')
        # self.hud.add_chatter('GET IN THE CHOPPA! GET IN THE CHOPPA NOWWW!')

    def _make_debug_hud(self):
        # todo
        pass

    def handle_events(self):
        events = pygame.event.get()
        actions, unmapped = gameplay_event_map.get_actions(events)
        for action, extra in actions:
            if action != ACTION_HERO_AIM:
                logger.debug('action {0}, {1}', action, extra)

            if action == ACTION_TOGGLE_DEBUG_RENDER:  # this isn't an action actually
                self._debug_render = not self._debug_render
                self._debug_render_changed()

            if action == ACTION_HERO_AIM:
                # convert to world coordinates
                # event.pos, event.rel, event.buttons
                pos, rel, buttons = extra
                world_pos = self.cam.screen_to_world_comp(pos[0], pos[1])

                extra = pos, rel, buttons, world_pos
                self._mouse_spr.position.x = world_pos.x
                self._mouse_spr.position.y = world_pos.y + 75  # this is not accurate... would need an object in world with collisions
                self._mouse_aim.position.x = world_pos.x
                self._mouse_aim.position.y = world_pos.y

            self.event_dispatcher.fire(EVT_ACTION, action, extra)  # game logic input

        for event in unmapped:

            logger.debug("unmapped event: {0}", event)

            if event.type == music_ended_pygame_event:
                logger.info("music ended event!")
                self.music_player.on_music_ended()

    def update_step(self, delta, sim_time, *args):
        self.handle_events()
        self.tweener.update(delta)
        self.update_hud()
        self.scheduler.update(delta, sim_time)
        settings.start_info.mission_playbook.update(delta)

    def update_hud(self):
        self.hud.update()
        try:
            self.hud.set_coords(self.player.map_floor_num, self.player.map_room_num)
            weapon = self.player.weapon_system.current_weapon
            self.hud.set_weapon(weapon.__class__.__name__)
            utility = self.player.weapon_system.current_weapon_secondary
            self.hud.set_utility(utility.__class__.__name__)
            self.hud.set_location(self.start_info.location)
            self.hud.set_health(self.player.health)
        except:
            self.hud.set_coords(self.player.floor_num, self.player.room_num)
            self.hud.set_weapon(self.player.weapon)
            self.hud.set_utility(self.player.utility)
            self.hud.set_location(self.start_info.location)
            self.hud.set_health(self.player.health)



    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        self.renderer.draw(screen, self.cam, settings.back_ground_color, False)  # disable interpolation
        self.hud.draw(screen)

        if do_flip:
            pygame.display.flip()
        self.clock.tick()

    def _debug_render_changed(self):
        if self._debug_render:
            self._debug_sprites = [VectorSprite(Vec(1, 0, 0) * 50, Point(0, 0, 0), (255, 0, 0), z_layer=1000),
                                   VectorSprite(Vec(0, 1, 0) * 50, Point(0, 0, 0), (0, 255, 0), z_layer=1000),
                                   VectorSprite(Vec(0, 0, 1) * 50, Point(0, 0, 0), (0, 0, 255), z_layer=1000)]

            # self._debug_hud_sprites = [VectorSprite(Vec(1, 0, 0) * 50, Point(0, 0, 0), (255, 0, 0)),
            #                            VectorSprite(Vec(0, 1, 0) * 50, Point(0, 0, 0), (0, 255, 0)),
            #                            VectorSprite(Vec(0, 0, 1) * 50, Point(0, 0, 0), (0, 0, 255))]

            # todo
            # for ent, spr in self.entity_sprite.items():
            #     if isinstance(spr, TrackingSprite):
            #         radius = getattr(ent, "radius", None)
            #         if radius:
            #             self._debug_sprites.append(RadiusSprite(ent, LAYER_TOWER + 1, True, ent.radius))
            #
            # for spr in self._debug_hud_sprites:
            #     spr.z_layer = LAYER_MOUSE_SPRITE + 1

            self.renderer.add_sprites(self._debug_sprites)
            self.debug_hud_on = True
        else:
            self.renderer.remove_sprites(self._debug_sprites)
            self._debug_sprites = []
            self.debug_hud_on = False

    # noinspection PyTypeChecker
    def _on_hero_created(self, the_hero):
        n = self.images[(the_hero.kind, 0)]
        spr = sprites.AnimatedSprite(n.data[-2:], the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero][hero.Crouching] = spr
        self.entity_sprite[the_hero][hero.Cowering] = spr
        self.renderer.add_sprite(spr)
        spr.visible = False

        n = self.images[(the_hero.kind, 1)]
        spr = sprites.AnimatedSprite(n.data, the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero][hero.Jump] = spr
        self.entity_sprite[the_hero][hero.Falling] = spr  # TODO
        self.renderer.add_sprite(spr)
        spr.visible = False

        n = self.images[(the_hero.kind, 2)]
        spr = sprites.AnimatedSprite(n.data, the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero][hero.Standing] = spr
        self.renderer.add_sprite(spr)
        spr.visible = False

        n = self.images[(the_hero.kind, 3)]
        spr = sprites.AnimatedSprite(n.data, the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero][hero.Walking] = spr
        self.renderer.add_sprite(spr)
        spr.visible = False

        n = self.images[(the_hero.kind, 8)]
        spr = sprites.AnimatedSprite(n.data, the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero][hero.Climbing] = spr
        self.renderer.add_sprite(spr)
        spr.visible = False

        self.entity_sprite[the_hero][the_hero.state_machine.current_state].visible = True

        # arm
        n = self.images[(the_hero.kind, 4)]
        spr = sprites.AnimatedSprite(n.data, the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero.arm_id][hero.Jump] = spr
        self.renderer.add_sprite(spr)
        spr.sub_layer = LAYER_HERO_ARM
        spr.visible = False

        n = self.images[(the_hero.kind, 5)]
        spr = sprites.AnimatedSprite(n.data, the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero.arm_id][hero.Standing] = spr
        self.entity_sprite[the_hero.arm_id][hero.Falling] = spr  # TODO
        self.renderer.add_sprite(spr)
        spr.sub_layer = LAYER_HERO_ARM
        spr.visible = False

        n = self.images[(the_hero.kind, 6)]
        spr = sprites.AnimatedSprite(n.data, the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero.arm_id][hero.Walking] = spr
        self.renderer.add_sprite(spr)
        spr.sub_layer = LAYER_HERO_ARM
        spr.visible = False

        n = self.images[(the_hero.kind, 7)]
        spr = sprites.AnimatedSprite(n.data, the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero.arm_id]["shoot"] = spr
        self.renderer.add_sprite(spr)
        spr.sub_layer = LAYER_HERO_ARM
        spr.visible = False

        n = self.images[(the_hero.kind, 9)]
        spr = sprites.AnimatedSprite(n.data, the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero.arm_id][hero.Climbing] = spr
        self.renderer.add_sprite(spr)
        spr.sub_layer = LAYER_HERO_ARM
        spr.visible = False

        n = self.images[(the_hero.kind, 10)]
        spr = sprites.AnimatedSprite(n.data[-2:], the_hero.position, self.scheduler, n.fps, z_layer=LAYER_HERO,
                                     tracked_entity=the_hero)
        self.entity_sprite[the_hero.arm_id][hero.Crouching] = spr
        self.entity_sprite[the_hero.arm_id][hero.Cowering] = spr
        self.renderer.add_sprite(spr)
        spr.sub_layer = LAYER_HERO_ARM
        spr.visible = False

        self.entity_sprite[the_hero.arm_id][the_hero.state_machine.current_state].visible = True

        if the_hero.kind == settings.KIND_HERO:
            self.cam.track = the_hero  # this moves the entire level around

        for w in the_hero.weapon_system.arsenal:
            if w.kind & settings.KIND_WEAPON_ROCKET_LAUNCHER or w.kind & settings.KIND_WEAPON_BLASTER:
                frame = self.images[w.kind].data
                # position = the_hero.position.clone()
                # position.z -= the_hero.height / 2.0  # source position of bullet
                # position.x += settings.hero_bullet_offset * (-1 if the_hero.facing_left else 1)

                spr = AnimatedSprite([frame], the_hero.position, self.scheduler, z_layer=LAYER_HERO)
                spr.anchor = Vec(settings.hero_bullet_offset * (-1 if the_hero.facing_left else 1), the_hero.height / 2.0, 0)
                spr.sub_layer = settings.LAYER_HERO_WEAPON
                self.entity_sprite[w][0] = spr
                spr.visible = the_hero.weapon_system.current_weapon == w
                self.renderer.add_sprite(spr)

    def _on_exit_created(self, room, rect):
        # do not create an object at the drop point
        if room.name == 'Q1':
            return

        point = Point(*rect.midbottom)

        if room.kind == settings.KIND_ELEVATOR_EXIT:
            n = self.images[room.kind]
            spr = AnimatedSprite(n.data, point, self.scheduler, n.fps, LAYER_EXIT)
            spr.stop()
            self.renderer.add_sprite(spr)
            pos = point + Vec(87, -125)
            self._add_shield(room.distant_end.floor_num, room.distant_end.name, pos)
            return
        elif room.kind == settings.KIND_STAIRWELL_EXIT:
            n = self.images[room.kind]
            choice = random.choice((1, 2))  # 2 is open door
            frame = n.data[choice]
            spr = AnimatedSprite([frame], point, self.scheduler, 0, LAYER_EXIT)
            spr.stop()
            self.renderer.add_sprite(spr)
            pos = point + Vec(70, -125)
            self._add_shield(room.distant_end.floor_num, room.distant_end.name, pos)
            return
        elif room.kind == settings.KIND_QUEST_ROOM:
            n = self.images[room.kind]
            choice = random.choice((1, 2))  # 2 is open door
            frame = n.data[choice]
            spr = AnimatedSprite([frame], point, self.scheduler, 0, LAYER_EXIT)
            spr.stop()
            self.renderer.add_sprite(spr)
            pos = point + Vec(70, -125)
            self._add_shield(room.floor_num, room.name, pos)
            return


        img = pygame.Surface((settings.room_width_px, settings.floor_height_px // 2), pygame.SRCALPHA)
        img.fill(pygame.Color('palegreen'))
        img.fill((255, 255, 255, 100), None, pygame.BLEND_RGBA_MULT)
        msg = "{0}\nr{1},f{2}\n{3} {4} ".format(room.__class__.__name__, room.room_num, room.floor_num, room.name, room.map_id)
        pygametext.draw(msg, surf=img, midtop=img.get_rect().midtop,
                        fontname='VeraBd.ttf', fontsize=20, color='black')
        sprite = GummExitSprite(img, point, z_layer=LAYER_EXIT)
        sprite.visible = True
        sprite.anchor = 'midbottom'
        self.renderer.add_sprite(sprite)

    def _on_weapon_switch(self, prev_weapon, current_weapon):
        if prev_weapon in self.entity_sprite:
            self.entity_sprite[prev_weapon][0].visible = False
            self.entity_sprite[current_weapon][0].visible = True

            if current_weapon in self.entity_sprite:
                self.entity_sprite[current_weapon][0].flipped_x = not self.player.facing_left

    def add_walkable_areas(self, areas):
        sprites = [RectSprite(area.rect, z_layer=LAYER_FLOOR) for area in areas]
        self.renderer.add_sprites(sprites)
        h = 181
        back_walls = [RectSprite(pygame.Rect(area.rect.x, area.rect.y - h, area.rect.w, h),
                                 color=(148, 148, 148), z_layer=settings.LAYER_BACK_WALL)
                      for area in areas]
        for w in back_walls:
            w.anchor = 'midbottom'
        self.renderer.add_sprites(back_walls)

    # noinspection PyUnresolvedReferences
    def _hero_state_changed(self, the_hero):
        if the_hero in self.entity_sprite:
            self.entity_sprite[the_hero][the_hero.state_machine.previous_state].visible = False
            self.entity_sprite[the_hero][the_hero.state_machine.current_state].visible = True
            self.entity_sprite[the_hero][the_hero.state_machine.current_state].flipped_x = not the_hero.facing_left

        if the_hero.arm_id in self.entity_sprite:
            self.entity_sprite[the_hero.arm_id][the_hero.state_machine.previous_state].visible = False
            self.entity_sprite[the_hero.arm_id][the_hero.state_machine.current_state].visible = True
            self.entity_sprite[the_hero.arm_id][
                the_hero.state_machine.current_state].flipped_x = not the_hero.facing_left

        weapon = the_hero.weapon_system.current_weapon
        if weapon in self.entity_sprite:
            self.entity_sprite[weapon][0].flipped_x = not the_hero.facing_left

    def _on_hero_facing_changed(self, the_hero):
        if the_hero in self.entity_sprite:
            self.entity_sprite[the_hero][the_hero.state_machine.current_state].flipped_x = not the_hero.facing_left
            # logger.debug("client {0} facing set {1}", the_hero.kind, the_hero.facing_left)

        if the_hero.arm_id in self.entity_sprite:
            self.entity_sprite[the_hero.arm_id][
                the_hero.state_machine.current_state].flipped_x = not the_hero.facing_left

        weapon = the_hero.weapon_system.current_weapon
        if weapon in self.entity_sprite:
            self.entity_sprite[weapon][0].flipped_x = not the_hero.facing_left


    def _on_bullet_hit(self, bullet, player):
        n = self.images["hurt"]
        spr = sprites.AnimatedSprite(n.data, bullet.position.clone(), self.scheduler, n.fps)
        spr.event_animation_end += self._on_remove_sprite_fom_renderer
        self.renderer.add_sprite(spr)

        self._on_bullet_removed(bullet)

    def _on_player_died(self, player):
        if player in self.entity_sprite:
            for spr in self.entity_sprite[player].values():
                spr.stop()
                spr.visible = False
                self.renderer.remove_sprite(spr)
            del self.entity_sprite[player]
        if player.arm_id in self.entity_sprite:
            for spr in self.entity_sprite[player.arm_id].values():
                spr.stop()
                spr.visible = False
                self.renderer.remove_sprite(spr)
            del self.entity_sprite[player.arm_id]

        for w in player.weapon_system.arsenal:
            if w in self.entity_sprite:
                for spr in self.entity_sprite[w].values():
                    spr.stop()
                    spr.visible = False
                    self.renderer.remove_sprite(spr)
                del self.entity_sprite[w]

    def _on_comms_emit(self, message):
        self.hud.add_chatter(message)

    def _on_bullet_created(self, bullet):
        n = self.images[bullet.kind]
        spr = sprites.AnimatedSprite(n.data, bullet.position, self.scheduler, n.fps)
        spr.sub_layer = 1
        spr.rotation = -bullet.velocity.angle
        logger.debug("Add bullet: {0}", bullet.position)
        self.entity_sprite[bullet][0] = spr
        self.renderer.add_sprite(spr)

    def _on_bullet_removed(self, bullet):
        if bullet in self.entity_sprite:
            self.renderer.remove_sprite(self.entity_sprite[bullet][0])
            del self.entity_sprite[bullet]

    def _on_explode_created(self, explosion):
        n = self.images[explosion.kind]
        spr = sprites.AnimatedSprite(n.data, explosion.position, self.scheduler, n.fps)
        spr.rotation = 0.0
        spr.event_animation_end += self._on_remove_sprite_fom_renderer
        logger.debug("Add explosion: {0}", explosion.position)
        self.renderer.add_sprite(spr)

    def _on_remove_sprite_fom_renderer(self, spr):
        self.renderer.remove_sprite(spr)

    def _on_bullet_wall(self, bullet):
        if bullet.kind == settings.KIND_BLASTER_BULLET:
            n = self.images["wall_hit"]
            spr = sprites.AnimatedSprite(n.data, bullet.position.clone(), self.scheduler, n.fps)
            spr.event_animation_end += self._on_remove_sprite_fom_renderer
            self.renderer.add_sprite(spr)

        self._on_bullet_removed(bullet)

    def _on_rope_created(self, rope):
        spr = RopeSprite(rope.position, rope.length)
        self.renderer.add_sprite(spr)

    def _on_use_object_created(self, use_object):
        # todo: create sprite with right gfx for use_object.use_object_id
        logger.error('_on_use_object_created: Not Yet Implemented')

    def _add_shield(self, floor_num, name, pos):
        msg = "{0}:{1}".format(floor_num, name)
        img = pygametext.getsurf(msg, background=(210, 210, 210), fontname='VeraBd.ttf', fontsize=10, color='red')
        spr = AnimatedSprite([img], pos, self.scheduler, z_layer=LAYER_EXIT_SHIELDS)
        self.renderer.add_sprite(spr)


logger.debug("imported")
