# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'sfx_handler.py' is part of pw---__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

from gamelib import settings
from gamelib.client import resource_sfx
# from gamelib.eventing import event_dispatcher
from gamelib.settings import EVT_SOUND_STARTED, EVT_BULLET_CREATED, EVT_MUSIC_ACTION, EVT_MUSIC_TENSIONA, \
    EVT_MUSIC_TENSIONB, EVT_ENTER_ELEVATOR, EVT_EXIT_ELEVATOR, EVT_EXPLOSION_CREATED, \
    KIND_GRENADE, KIND_ROCKET, KIND_ROPE_BULLET
from pyknic.pyknic_pygame import sfx

import pygame

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class SfxHandler(object):
    def __init__(self, music_player):
        self.music_player = music_player
        self.sfx_player = sfx.SoundPlayer(1.0)
        self.sfx_player.setup_channels(settings.MIXER_NUM_CHANNELS, settings.MIXER_RESERVED_CHANNELS)
        # self.sfx_player.load(gamelib.client.resource_sfx.sfx_data)
        self.sfx_player.load(resource_sfx.sfx_data)

        self.music_type = self.music_player.keep_last_musictype
        self.last_position = self.music_player.keep_last_position
        self.where = 'main'

    def register_events(self, event_dispatcher):
        # TODO
        event_dispatcher.register_event_type(EVT_SOUND_STARTED)
        event_dispatcher.add_listener(EVT_SOUND_STARTED, self.sfx_player.handle_message)
        event_dispatcher.add_listener(EVT_BULLET_CREATED, self._on_gunfire)
        event_dispatcher.add_listener(EVT_EXPLOSION_CREATED, self._on_explode)

        event_dispatcher.add_listener(EVT_MUSIC_ACTION, self._on_music_event)
        event_dispatcher.add_listener(EVT_MUSIC_TENSIONA, self._on_music_event)
        event_dispatcher.add_listener(EVT_MUSIC_TENSIONB, self._on_music_event)

        event_dispatcher.add_listener(EVT_ENTER_ELEVATOR, self._on_elevator_event)
        event_dispatcher.add_listener(EVT_EXIT_ELEVATOR, self._on_elevator_event)

        # event_dispatcher.add_listener(EVT_XXX, self._on_xxx)  # register for event XXX here

    def _on_gunfire(self, bullet):
        if bullet.params.bullet_kind == KIND_GRENADE or bullet.params.bullet_kind == KIND_ROPE_BULLET:
            self.sfx_player.handle_message(EVT_SOUND_STARTED, resource_sfx.SFX_THROW, None)
        elif bullet.params.bullet_kind == KIND_ROCKET:
            self.sfx_player.handle_message(EVT_SOUND_STARTED, resource_sfx.SFX_ROCKET, None)
        else:
            self.sfx_player.handle_message(EVT_SOUND_STARTED, resource_sfx.SFX_GUNFIRE1, None)
    
    def _on_explode(self, event_type, *args):
        self.sfx_player.handle_message(EVT_SOUND_STARTED, resource_sfx.SFX_EXPLODE, None)
        """
        # testing: dynamic music changes
        if self.where == 'main':
        	self.where = 'elevator'
        	event_dispatcher.fire(EVT_ENTER_ELEVATOR, EVT_ENTER_ELEVATOR, None)
        elif self.where == 'elevator':
            self.where = 'main'
            event_dispatcher.fire(EVT_EXIT_ELEVATOR, EVT_EXIT_ELEVATOR, None)
        elif self.music_type == 'tensionA':
            self.music_type = 'tensionB'
            event_dispatcher.fire(EVT_MUSIC_TENSIONB, EVT_MUSIC_TENSIONB, None)
        elif self.music_type == 'tensionB':
            self.music_type = 'action'
            event_dispatcher.fire(EVT_MUSIC_ACTION, EVT_MUSIC_ACTION, None)
        """

    def _on_music_event(self, event_type, *args):
        if event_type == EVT_MUSIC_ACTION:
            self.music_type = 'main'
            self.music_player.fill_music_carousel([resource_sfx.songs[0]])
            self.music_player.start_music_carousel()
        elif event_type == EVT_MUSIC_TENSIONA:
            self.music_type = 'tensionA'
            #self.music_player.fill_music_carousel([resource_sfx.songs[1], resource_sfx.songs[3]])
            self.music_player.fill_music_carousel([resource_sfx.songs[3]])
            self.music_player.start_music_carousel()
        elif event_type == EVT_MUSIC_TENSIONB:
            self.music_type = 'tensionB'
            self.music_player.fill_music_carousel([resource_sfx.songs[2]])
            self.music_player.start_music_carousel()
        self.music_player.keep_last_musictype = self.music_type

    def _on_elevator_event(self, event_type, *args):
        if event_type == EVT_ENTER_ELEVATOR:
            self.last_position = self.last_position + pygame.mixer.music.get_pos()/1000.0
            if self.music_type == 'main':
                self.last_position = self.last_position % 115.0
            elif self.music_type == 'tensionA':
                self.last_position = self.last_position % 55.0
            elif self.music_type == 'tensionB':
                self.last_position = self.last_position % 23.3333
            self.sfx_player.handle_message(EVT_SOUND_STARTED, resource_sfx.SFX_ELEVATOR_DOOR, None)
            self.music_player.fill_music_carousel([resource_sfx.songs[6]])
            self.music_player.start_music_carousel()
        elif event_type == EVT_EXIT_ELEVATOR:
            self.sfx_player.handle_message(EVT_SOUND_STARTED, resource_sfx.SFX_ELEVATOR_DOOR, None)
            if self.music_type == 'main':
                self.music_player.fill_music_carousel([resource_sfx.songs[0]])
            elif self.music_type == 'tensionA':
                #self.music_player.fill_music_carousel([resource_sfx.songs[1], resource_sfx.songs[3]])
                self.music_player.fill_music_carousel([resource_sfx.songs[3]])
            elif self.music_type == 'tensionB':
                self.music_player.fill_music_carousel([resource_sfx.songs[2]])
            self.music_player.start_music_carousel(start=self.last_position)
            self.music_player.keep_last_position = self.last_position
            self.music_player.keep_last_musictype = self.music_type

    def _on_xxx(self, event_type, *args):
        # todo: instead of firing another not needed event I have added a todo in the sfx_player to add some methods
        # event_dispatcher.fire(EVT_SOUND_STARTED, resource_sfx.SFX_XXX, None)
        pass


logger.debug("imported")
