# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'sprites.py' is part of pw-__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

import pygame

from pyknic.animation import Animation
from pyknic.mathematics import Point3
from pyknic.pyknic_pygame.spritesystem import Sprite

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class GummExitSprite(Sprite):
    pass


class RectSprite(Sprite):
    draw_special = True
    dirty_update = False

    def __init__(self, area, color=(100, 100, 100), z_layer=0):
        pos = Point3(area.centerx, area.centery)
        Sprite.__init__(self, None, pos, z_layer=z_layer)
        self.area = area
        self.color = color

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        screen_x, screen_y = cam.world_to_screen(self.position).as_xy_tuple(int)
        screen_rect = self.area.move(screen_x - self.area.centerx, screen_y - self.area.centery)
        return pygame.draw.rect(surf, self.color, screen_rect)


class RopeSprite(Sprite):
    draw_special = True
    dirty_update = False

    def __init__(self, position, rope_length, color=(10, 10, 10), z_layer=0):
        Sprite.__init__(self, None, position, z_layer=z_layer)
        self.color = color
        self.rope_string = pygame.Rect(0, 0, 10, rope_length)

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        screen_x, screen_y = cam.world_to_screen(self.position).as_xy_tuple(int)
        # screen_rect = self.rope_string.move(screen_x - self.rope_string.centerx, screen_y - self.rope_string.centery)
        self.rope_string.midbottom = (screen_x, screen_y)
        return pygame.draw.rect(surf, self.color, self.rope_string)


class AnimatedSprite(Sprite, Animation):

    def __init__(self, frames, position, scheduler, fps=0, z_layer=0, anchor='midbottom', start_frame_idx=0,
                 tracked_entity=None):
        if type(frames) is not list:
            frames = [frames]
        Animation.__init__(self, start_frame_idx, len(frames), fps, scheduler)
        Sprite.__init__(self, frames[start_frame_idx], position, anchor, z_layer)
        self.frames = frames
        self.event_index_changed += self._on_change_frame
        self.start()
        self.tracked_entity = tracked_entity

    def _on_change_frame(self, *args):
        self.set_image(self.frames[self.current_index])

    def on_visible_changed(self):
        Sprite.on_visible_changed(self)
        if self.visible:
            self.start()
        else:
            self.stop()


logger.debug("imported")
