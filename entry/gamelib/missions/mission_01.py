# -*- coding: utf-8 -*-

import logging

from gamelib import settings
from gamelib.entities.building import create_and_load_building
from gamelib.mission_playbook import MissionPlaybook,  Cine, ObjectiveGoToRoom, ObjectiveSeeMan, ObjectiveStopMan, \
    ObjectiveGetMan, ObjectiveGetInfo, ObjectiveGetItem, ObjectiveUseItem


logger = logging.getLogger(__name__)
logger.debug("importing...")


building_data = """
MISSION 01
  | v  _  _  _  _  _  _  _  _  _10_  _  _  _  _  _  _  _  _20_  _  _  _  _  _  _  _  _  _30_  _  _  _  _  _  _  _  _  _40
04|          S1          Q1 E1
03|    Q3    S1    S2
02|    Q2    S3    S2
01|          S3             E1 L1
  | ^  _  _  _  _  _  _  _  _  _10_  _  _  _  _  _  _  _  _20_  _  _  _  _  _  _  _  _  _30_  _  _  _  _  _  _  _  _  _40
"""


def load(start_info, event_dispatcher):

    building = create_and_load_building(4, 10, building_data)
    playbook = MissionPlaybook(start_info, event_dispatcher)

    # Q1. Mission start.
    ca = Cine(["Comms: Get to floor 2 ASAP. See the man."])
    q = ObjectiveGetInfo(playbook, 'Q1', cine_a=ca)
    playbook.add_objective(q)

    # Q2. See the man.
    ca = Cine(["Man: Ah. You look exactly as they described!",
               "Man: Here, this is what you need.",
               "Info: You've acquired a key.",
               "Hero: The man gave it up.",
               "Comms: Get to floor 3. Find out what's in that room.",
               # fixme: had to put this kludge to display the final message; still not working 100%
               ""])
    q = ObjectiveSeeMan(playbook, 'Q2', need_target='OBJ_MAN', cine_a=ca)
    playbook.add_objective(q)

    # Q3. Get the tape.
    ca = Cine(["Info: You found a tape labeled \"Hero's Mix\".",
               "Hero: They're playing my tune. I got the package.",
               "Comms: Return to the roof for extract...",
               "Comms: Cancel that order.",
               "Comms: Our chopper was taken out by a self-destructing message.",
               "Comms: GET TO THE LOBBY NOWWW. That's on floor 1, by the way.",
               "Comms: And there's an elevator at the right end of floor 4.",
               # fixme: had to put this kludge to display the final message; still not working 100%
               ""])
    q = ObjectiveGetItem(playbook, 'Q3', need_target='OBJ_TAPE', cine_a=ca)
    playbook.add_objective(q)

    # L1. Game over.
    ca = Cine(["Comms: You got company. Go!",
               "Hero: You know... a guy's been doing this as long as I have.",
               "Hero: He gets to thinkin'.",
               "Hero: After this mission, I've seen everything.",
               "Hero: I'm gonna hang it up.",
               "Comms: Like last time. You'll be back.",
               "Hero: Hasta la vista, baby.",
               # fixme: had to put this kludge to display the final message; still not working 100%
               ""])
    q = ObjectiveGetInfo(playbook, 'L1', cine_a=ca)
    playbook.add_objective(q)

    start_info.building = building
    start_info.mission_playbook = playbook
    start_info.floor_num = start_info.building.rooms_by_name['Q1'].floor_num  # better than hard coding a number
    start_info.room_num = start_info.building.rooms_by_name['Q1'].room_num  # better than hard coding a number
    start_info.location = settings.LOCATION_FRONT

    return start_info


logger.debug("imported")
