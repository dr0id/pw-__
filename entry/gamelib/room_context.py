# -*- coding: utf-8 -*-

import logging

import pygame

from gamelib import settings, pygametext
from gamelib.client import sprites
from gamelib.client.sprites import GummExitSprite, AnimatedSprite
from gamelib.common_context import CommonContext
from pyknic import tweening
from pyknic.mathematics import Point3 as Point, Point3
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.timing import Timer, Scheduler
from pyknic.tweening import Tweener

from gamelib.settings import music_ended_pygame_event


logger = logging.getLogger(__name__)
logger.debug("importing...")


class DummyPlayer(object):
    def __init__(self, fn, rn, weapon='Blaster', utility='Grenade', room_name='Room'):
        self.floor_num = fn
        self.room_num = rn
        self.weapon = weapon
        self.utility = utility
        self.location = room_name
        self.health = 100


class QuestRoomContext(TimeSteppedContext):

    def __init__(self, client, space, start_info, event_dispatcher):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.start_info = start_info
        self.client = client
        self.screen = None
        self.scheduler = Scheduler()
        self.timer = Timer(self.scheduler)
        self.tweener = Tweener()
        self.player = None

    def bug_out(self):
        logger.info('QuestRoomContext> bugging out')
        self.start_info.change_context = settings.LOCATION_RETURN
        self.start_info.mission_playbook.advance_objective()
        si = self.start_info
        fn, rn = si.floor_num, si.room_num
        room = si.building.get_room(fn, rn)
        if room.name == 'L1':
            si.mission_complete = True
        self.pop()

    def enter(self):
        # TODO
        logger.info('QuestRoomContext> enter')
        self.screen = pygame.display.get_surface()
        self.player = DummyPlayer(self.start_info.floor_num, self.start_info.room_num, room_name='Quest Room')
        self.client.player = self.player
        self.start_info.mission_playbook.trigger_use()
        frames = self.client.images["quest_room"].data
        pos = Point3(settings.screen_width / 2, settings.screen_height / 2, 0)
        spr = sprites.AnimatedSprite(frames,pos, self.scheduler)
        spr.anchor = "center"
        self.client.renderer.add_sprite(spr)


    def load_location(self):
        # TODO
        pass

    def update_step(self, delta, sim_time, *args):
        self.scheduler.update(delta, sim_time)
        self.client.update_step(delta, sim_time, *args)
        self.tweener.update(delta)
        for event in pygame.event.get():
            if event.type == music_ended_pygame_event:
                logger.info("music ended event!")
                self.client.music_player.on_music_ended()
        self.update_mission(delta)

    def update_mission(self, delta):
        playbook = self.start_info.mission_playbook
        playbook.update(delta)
        if playbook.is_objective_accomplished():
            self.bug_out()
        pygame.event.get()

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        self.screen.fill((0, 0, 0))
        if self.screen:
            self.client.draw(self.screen, do_flip, interpolation_factor)


class AmbushRoomContext(TimeSteppedContext):

    def __init__(self, client, space, start_info, event_dispatcher):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        # TODO

    def enter(self):
        # TODO
        pass

    def load_location(self):
        # TODO
        pass


class HealthRoomContext(TimeSteppedContext):

    def __init__(self, client, space, start_info, event_dispatcher):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.start_info = start_info
        self.client = client
        self.screen = None
        self.scheduler = Scheduler()
        self.timer = Timer(self.scheduler)
        self.tweener = Tweener()
        self.player = None

    def bug_out(self):
        logger.info('HealthRoomContext> bugging out')
        self.start_info.change_context = settings.LOCATION_RETURN
        self.start_info.mission_playbook.advance_objective()
        self.start_info.hero_health = settings.hero_starting_health
        self.pop()

    def enter(self):
        # TODO
        logger.info('HealthRoomContext> enter')
        self.screen = pygame.display.get_surface()
        self.player = DummyPlayer(self.start_info.floor_num, self.start_info.room_num, room_name='Health Room')
        self.client.player = self.player
        self.start_info.mission_playbook.trigger_use()

    def load_location(self):
        # TODO
        pass

    def update_step(self, delta, sim_time, *args):
        self.scheduler.update(delta, sim_time)
        self.client.update_step(delta, sim_time, *args)
        self.tweener.update(delta)
        for event in pygame.event.get():
            if event.type == music_ended_pygame_event:
                logger.info("music ended event!")
                self.client.music_player.on_music_ended()
        self.update_mission(delta)

    def update_mission(self, delta):
        playbook = self.start_info.mission_playbook
        playbook.update(delta)
        if playbook.is_objective_accomplished():
            self.bug_out()
        pygame.event.get()

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        self.screen.fill((0, 0, 0))
        if self.screen:
            self.client.draw(self.screen, do_flip, interpolation_factor)


class TreasureRoomContext(TimeSteppedContext):

    def __init__(self, client, space, start_info, event_dispatcher):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        # TODO

    def enter(self):
        # TODO
        pass

    def load_location(self):
        # TODO
        pass


logger.debug("imported")
