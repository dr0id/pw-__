# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'settings.py' is part of pw---__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Settings and constants.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

import pygame

from gamelib import pygametext
from pyknic.generators import FlagGenerator, GlobalIdGenerator
from pyknic.mathematics import Vec3
from pyknic.pyknic_pygame.eventmapper import EventMapper, ANY_MOD

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

# --------------------------------------------------------------------------------
# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
# --------------------------------------------------------------------------------
#
# This file contains the distributed default settings.
#
# Don't override settings by editing this file, else they will end up in the repo.
# Instead, see the DEBUGS section at the end of this file for instructions on
# keeping your custom settings out of the repo.
#
# --------------------------------------------------------------------------------
# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
# --------------------------------------------------------------------------------


#
#
# # ------------------------------------------------------------------------------
# # Tunables
# # ------------------------------------------------------------------------------
#
# print_fps = False
max_fps = 60
master_volume = 0.5  # min: 0.0; max: 1.0
music_volume = 1.0  # resulting level = master_volume * music_volume
sfx_volume = 1.0  # resulting level = master_volume * sfx_volume
mission_chatter_speed = 4.0  # seconds between emits
hero_starting_health = 100   # hero health at start of mission and healing rooms
log_level = [logging.WARNING, logging.DEBUG, logging.INFO][1]
fx_enable_tactical_hud_flicker = True

#
#
# # ------------------------------------------------------------------------------
# # Display
# # ------------------------------------------------------------------------------
#
# os.environ['SDL_VIDEO_CENTERED'] = '1'
#
screen_width = 1024
screen_height = 768
screen_size = screen_width, screen_height
screen_flags_presets = (
    0,
    pygame.FULLSCREEN | pygame.DOUBLEBUF,
)
screen_flags = screen_flags_presets[0]
default_fill_color = 100, 100, 100

title = "pw---__"
path_to_icon = "./data/icon.png"

# ------------------------------------------------------------------------------
# Game
# ------------------------------------------------------------------------------

start_info = None


# ------------------------------------------------------------------------------
# Sound
# ------------------------------------------------------------------------------

music_player = None

# Mixer
MIXER_FREQUENCY = 0  # default:22050
MIXER_SIZE = -16  # default: -16
MIXER_CHANNELS = 2  # default: 2 stereo or 1 mono
MIXER_BUFFER_SIZE = 16  # default: 4096

MIXER_NUM_CHANNELS = 24
MIXER_RESERVED_CHANNELS = 6  # default:

# Player
(
    channel_ants_speaking,
    channel_building,
    channel_bullet,
    channel_power,
    channel_chips,
    channel_6,
) = range(MIXER_RESERVED_CHANNELS)

# ------------------------------------------------------------------------------
# Events
# ------------------------------------------------------------------------------

GLOBAL_ID_ACTIONS = 1000
GLOBAL_ID_MISSION_OBJECTIVES = 2000

# actions
_global_id_generator = GlobalIdGenerator(GLOBAL_ID_ACTIONS)  # avoid collision with event ids

ACTION_TOGGLE_DEBUG_RENDER = _global_id_generator.next()
ACTION_QUIT = _global_id_generator.next()

ACTION_HERO_AIM = _global_id_generator.next()
ACTION_HERO_FIRE = _global_id_generator.next()
ACTION_HERO_HOLD_FIRE = _global_id_generator.next()
ACTION_HERO_MOVE_LEFT = _global_id_generator.next()
ACTION_HERO_MOVE_RIGHT = _global_id_generator.next()
ACTION_HERO_MOVE_UP = _global_id_generator.next()
ACTION_HERO_MOVE_DOWN = _global_id_generator.next()
ACTION_HERO_STOP_LEFT = _global_id_generator.next()
ACTION_HERO_STOP_RIGHT = _global_id_generator.next()
ACTION_HERO_STOP_UP = _global_id_generator.next()
ACTION_HERO_STOP_DOWN = _global_id_generator.next()
ACTION_HERO_SWITCH = _global_id_generator.next()
ACTION_HERO_USE = _global_id_generator.next()
ACTION_HERO_CROUCH = _global_id_generator.next()
ACTION_HERO_STAND = _global_id_generator.next()
# ACTION_HERO_SHOOT_GRAPPLING = _global_id_generator.next()
ACTION_HERO_JUMP = _global_id_generator.next()
ACTION_HERO_FIRE_SECONDARY = _global_id_generator.next()
ACTION_HERO_HOLD_SECONDARY = _global_id_generator.next()
ACTION_HERO_SWITCH_SECONDARY = _global_id_generator.next()

gameplay_event_map = EventMapper({
    pygame.MOUSEBUTTONDOWN: {
        1: ACTION_HERO_FIRE,  # LMB
        2: ACTION_HERO_FIRE_SECONDARY,  # MMB
        3: ACTION_HERO_FIRE_SECONDARY,  # RMB
        4: ACTION_HERO_SWITCH,  # mouse scroll up(?)
        5: ACTION_HERO_SWITCH,  # mouse scroll down(?)
    },
    pygame.MOUSEBUTTONUP: {
        1: ACTION_HERO_HOLD_FIRE,
        2: ACTION_HERO_HOLD_SECONDARY,  # MMB
        3: ACTION_HERO_HOLD_SECONDARY,  # RMB
    },
    pygame.MOUSEMOTION: {
        None: ACTION_HERO_AIM,
    },
    pygame.KEYDOWN: {

        (pygame.K_F1, ANY_MOD): ACTION_TOGGLE_DEBUG_RENDER,
        (pygame.K_F4, pygame.KMOD_ALT): ACTION_QUIT,
        # hero
        (pygame.K_a, ANY_MOD): ACTION_HERO_MOVE_LEFT,
        (pygame.K_d, ANY_MOD): ACTION_HERO_MOVE_RIGHT,
        (pygame.K_w, ANY_MOD): ACTION_HERO_MOVE_UP,
        (pygame.K_s, ANY_MOD): ACTION_HERO_MOVE_DOWN,
        (pygame.K_e, ANY_MOD): ACTION_HERO_USE,
        (pygame.K_LSHIFT, ANY_MOD): ACTION_HERO_CROUCH,
        (pygame.K_RSHIFT, ANY_MOD): ACTION_HERO_CROUCH,
        (pygame.K_TAB, ANY_MOD): ACTION_HERO_SWITCH,
        # (pygame.K_q, ANY_MOD): ACTION_HERO_SWITCH_SECONDARY,
        (pygame.K_SPACE, ANY_MOD): ACTION_HERO_JUMP
    },
    pygame.KEYUP: {
        # (pygame.K_ESCAPE, ANY_MOD): ACTION_WIN_LEVEL,
        (pygame.K_a, ANY_MOD): ACTION_HERO_STOP_LEFT,
        (pygame.K_d, ANY_MOD): ACTION_HERO_STOP_RIGHT,
        (pygame.K_w, ANY_MOD): ACTION_HERO_STOP_UP,
        (pygame.K_s, ANY_MOD): ACTION_HERO_STOP_DOWN,
        # (pygame.K_q, ANY_MOD): ACTION_HERO_HOLD_SECONDARY,
        (pygame.K_LSHIFT, ANY_MOD): ACTION_HERO_STAND,
        (pygame.K_RSHIFT, ANY_MOD): ACTION_HERO_STAND,
    },
    pygame.QUIT: {None: ACTION_QUIT},

})

music_ended_pygame_event = pygame.USEREVENT

# game events
EVT_QUIT = _global_id_generator.next()
EVT_ACTION = _global_id_generator.next()  # action, extra
EVT_SOUND_STARTED = _global_id_generator.next()  # sfx resource id, mode of operation
EVT_HERO_CREATED = _global_id_generator.next()  # hero
EVT_HERO_STATE_CHANGED = _global_id_generator.next()  # hero
EVT_HERO_FACING_CHANGED = _global_id_generator.next()  # hero
EVT_BULLET_CREATED = _global_id_generator.next()  # bullet
EVT_BULLET_REMOVED = _global_id_generator.next()  # bullet
EVT_BULLET_COLLIDE_WALL = _global_id_generator.next()  # bullet
EVT_ROPE_CREATED = _global_id_generator.next()  # rope entity
EVT_OBJECT_USED = _global_id_generator.next()  # use_object_id
EVT_USE_OBJECT_CREATED = _global_id_generator.next()  # use_object
EVT_HIT = _global_id_generator.next()  # use_object
EVT_DIED = _global_id_generator.next()  # use_object
EVT_WEAPON_SLO1_SWITCHED = _global_id_generator.next()  # use_object
EVT_WEAPON_SLO2_SWITCHED = _global_id_generator.next()  # use_object

EVT_EXPLOSION_CREATED = _global_id_generator.next()  # explosion (grenade? rocket launcher?)

EVT_ENTER_ELEVATOR = _global_id_generator.next() # fire when player enters an elevator
EVT_EXIT_ELEVATOR = _global_id_generator.next() # fire when player exits an elevator

EVT_MUSIC_ACTION = _global_id_generator.next()
EVT_MUSIC_TENSIONA = _global_id_generator.next()
EVT_MUSIC_TENSIONB = _global_id_generator.next()

EVT_COMMS_EMIT = _global_id_generator.next()
EVT_EXIT_CREATED = _global_id_generator.next()

# ------------------------------------------------------------------------------
# Kinds
# ------------------------------------------------------------------------------
generator = FlagGenerator()
KIND_HERO = generator.next()
KIND_BLASTER_BULLET = generator.next()
KIND_ROCKET = generator.next()
KIND_GRENADE = generator.next()
KIND_ROPE_BULLET = generator.next()
KIND_WEAPON_BLASTER = generator.next()
KIND_WEAPON_ROCKET_LAUNCHER = generator.next()
KIND_WEAPON_GRENADE_LAUNCHER = generator.next()
KIND_WEAPON_ROPE_LAUNCHER = generator.next()
KIND_WEAPON_NONE = generator.next()
KIND_BADDY = generator.next()
KIND_WALKABLE_AREA = generator.next()
KIND_USE = generator.next()
KIND_USE_OBJECT = generator.next()
KIND_ROPE = generator.next()
KIND_EXPLOSION = generator.next()

KIND_BUILDING = generator.next()
KIND_FLOOR = generator.next()
KIND_ROOM = generator.next()
KIND_QUEST_ROOM = generator.next()
KIND_AMBUSH_ROOM = generator.next()
KIND_HEALTH_ROOM = generator.next()
KIND_TREASURE_ROOM = generator.next()
KIND_PASSAGE_EXIT = generator.next()
KIND_STAIRWELL_EXIT = generator.next()
KIND_ELEVATOR_EXIT = generator.next()
KIND_WINDOW_EXIT = generator.next()
KIND_LOBBY_EXIT = generator.next()


# ------------------------------------------------------------------------------
# Locations
# ------------------------------------------------------------------------------

generate = FlagGenerator()
LOCATION_FRONT = generator.next()
LOCATION_HALLWAY = generator.next()
LOCATION_ROOM = generator.next()
LOCATION_ELEVATOR = generator.next()
LOCATION_STAIRWELL = generator.next()
LOCATION_RETURN = generator.next()
location_labels = {
    None: "",
    LOCATION_FRONT: "Front",
    LOCATION_HALLWAY: "Hallway",
    LOCATION_ROOM: "Room",
    LOCATION_ELEVATOR: "Elevator",
    LOCATION_STAIRWELL: "Stairwell",
    LOCATION_RETURN: "Return",
}


# ------------------------------------------------------------------------------
# Mission Objectives
# ------------------------------------------------------------------------------

OBJECTIVE_GO_TO_ROOM = _global_id_generator.next()
OBJECTIVE_SEE_MAN = _global_id_generator.next()
OBJECTIVE_STOP_MAN = _global_id_generator.next()
OBJECTIVE_GET_MAN = _global_id_generator.next()
OBJECTIVE_GET_INFO = _global_id_generator.next()
OBJECTIVE_GET_ITEM = _global_id_generator.next()
OBJECTIVE_USE_ITEM = _global_id_generator.next()

OBJECTIVE_PLANNED = _global_id_generator.next()
OBJECTIVE_IN_PROGRESS = _global_id_generator.next()
OBJECTIVE_ABORTED = _global_id_generator.next()
OBJECTIVE_ACCOMPLISHED = _global_id_generator.next()

# ------------------------------------------------------------------------------
# Entities
# ------------------------------------------------------------------------------

hero_max_speed = 150
hero_climb_speed = 50
hero_mass = 1.0
hero_dist_to_activate_sq = 30 * 30
hero_bullet_offset = 30

max_baddies_at_once = 3

stairwell_duration = 3.0  # [s] how long the stairwells take
elevator_duration = 10.0  # [s] how long the elevator take

class BlasterParams(object):
    firing_frequency = 6.0  # 1/sec
    bullet_kind = KIND_BLASTER_BULLET
    bullet_speed = 600.0  # units/s
    bullet_range = 800.0  # units
    bullet_radius = 10.0
    bullet_damage_radius = 10.0
    kind = KIND_WEAPON_BLASTER
    gravity = Vec3(0.0, 0.0, 0.0)
    bullet_damage = 1

class RocketParams(object):
    firing_frequency = 1.0  # 1/sec
    bullet_kind = KIND_ROCKET
    bullet_speed = 170.0  # units/s
    bullet_range = 1000.0  # units
    bullet_radius = 10.0
    bullet_damage_radius = 100.0
    kind = KIND_WEAPON_ROCKET_LAUNCHER
    gravity = Vec3(0.0, 0.0, 0.0)
    bullet_damage = 10


class GrenadeParams(object):
    firing_frequency = 3.0  # 1/sec
    bullet_kind = KIND_GRENADE
    bullet_speed = 160.0  # units/s
    bullet_range = 300.0  # units
    bullet_radius = 10.0
    bullet_damage_radius = 80.0
    kind = KIND_WEAPON_GRENADE_LAUNCHER
    gravity = Vec3(0.0, 0.0, 500.0)
    bullet_damage = 10

class RopeLauncherParams(object):
    bullet_kind = KIND_ROPE_BULLET
    kind = KIND_WEAPON_ROPE_LAUNCHER
    firing_frequency = 6.0  # 1/sec
    bullet_speed = 600.0  # units/s
    bullet_range = 300.0  # units
    bullet_radius = 10.0
    bullet_damage_radius = 10.0
    gravity = Vec3(0.0, 0.0, 0.0)


# ------------------------------------------------------------------------------
# World
# ------------------------------------------------------------------------------
cell_size = 100
floor_height_px = 270
room_width_px = 256

# ------------------------------------------------------------------------------
# Map
# ------------------------------------------------------------------------------
map_dir = 'data/maps'
map_files = (
    'paths_1.json',
)

# ------------------------------------------------------------------------------
# Stepper
# ------------------------------------------------------------------------------
STEP_DT = 1.0 / 50.0  # [1.0 / fps] = [s]
MAX_DT = 20 * STEP_DT  # [s]
DRAW_FPS = 30  # [fps]

# ------------------------------------------------------------------------------
# UI
# ------------------------------------------------------------------------------
hud_title = 'Improbable Mission Force'
title_hud_rect = 0, 10, 1, 1  # centered; x, w, h calculated after font render
mission_hud_rect = 10, 10, 1, 1  # topleft; w, h calculated after font render
objective_hud_rect = 0, 10, 1, 1  # topright; x, w, h calculated after font render
# midbottom; y calculated after font render
# changing the size: just change it here, IMFHUD will position it
chatter_box_rect = screen_width // 4, 0, screen_width // 2, screen_height // 5
tactical_box_rect = 10, 150, 190, 115

hud_fill_color = (25, 25, 50)
hud_border_color = (5, 5, 25)
hud_chatter_msgs = []

# TODO remove OLD
# hud_height = 66
# main_screen_rect = (0, 0, screen_width, screen_height - hud_height)
# hud_screen_rect = (0, main_screen_rect[3], screen_width, screen_height - main_screen_rect[3])
# build_button_size = (hud_height * 4 // 5, hud_height)
# scroll_button_size = (hud_height * 2 // 5, hud_height)
#
# health_bar_size = (50, 10)
# health_bar_back_color = (0, 150, 0)
# health_bar_color = (0, 200, 0)
#
back_ground_color = (126, 192, 238)
#
# level_indicator_color = (255, 255, 0)
# use_level_indicator_back_ground = True
# level_indicator_back_color = (0, 0, 150)
# level_indicator_radius = 3
# level_indicator_step_size = 8
# level_indicator_x_offset = -health_bar_size[0] // 2 + level_indicator_radius + 2
# level_indicator_y_offset = 10 - health_bar_size[1]
# max_levels = 6
#
# build_bar_size = (50, 10)
# build_bar_back_color = (0, 38, 255)
# build_bar_color = (0, 255, 255)
#
# normal_message_wait = 4.0
# short_message_wait = 0.8
#
# game_won_message = "...GAME WON..."
# game_lost_message = "...GAME LOST..."
# game_over_messages = game_won_message, game_lost_message

# main
LAYER_BACK_WALL = -10
LAYER_EXIT = -9
LAYER_EXIT_SHIELDS = -8
LAYER_FLOOR = -1
LAYER_HERO = 0
LAYER_HERO_WEAPON = 0.5
LAYER_HERO_ARM = 1  # sub layer actually
LAYER_MOUSE = 100000

# playfield_rect = pygame.Rect(main_screen_rect)

# chipworks_rect = playfield_rect.copy()
# contested_lands_rect = playfield_rect.copy()
# ant_country_rect = playfield_rect.copy()
# chipworks_rect.h *= 0.3
# contested_lands_rect.h *= 0.5
# contested_lands_rect.y = chipworks_rect.bottom
# ant_country_rect.h -= chipworks_rect.h + contested_lands_rect.h
# ant_country_rect.y = contested_lands_rect.bottom

# ------------------------------------------------------------------------------
# Graphics
# ------------------------------------------------------------------------------

gfx_path = "data/gfx"

# for use with rotozoom, to scale tower and mouse sprite size
widget_scale = 0.75

# queue holds the texts
system_message_y = screen_height * 0.2

# cached surfaces (required by spritefx)
# Note: pygametext does its own caching. No need to cache surfaces from pygametext.getsurf() unless you transform them.
# Note: these settings are not tunable on the fly. If you want to change the image cache settings during runtime, you'll
# have to call the image_cache instance's methods.
# Note: If you rely on aging or memory cap, remember to call image_cache.update(), or purge_memory() or purge_old().
# TODO: if we need image cache...
# image_cache_expire = 1 * 60  # expire unused textures after N seconds; recommend 1 min (1 * 60)
# image_cache_memory = 5 * 2 ** 20  # force expiration if memory is exceeded; recommend 5 MB (5 * 2**20)
# image_cache = imagecache.ImageCache(image_cache_expire, image_cache_memory)
# image_cache.enable_age_cap(True)

pygametext.FONT_NAME_TEMPLATE = 'data/font/%s'
pygametext.MEMORY_LIMIT_MB = 32  # it takes about 5 min to hit the default 64 MB, so 32 is not too aggressive

# font_themes is used with the functions in module pygametext
# Example call to render some text using a theme:
#   image = pygametext.getsurf('some text', **settings.font_themes['intro'])
# font_themes can be accessed by nested dict keys:
#   font_theme['intro']['fontname']
font_themes = dict(
    # Game title
    title=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=50,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    # Mission and Objective
    mission=dict(
        fontname='VeraBd.ttf',
        fontsize=18,
        color='gold',
        gcolor='yellow',
        ocolor='grey20',
        owidth=0.5,
        scolor=None,
        shadow=(1, 1),
    ),
    chatter_box=dict(
        fontname='Boogaloo.ttf',
        fontsize=20,
        color='white',
        # gcolor='orange',
        ocolor='black',
        owidth=0.2,
        scolor=None,
        shadow=None,
    ),
    tactical=dict(
        fontname='Vera.ttf',
        fontsize=13,
        color='yellowgreen',
        # gcolor='orange',
        # ocolor='black',
        # owidth=0.1,
        scolor='green4',
        shadow=(2, 2),
    ),
    # TODO: more themes TBD
    intro=dict(
        fontname='Boogaloo.ttf',
        fontsize=52,
        color='red2',
        gcolor='deepskyblue',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=None,
    ),
    gamehud=dict(
        fontname='VeraBd.ttf',
        fontsize=25,
        color='yellow',
        gcolor='orange2',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
    floatingtext=dict(
        fontname='Vera.ttf',
        fontsize=10,
        color='white',
        # gcolor='orange',
        ocolor='black',
        owidth=0.1,
        scolor=None,
        shadow=None,
        background=(0, 0, 0, 128)
    ),
)

# ------------------------------------------------------------------------------
# DEBUGS
# ------------------------------------------------------------------------------

# IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT  IMPORTANT
#
# Don't override debug settings in this file. Instead, do the following:
# This allows customizations for devs, and insures play-testers pulling from the
# repo will use the intended default settings.
#
# HOW TO:
# 1. Create gamelib/_custom.py.
# 2. Override your developer settings in _custom.py.
# 3. DO NOT add _custom.py to the repo. :)
#
# SAMPLE _custom.py:
# debug_quit = True
#
debug_quit = False

# Enable/disable a local developer context.
# True: invoke the shim in gamelib.main.main(); you will need to create a module
# gamelib._custom_context. See gamelib._custom_context_sample.py and gamelib.game_context.py.
#
# NOTE: >>>>> helper: cd gamelib; cp _custom_context_sample.py _custom_context.py <<<<<
# NOTE: >>>>> take care NOT to push your gamelib._custom_context.py to the repo. <<<<<
debug_context_shim = False
debug_mission_shim = False

debug_bypass_splash = False
debug_bypass_intro = False

debug_player_invincible = False

# noinspection PyBroadException
try:
    # noinspection PyUnresolvedReferences,PyProtectedMember
    from gamelib._custom import *
except ImportError:
    pass

logger.debug("imported")


