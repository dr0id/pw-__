# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'game_context.py' is part of pw---__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The splashscreen context

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import os
from itertools import chain

import pygame

from gamelib import settings
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.timing import Scheduler

chain_from_iterable = chain.from_iterable

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class SplashContext(TimeSteppedContext):

    def __init__(self):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self._activables = []
        self.start_info = []
        self.game_clock = self._game_time
        self.scheduler = Scheduler()
        self.screen = None  # fixme: this is actually something of the client
        self.count_out = 60
        self.quitting = False
        self.tick = 0

    def enter(self):
        TimeSteppedContext.enter(self)
        self.screen = pygame.display.get_surface()
        self.splash_gfx = pygame.image.load(os.path.join('data', 'splashscreen.png')).convert()
        self.tick = 0

    def update_step(self, dt, sim_time, *args):
        self.scheduler.update(dt, sim_time, *args)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quitting = True
            if event.type == pygame.KEYDOWN or event.type == pygame.KEYUP or event.type == pygame.MOUSEBUTTONDOWN or event.type == pygame.MOUSEBUTTONUP:
                self.quitting = True
        if self.quitting:
            self.count_out -= 1
            if self.count_out == 0:
                pygame.mixer.music.stop()
                pygame.mixer.music.set_volume(settings.master_volume)
                self.pop()
                return
            pygame.mixer.music.set_volume(settings.master_volume * (self.count_out / 60.0))
        self.tick += 1
        if self.tick == 1:
        	# Start up main game music loop
        	pygame.mixer.music.load(os.path.join('data', 'music', 'victory.ogg'))
        	pygame.mixer.music.set_volume(settings.master_volume)
        	pygame.mixer.music.play(-1)

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        self.screen.blit(self.splash_gfx, (0, 0))
        pygame.display.flip()

    def _on_quit(self, *args):
        self.quitting

    # def _on_action(self, action, extra):
    #     if action == ACTION_QUIT:
    #         event_dispatcher.fire(EVT_QUIT)
    #     else:
    #         self.quitting = True


logger.debug("imported")
