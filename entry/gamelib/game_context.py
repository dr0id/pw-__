# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'game_context.py' is part of pw---__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The main game context

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import random
import sys
from itertools import chain

import pygame

from gamelib import settings
from gamelib.collision import check_collisions, register_collision_func
from gamelib.entities import hero, entities
from gamelib import mission_playbook
from gamelib.entities.building import _Room, QuestRoom, AmbushRoom, HealthRoom, TreasureRoom, \
    ElevatorExit, StairwellExit, WindowExit
from gamelib.entities.entities import WalkableArea, Rope, UseObject, Explosion
from gamelib.entities.hero import Hero
# from gamelib.hallway_context import HallwayContext
# from gamelib.passage_context import ElevatorContext, StairwellContext
# from gamelib.room_context import QuestRoomContext, AmbushRoomContext, HealthRoomContext, TreasureRoomContext
from gamelib.settings import EVT_QUIT, EVT_ACTION, ACTION_QUIT, EVT_HERO_CREATED, EVT_BULLET_REMOVED, \
    EVT_BULLET_CREATED, KIND_HERO, KIND_WALKABLE_AREA, EVT_OBJECT_USED, EVT_EXIT_CREATED, \
    LOCATION_FRONT, LOCATION_HALLWAY, KIND_BLASTER_BULLET, \
    KIND_GRENADE, KIND_ROCKET, EVT_BULLET_COLLIDE_WALL, KIND_ROPE_BULLET, EVT_ROPE_CREATED, \
    EVT_EXPLOSION_CREATED, KIND_BADDY, EVT_DIED, KIND_EXPLOSION, \
    KIND_QUEST_ROOM, KIND_AMBUSH_ROOM, KIND_HEALTH_ROOM, KIND_TREASURE_ROOM, KIND_STAIRWELL_EXIT, \
    KIND_ELEVATOR_EXIT, KIND_WINDOW_EXIT, EVT_WEAPON_SLO1_SWITCHED, EVT_WEAPON_SLO2_SWITCHED
# from pyknic.events import EventDispatcher
# from pyknic.mathematics.geometry import SphereBinSpace2
# from gamelib.client.client import Client
from pyknic.mathematics import Point3, Point2
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.timing import Scheduler, Timer
from pyknic.tweening import Tweener

chain_from_iterable = chain.from_iterable

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class StartInfo(object):

    def __init__(self):
        self.mission_num = 0  # runtime: 1..N
        self.building = None  # gamelib.building.Building
        self.floor_num = 0  # 1..building.height
        self.room_num = 0  # 1..building.width
        self.location = LOCATION_HALLWAY  # settings.LOCATION_*
        self.mission_playbook = None  # set by MissionPlaybook.__init__()

        # these will communicate to the parent context, MissionManager
        self.mission_complete = True  # True: triggers loading of the first mission
        self.hero_health = settings.hero_starting_health
        self.game_quit = False
        self.hero_revenge = False
        self.change_context = None

    def __str__(self):
        return str(
            'StartInfo() <mission_num:{}, building:{}, floor_num:{}, room_num:{}, location:{}, mission_playbook:{}>'.format(
                self.mission_num, self.building, self.floor_num, self.room_num, self.location, self.mission_playbook))


class GameContext(TimeSteppedContext):

    def __init__(self, client, space, start_info, event_dispatcher):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self._entities_to_use = set()
        self.start_info = start_info
        self.space = space
        self.client = client
        self.event_dispatcher = event_dispatcher
        self.game_clock = self._game_time
        self.update_entities = set()
        self.scheduler = Scheduler()
        self.tweener = Tweener()
        self.level = None
        self.wave = None
        self._towers = set()
        self._bullets = set()
        self._explosions = set()
        self.debug_hud_timer = None
        self.game_over = False
        self.player = None
        self.walkable_areas = []
        self.ai_brains = []
        self.ai_params = [entities.AiBulletMan, entities.AiGrenadier, entities.AiRocketeer]

        self._baddies_timer = Timer(self.scheduler, repeat=True)
        self._baddies_timer.event_elapsed += self._spawn_baddies
        self._baddies_timer.interval = 15
        self._baddies_timer.start()

        self._in_combat = False
        self._allow_n_baddies = 1

        # self.player_walk_distance_sq = 0
        # self._next_baddy_encounter_dist_sq = random.random() * settings.encounter_random_distance + settings.encounter_min_distance
        # self._next_baddy_encounter_dist_sq *= self._next_baddy_encounter_dist_sq

        self.screen = None  # fixme: this is actually something of the client

    def enter(self):
        TimeSteppedContext.enter(self)
        self.screen = pygame.display.get_surface()
        event_dispatcher = self.event_dispatcher
        event_dispatcher.register_event_type(EVT_QUIT)
        event_dispatcher.register_event_type(EVT_ACTION)

        event_dispatcher.register_event_type(EVT_OBJECT_USED)
        event_dispatcher.register_event_type(EVT_WEAPON_SLO1_SWITCHED)
        event_dispatcher.register_event_type(EVT_WEAPON_SLO2_SWITCHED)

        event_dispatcher.add_listener(EVT_QUIT, self._on_quit)
        event_dispatcher.add_listener(EVT_ACTION, self._on_action)
        event_dispatcher.add_listener(EVT_BULLET_CREATED, self._on_bullet_created)
        event_dispatcher.add_listener(EVT_BULLET_REMOVED, self._on_bullet_removed)
        event_dispatcher.add_listener(EVT_EXPLOSION_CREATED, self._on_explode_created)
        event_dispatcher.add_listener(EVT_DIED, self._on_died)
        event_dispatcher.add_listener(EVT_OBJECT_USED, self._on_object_used)

        # collision functions
        register_collision_func((KIND_HERO, KIND_WALKABLE_AREA), self._collide_hero_vs_floor)
        register_collision_func((settings.KIND_BADDY, KIND_WALKABLE_AREA), self._collide_hero_vs_floor)
        register_collision_func(
            (KIND_BLASTER_BULLET | KIND_GRENADE | KIND_ROCKET | KIND_ROPE_BULLET, KIND_WALKABLE_AREA),
            self._collide_bullet_vs_floor)
        register_collision_func(
            (KIND_BLASTER_BULLET | KIND_GRENADE | KIND_ROCKET | KIND_ROPE_BULLET, KIND_HERO | KIND_BADDY),
            self._collide_bullet_vs_Body)
        register_collision_func((KIND_HERO | KIND_BADDY, KIND_EXPLOSION), self._collide_player_vs_explosion)

        # self.client.init()

        # player
        hero_x, hero_y = self.map_to_world(self.start_info.room_num, self.start_info.floor_num)
        # hero_y += settings.floor_height_px * 0.3 * 0.5
        self.player = Hero(Point3(hero_x, hero_y), pygame.Rect(0, 0, 50, 150), self, self.scheduler, hero.Standing,
                           self.event_dispatcher)
        self.player.health = self.start_info.hero_health
        self.player.map_floor_num = self.start_info.floor_num
        self.player.map_room_num = self.start_info.room_num
        self.client.player = self.player
        event_dispatcher.fire(EVT_HERO_CREATED, self.player)
        self.update_entities.add(self.player)
        logger.debug(">>> hero at {0}", self.player.position)

        # initial data
        num_floors = self.start_info.building.height
        num_rooms = self.start_info.building.width
        for floor_num in range(1, num_floors + 1):
            x1, y1 = self.map_to_world(1, floor_num)
            x2, y2 = self.map_to_world(num_rooms + 1, floor_num + 1)
            area = WalkableArea(x1, y1, x2 - x1, (y1 - y2) * 0.3, y2)
            logger.debug('>>>> area {}: {}', floor_num, area)
            self.walkable_areas.append(area)

        # self._create_npc(entities.AiGrenadier, self.start_info.room_num + 2, self.start_info.floor_num)
        # self._create_npc(entities.AiRocketeer, self.start_info.room_num + 3, self.start_info.floor_num)
        # self._create_npc(entities.AiBulletMan, self.start_info.room_num - 2, self.start_info.floor_num)

        self.client.add_walkable_areas(self.walkable_areas)

        # fixme: this should be event driven, as part of the chopper drop
        settings.start_info.mission_playbook.start_mission()

        self.create_exits()

    def exit(self):
        self.client.exit()

    def create_exits(self):
        # create exits
        si = self.start_info
        fn = si.floor_num
        rooms = si.building.get_rooms_as_list(fn)
        kind_map = {QuestRoom: KIND_QUEST_ROOM, AmbushRoom: KIND_AMBUSH_ROOM, HealthRoom: KIND_HEALTH_ROOM,
                    TreasureRoom: KIND_TREASURE_ROOM, ElevatorExit: KIND_ELEVATOR_EXIT,
                    StairwellExit: KIND_STAIRWELL_EXIT, WindowExit: KIND_WINDOW_EXIT}
        for r in rooms:
            logger.error('Processing exit: {}', r)
            rn = r.room_num
            x, y = self.map_to_world(rn, fn)
            rect = pygame.Rect(0, 0, settings.room_width_px, settings.floor_height_px)
            world_pos = x, y
            rect.bottomleft = world_pos
            if isinstance(r, (ElevatorExit, StairwellExit)):
                self.create_usable_object(r, world_pos, self.event_dispatcher)
                self.event_dispatcher.fire(EVT_EXIT_CREATED, r, rect)
                logger.debug('Passage created: {} {}', r, rect)
            elif isinstance(r, (QuestRoom, AmbushRoom, HealthRoom, TreasureRoom)):
                # todo: Gumm is working on this part
                if r.name == "Q1":
                    # Q1 is the drop point. It's not a usable object.
                    logger.error('Skipping Q1')
                    continue
                if si.location == LOCATION_HALLWAY:
                    self.create_usable_object(r, world_pos, self.event_dispatcher)
                    self.event_dispatcher.fire(EVT_EXIT_CREATED, r, rect)
                    logger.debug('Door created: {} {}', r, rect)
            elif isinstance(r, WindowExit):
                self.create_usable_object(r, world_pos, self.event_dispatcher)
                self.event_dispatcher.fire(EVT_EXIT_CREATED, r, rect)
                logger.debug('Window created: {} {}', r, rect)

    def _create_npc(self, ai_params, room_num, floor_num):
        logger.debug("create npc in room {0}/ floor {1}", room_num, floor_num)
        npc_x, npc_y = self.map_to_world(room_num, floor_num)
        npc_y += settings.floor_height_px * 0.3 * 0.5
        npc = Hero(Point3(npc_x, npc_y), pygame.Rect(0, 0, 50, 130), self, self.scheduler, hero.Standing,
                   self.event_dispatcher)
        npc.kind = settings.KIND_BADDY
        npc.health = 10
        ai = entities.Ai(npc, self.player, self.scheduler, self, ai_params)
        self.ai_brains.append(ai)
        self.update_entities.add(npc)
        self.event_dispatcher.fire(EVT_HERO_CREATED, npc)

    def resume(self):
        self.client.resume()

    def world_to_map(self, world_x, world_y):
        """scale world coords down to map floor and room"""
        # derivation: starting with the map_to_world formula and solving for the y_map
        # y_world = (settings.start_info.building.height - y_map) * settings.floor_height_px
        # y_world // settings.floor_height_px = settings.start_info.building.height - y_map
        # y_map = settings.start_info.building.height - y_world // settings.floor_height_px
        floor_num = self.start_info.building.height - world_y // settings.floor_height_px
        room_num = world_x // settings.room_width_px + 1
        return floor_num, room_num

    def map_to_world(self, room_num, floor_num):
        """scale map floor and room up to world coords"""
        return settings.room_width_px * (room_num - 1), (
                self.start_info.building.height - floor_num) * settings.floor_height_px

    def update_step(self, dt, sim_time, *args):
        if settings.debug_player_invincible:
            self.player.health = 1000000

        self.scheduler.update(dt, sim_time, *args)
        for e in self.update_entities:
            e.update(dt, sim_time)

        self.space.adds(self.update_entities)  # update positions

        for b in self._bullets:
            b.old_position = b.position.clone()
            b.velocity += b.params.gravity / 1.0 * dt  # f = m * a  -> a = f / m
            b.position += b.velocity * dt

        check_collisions(self.update_entities.union(self._bullets, set(self.walkable_areas), self._entities_to_use))

        # remove the explosions after one update (health should then be 0), this way we can have a normal collision
        # check against explosions
        self.update_entities.difference_update(
            [_u for _u in self.update_entities if _u.kind & settings.KIND_EXPLOSION and _u.health <= 0])

        self.client.update_step(dt, sim_time, *args)

        map_pos = self.world_to_map(self.player.position.x, self.player.position.y)
        self.player.map_floor_num, self.player.map_room_num = map_pos
        self.start_info.floor_num, self.start_info.room_num = map_pos

        # this is also done in Client.update_step()
        # self.start_info.mission_playbook.update(dt)
        self.tweener.update(dt)
        self.check_game_end_conditions()

    def _spawn_baddies(self, *args):
        self._baddies_timer.interval = random.randint(4, 20)
        baddy_count = len([_b for _b in self.update_entities if _b.kind & settings.KIND_BADDY])

        if self._in_combat:
            self._allow_n_baddies -= 1
            if self._allow_n_baddies <= 0:
                if baddy_count == 0:
                    # victory
                    self._in_combat = False
                    self.event_dispatcher.fire(settings.EVT_MUSIC_ACTION, settings.EVT_MUSIC_ACTION)
                    return
                else:
                    # still fighting
                    self._baddies_timer.interval = 2  # check every 2s if the combat is still on
                    return
        else:
            # define how many baddies will join next
            self._allow_n_baddies = random.randint(1, settings.max_baddies_at_once)
            if random.choice([True, False]):
                self.event_dispatcher.fire(settings.EVT_MUSIC_TENSIONA, settings.EVT_MUSIC_TENSIONA)
            else:
                self.event_dispatcher.fire(settings.EVT_MUSIC_TENSIONB, settings.EVT_MUSIC_TENSIONB)

        self._in_combat = True
        self._baddies_timer.interval = random.randint(1, 10)

        # create a baddy
        ai_params = random.choice(self.ai_params)
        room_num = self.player.map_room_num
        while room_num == self.player.map_room_num:
            room_num = self.player.map_room_num + random.choice([2, 3, 4]) * random.choice([-1, 1])
            # clamp to valid room
            room_num = room_num if room_num > 0 else 1
            room_num = room_num if room_num < self.start_info.building.width else self.start_info.building.width
        # create npc
        self._create_npc(ai_params, room_num, self.player.map_floor_num)

    def check_game_end_conditions(self):
        if self.player.health <= 0:
            logger.error("HERO DEAD!")

    def _collide_bullet_vs_floor(self, bullets, floors):
        for bullet in bullets:
            for floor in floors:
                if floor.rect.collidepoint(bullet.position.x, bullet.position.y):
                    if bullet.position.z <= 0:  # todo: is 0 always the ground level?
                        break
            else:
                if bullet.kind & KIND_ROPE_BULLET:
                    idx = pygame.Rect(bullet.old_position.as_xy_tuple(int), (1, 1)).collidelist(self.walkable_areas)
                    if idx < 0:
                        logger.error("could not place rope, outside!")
                    else:
                        r = self.walkable_areas[idx].rect
                        bottom_dy = r.bottom - bullet.old_position.y
                        top_dy = bullet.old_position.y - r.top
                        left_dx = bullet.old_position.x - r.left
                        right_dx = r.right - bullet.old_position.x
                        assert bottom_dy >= 0, "{0}, {1}, {2}".format(bottom_dy, r.bottom, bullet.old_position.y)
                        assert top_dy >= 0, "{0}, {1}, {2}".format(top_dy, bullet.old_position.y, r.top)
                        assert left_dx >= 0, "{0}, {1}, {2}".format(left_dx, bullet.old_position.x, r.left)
                        assert right_dx >= 0, "{0}, {1}, {2}".format(right_dx, r.right, bullet.old_position.x)
                        pos = bullet.old_position.clone()
                        if bottom_dy < top_dy:
                            if left_dx < bottom_dy:
                                # left
                                pos.x = r.left + 1
                            elif right_dx < bottom_dy:
                                # right
                                pos.x = r.right - 1
                            else:
                                pos.y = r.bottom - 1
                        else:
                            if left_dx < top_dy:
                                # left
                                pos.x = r.left + 1
                            elif right_dx < top_dy:
                                # right
                                pos.x = r.right - 1
                            else:
                                pos.y = r.top + 1
                        assert r.collidepoint(pos.x, pos.y), "{0} - {1}".format(r, pos)

                        self._add_rope_at(pos)
                elif bullet.kind & settings.KIND_GRENADE or bullet.kind & settings.KIND_ROCKET:
                    self._add_explosion(bullet.position, bullet.params.bullet_damage_radius, bullet.owner,
                                        bullet.params.bullet_damage)
                self.event_dispatcher.fire(EVT_BULLET_COLLIDE_WALL, bullet)
                self._bullets.remove(bullet)

    def _add_explosion(self, position, damage_radius, owner, damage):
        explosion = Explosion(position, damage_radius, owner, damage)
        self.update_entities.add(explosion)
        self.event_dispatcher.fire(EVT_EXPLOSION_CREATED, explosion)

    def _collide_bullet_vs_Body(self, bullets, players):
        for b in bullets:
            for p in players:
                if b.owner.kind != p.kind:  # don't hurt your own kind
                    dx = b.position.x - p.position.x
                    dy = b.position.y - p.position.y
                    p_radius_sq = p.radius * p.radius
                    if dx * dx + dy * dy <= p_radius_sq:
                        if -b.position.z < p.height:  # inverted y axis 0 is ground
                            # hit
                            if b.kind & settings.KIND_BLASTER_BULLET:
                                p.hit(b)
                            elif b.kind & settings.KIND_ROCKET:
                                self._add_explosion(b.position, b.params.bullet_damage_radius, b.owner, b.params.bullet_damage)
                            elif b.kind & settings.KIND_GRENADE:
                                self._add_explosion(b.position, b.params.bullet_damage_radius, b.owner, b.params.bullet_damage)
                            elif b.kind & settings.KIND_ROPE_BULLET:
                                pass

    def _collide_player_vs_explosion(self, players, explosions):
        for player in players:
            for explosion in explosions:
                if explosion.owner != player:  # don't hurt yourself
                    pr = player.radius
                    er = explosion.radius
                    dx = player.position.x - explosion.position.x
                    dy = player.position.y - explosion.position.y
                    if dx * dx + dy * dy <= (pr + er) ** 2:
                        player.explosion_damage(explosion.damage)

    def _collide_hero_vs_floor(self, heroes, floors):
        for _hero in heroes:
            if _hero.state_machine.current_state == hero.Falling:
                continue
            _hero.hit_rect.x = _hero.position.x
            _hero.hit_rect.y = _hero.position.y
            idx = _hero.hit_rect.collidelist(floors)
            _hero.walk_area = floors[idx]
            if idx < 0:
                # walked outside, move back
                dp = _hero.position - _hero.old_position
                dp.z = 0.0  # allow jumping

                # test x direction
                _hero.hit_rect.x = _hero.old_position.x + dp.x
                _hero.hit_rect.y = _hero.old_position.y
                idx = _hero.hit_rect.collidelist(floors)
                if idx >= 0:
                    dp.x = 0.0

                # test y direction
                _hero.hit_rect.x = _hero.old_position.x
                _hero.hit_rect.y = _hero.old_position.y + dp.y
                idx = _hero.hit_rect.collidelist(floors)
                if idx >= 0:
                    dp.y = 0.0

                # update position to be back inside
                _hero.position -= dp

            # climbing down until you hit the floor
            if _hero.position.z > 0:  # todo: is 0 always the ground level?
                _hero.position.z = 0

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        # Gumm: in subsequence missions scheduler fires this before GameContext.enter is called
        if self.screen:
            self.client.draw(self.screen, do_flip, interpolation_factor)

    def get_usable_object_near(self, position):
        smallest_dist_sq = sys.maxsize
        nearest = None
        pos2 = Point2(position.x, position.y)
        logger.info('POS1> {}', pos2)
        a_pos = Point2(0, 0)
        for a in self._entities_to_use:
            a_pos.copy_values(Point2(*a.position))
            a_pos.x += settings.room_width_px / 2  # the usable object returns the left as position not mids
            d_sq = pos2.get_distance_sq(a_pos)
            logger.info('ENTITY> {}', a)
            logger.info('POS2> {}', a.position)
            logger.warning('DISTANCE> {} < {} & < {}', d_sq, smallest_dist_sq, settings.hero_dist_to_activate_sq)
            logger.error("UseObj in Floor, Room {0}", self.world_to_map(a_pos.x, a_pos.y))
            if d_sq < smallest_dist_sq and d_sq < settings.hero_dist_to_activate_sq:
                smallest_dist_sq = d_sq
                nearest = a
        logger.info('NEAREST> {}', nearest)
        return nearest

    def _on_quit(self, *args):
        logger.debug("GAME_CONTEXT> _on_quit")
        self.start_info.game_quit = True
        self.pop()

    def _on_action(self, action, extra):
        if action == ACTION_QUIT:
            self.event_dispatcher.fire(EVT_QUIT)
        else:
            self.player.handle_action(action, extra)

    def _on_bullet_created(self, bullet):
        self._bullets.add(bullet)

    def _on_bullet_removed(self, bullet):
        self._bullets.discard(bullet)

    def _on_explode_created(self, explosion):
        self._explosions.add(explosion)

    def _on_died(self, player):
        if player == self.player and not self.start_info.hero_revenge:
            def bug_out(*args):
                self.pop()
            # game over
            self.start_info.hero_revenge = True
            t = Timer(self.scheduler, 5)
            t.event_elapsed += bug_out
            t.start()
            self.client.hud.add_chatter('Comms: Face palm. Over and out.')
        self.update_entities.discard(player)
        brains = [_a for _a in self.ai_brains if _a.npc == player]
        if brains:
            assert len(brains) == 1, "should not have found more than one brain for a npc"
            brain = brains[0]
            brain.timer.stop()
            self.ai_brains.remove(brain)

    def _on_object_used(self, object_id):
        logger.debug("GAME_CONTEXT> _on_object_use: object_id={}", object_id)
        # TODO
        if isinstance(object_id, (QuestRoom, HealthRoom, AmbushRoom, TreasureRoom)):
            objective = self.start_info.mission_playbook.now_objective
            # do not enter a room if there is no mission objective
            if not objective:
                return
            # do not enter a room if the room is not the current mission objective
            quest_room_name = objective.room_name
            if quest_room_name != object_id.name:
                return
        self.start_info.change_context = object_id
        self.pop()

    def _add_rope_at(self, position):
        rope_position = position.clone()
        rope_position.z = 0
        rope_length = 200  # todo is this good?
        rope = Rope(rope_position, rope_length)
        self._entities_to_use.add(rope)
        self.event_dispatcher.fire(EVT_ROPE_CREATED, rope)

    def create_usable_object(self, use_object_id, position, event_dispatcher):
        logger.error('CREATE_USABLE> {} {}', use_object_id, position)
        usable_object = UseObject(use_object_id, position, event_dispatcher)
        self._entities_to_use.add(usable_object)
        event_dispatcher.fire(settings.EVT_USE_OBJECT_CREATED, usable_object)


logger.debug("imported")
