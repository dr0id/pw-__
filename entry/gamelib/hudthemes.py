# -*- coding: utf-8 -*-
"""hudthemes.py - A handful of attractive themes.

This module is not required. It is just an example of how one can develop themes.

Note: If pygametext complains that it can't find your font, set the font directory first. For example,
hudlight.set_font_template('fonts/%s').

Example usages

    # Using a theme out of the box.
    hud = hudlight.HUD(**hudthemes.HUD_THEMES['vera'])
    
    # Modifying a theme locally.
    theme = hudthemes.HUD_THEMES['default'].copy()  # << copy()
    theme.update(dict(
        fontsize=40,
    ))
    hud = hudlight.HUD(**theme)
    
    # Changing a theme globally.
    theme = hudthemes.HUD_THEMES['default']  # << no copy()
    theme.update(dict(
        fontsize=32,
        scolor=None,
        ocolor='black',
    ))
    hud1 = hudlight.HUD(**theme)
    hud2 = hudlight.HUD(**theme)

"""


__version__ = '3.0.0'
__author__ = 'Gummbum, (c) 2017'
__vernum__ = tuple(int(s) for s in __version__.split('.'))
__all__ = ['set_theme', 'HUD_THEMES']


def set_theme(hud, **theme):
    """change the theme for a HUD instance
    
    To change themes on an active HUD, make sure they have the same keys. This will insure that all the values will
    be set as desired. Otherwise, the result may have artifacts from the previous theme.
    
    Note: If pygametext complains that it can't find your font, set the font directory first. For example,
    hudlight.set_font_template('fonts/%s').
    
    :param hud: hudlight.HUD
    :param theme: **HUD_THEMES['name']
    :return: None
    """
    hud.set_fontname(theme['fontname']).set_fontsize(theme['fontsize'])
    hud.set_color(theme['color']).set_gcolor(theme['gcolor'])
    hud.set_ocolor(theme['ocolor']).set_scolor(theme['scolor']).set_shadow(theme['shadow'])


# All themes have the same keys so they can be easily swapped in run-time without leaving any artifacts.
HUD_THEMES = dict(
    # Boogaloo: nice fat font, red fading to blue with yellow outline
    boogaloo=dict(
        fontname='Boogaloo.ttf',
        fontsize=26,
        color='red2',
        gcolor='deepskyblue',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=(1, 1),
    ),
    # Roboto_Condensded: clear thin font, blue fading to yellow with black outline
    roboto=dict(
        fontname='Roboto_Condensed.ttf',
        fontsize=24,
        color='cornflowerblue',
        gcolor='lightyellow',
        ocolor='black',
        owidth=0.5,
        scolor=None,
        shadow=(1, 1),
    ),
    # Bubblegum_Sans: clear, stylish, informal font, red2 fading to orange with yellow outline
    bubblegum=dict(
        fontname='Bubblegum_Sans.ttf',
        fontsize=25,
        color='red2',
        gcolor='orange1',
        ocolor='yellow',
        owidth=0.5,
        scolor=None,
        shadow=(1, 1),
    ),
    # CherryCreamSoda: stylish, informal font, orange fading to purple with purple neon glow
    cherrycreamsoda=dict(
        fontname='CherryCreamSoda.ttf',
        fontsize=25,
        color='orange2',
        gcolor='mediumpurple4',
        ocolor='mediumpurple',
        owidth=0.5,
        scolor=None,
        shadow=(1, 1),
    ),
    # Vera: thin, formal font, lightblue fading to green4 with dark grey shadow
    vera=dict(
        fontname='Vera.ttf',
        fontsize=18,
        color='lightblue',
        gcolor='green4',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(-1, 1),
    ),
    # VeraBD: stylish, informal font, orange fading to purple with purple neon glow
    verabd=dict(
        fontname='VeraBd.ttf',
        fontsize=25,
        color='red2',
        gcolor='gold',
        ocolor=None,
        owidth=0.5,
        scolor='grey20',
        shadow=(1, 1),
    ),
)
HUD_THEMES['default'] = HUD_THEMES['boogaloo']
