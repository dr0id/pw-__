# -*- coding: utf-8 -*-

import logging

import pygame

from gamelib import settings, pygametext
from gamelib.client.sprites import GummExitSprite, AnimatedSprite
from gamelib.common_context import CommonContext
from pyknic import tweening
from pyknic.mathematics import Point3 as Point, Point3
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.timing import Timer, Scheduler
from pyknic.tweening import Tweener

from gamelib.settings import music_ended_pygame_event

logger = logging.getLogger(__name__)
logger.debug("importing...")


class DummyPlayer(object):
    def __init__(self, fn, rn, weapon='Blaster', utility='Grenade', room_name='Room'):
        self.floor_num = fn
        self.room_num = rn
        self.weapon = weapon
        self.utility = utility
        self.location = room_name
        self.health = 100


class ElevatorContext(TimeSteppedContext):

    def __init__(self, client, space, start_info, event_dispatcher):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.event_dispatcher = event_dispatcher
        self.start_info = start_info
        self.client = client
        self.screen = None
        self.scheduler = Scheduler()
        self.timer = Timer(self.scheduler)
        self.timer.event_elapsed += self._on_timer_elapsed
        self.tweener = Tweener()
        self.player = None

    def _on_timer_elapsed(self, *args):
        self.pop()

    def enter(self):
        self.screen = pygame.display.get_surface()
        self.timer.interval = settings.elevator_duration
        self.timer.start()
        spr = AnimatedSprite(self.client.images["elevator"].data,
                             Point3(settings.screen_width / 2, settings.screen_height / 2), self.client.scheduler)
        spr.anchor = 'center'
        self.client.renderer.add_sprite(spr)
        self.tweener.create_tween_by_end(spr.position, "x", spr.position.x, spr.position.x , settings.elevator_duration, tweening.ease_random_int_bounce, (-3, 3))
        self.tweener.create_tween_by_end(spr.position, "y", spr.position.y, spr.position.y, settings.elevator_duration, tweening.ease_random_int_bounce, (-3, 3))
        self.event_dispatcher.fire(settings.EVT_ENTER_ELEVATOR, settings.EVT_ENTER_ELEVATOR)
        self.player = DummyPlayer(self.start_info.floor_num, self.start_info.room_num, room_name='Quest Room')
        self.client.player = self.player

    def exit(self):
        self.start_info.change_context = settings.LOCATION_RETURN
        self.event_dispatcher.fire(settings.EVT_EXIT_ELEVATOR, settings.EVT_EXIT_ELEVATOR)

    def update_step(self, delta, sim_time, *args):
        self.scheduler.update(delta, sim_time)
        self.client.update_hud()
        self.tweener.update(delta)
        for event in pygame.event.get():
            if event.type == music_ended_pygame_event:
                logger.info("music ended event!")
                self.client.music_player.on_music_ended()

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        if self.screen:
            self.client.draw(self.screen, do_flip, interpolation_factor)


class StairwellContext(TimeSteppedContext):

    def __init__(self, client, space, start_info, event_dispatcher):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self.start_info = start_info
        self.client = client
        self.screen = None
        self.scheduler = Scheduler()
        self.timer = Timer(self.scheduler)
        self.timer.event_elapsed += self._on_timer_elapsed
        self.tweener = Tweener()
        self.player = None

    def _on_timer_elapsed(self, *args):
        self.pop()

    def enter(self):
        self.screen = pygame.display.get_surface()
        self.timer.interval = settings.stairwell_duration
        self.timer.start()
        spr = AnimatedSprite(self.client.images["stairwell"].data,
                             Point3(settings.screen_width / 2, settings.screen_height / 2), self.client.scheduler)
        spr.anchor = "center"
        self.client.renderer.add_sprite(spr)
        self.tweener.create_tween_by_end(spr, "zoom", 1.0, 1.15, settings.stairwell_duration)
        self.player = DummyPlayer(self.start_info.floor_num, self.start_info.room_num, room_name='Quest Room')
        self.client.player = self.player

    def exit(self):
        self.start_info.change_context = settings.LOCATION_RETURN

    def update_step(self, delta, sim_time, *args):
        self.scheduler.update(delta, sim_time)
        self.client.update_hud()
        self.tweener.update(delta)
        events = pygame.event.get()

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        if self.screen:
            self.client.draw(self.screen, do_flip, interpolation_factor)


logger.debug("imported")
