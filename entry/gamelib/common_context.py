# -*- coding: utf-8 -*-

from __future__ import print_function

import logging

from gamelib.game_context import GameContext


logger = logging.getLogger(__name__)
logger.debug("importing...")


class CommonContext(GameContext):

    def __init__(self, client, space, start_info, event_dispatcher):
        GameContext.__init__(self, client, space, start_info, event_dispatcher)

    def enter(self):
        GameContext.enter(self)
        # self.load_location()
        # self.start_info.mission_playbook.start_mission()

    # def create_exits(self):
    #     pass
    #
    # def load_location(self):
    #     pass
    #
    # def update_step(self, dt, sim_time, *args):
    #     self.scheduler.update(dt, sim_time, *args)
    #
    #     self.start_info.mission_playbook.update(dt)
    #
    #     self.update_scene(dt, sim_time, *args)
    #
    #     # for e in self.update_entities:
    #     #     e.update(dt, sim_time)
    #     #
    #     # self.space.adds(self.update_entities)  # update positions
    #     #
    #     # for b in self._bullets:
    #     #     b.old_position = b.position.clone()
    #     #     b.position += b.velocity * dt
    #     #
    #     # check_collisions(list(self.update_entities.union(self._bullets)) + self.walkable_areas)
    #
    #     self.client.update_step(dt, sim_time, *args)
    #
    #     self.tweener.update(dt)
    #
    # def update_scene(self, dt, sim_time, *args):
    #     pass


logger.debug("imported")
