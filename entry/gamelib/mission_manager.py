# -*- coding: utf-8 -*-

import logging

import pygame

from gamelib import settings
from gamelib.hallway_context import HallwayContext
from gamelib.settings import LOCATION_FRONT, LOCATION_HALLWAY, LOCATION_ELEVATOR, LOCATION_ROOM, LOCATION_STAIRWELL, \
    LOCATION_RETURN
from gamelib.entities.building import QuestRoom, AmbushRoom, HealthRoom, TreasureRoom, ElevatorExit, StairwellExit, \
    WindowExit, _Room
from gamelib.client.client import Client
from gamelib.game_context import StartInfo, GameContext
from gamelib.passage_context import ElevatorContext, StairwellContext
from gamelib.room_context import QuestRoomContext, AmbushRoomContext, HealthRoomContext, TreasureRoomContext
from gamelib.missions import mission_01, mission_02
from pyknic.context import Context
from pyknic.mathematics.geometry import SphereBinSpace2
from pyknic.events import EventDispatcher


logger = logging.getLogger(__name__)
logger.debug("importing...")

missions = [
    mission_01,
]
if settings.debug_mission_shim:
    from gamelib.missions import mission_gumm
    missions.insert(0, mission_gumm)


loc_names = {
    LOCATION_FRONT: 'LOCATION_FRONT',
    LOCATION_HALLWAY: 'LOCATION_HALLWAY',
    LOCATION_ELEVATOR: 'LOCATION_ELEVATOR',
    LOCATION_ROOM: 'LOCATION_ROOM',
    LOCATION_STAIRWELL: 'LOCATION_STAIRWELL',
    LOCATION_RETURN: 'LOCATION_RETURN',
}


class NothingToDoException:
    pass


class MissionManager(Context):
    def __init__(self):

        # -------------------------------------------------------------------------
        # NB: This is the master StartInfo. It is preserved in settings.start_info.
        # -------------------------------------------------------------------------
        self.start_info = settings.start_info = StartInfo()

    def update_mission(self):
        logger.info('MISSION_MANAGER> update_mission STARTING')
        # self.start_info = settings.start_info

        if self.start_info.hero_revenge:
            logger.info("MISSION_MANAGER> hero's revenge")

            self.start_info.hero_revenge = False

            # load the mission map
            event_dispatcher = EventDispatcher()
            mission = missions[self.start_info.mission_num - 1]
            mission.load(self.start_info, event_dispatcher)
            logger.info('MISSION_MANAGER> Loaded {}', self.start_info)
            logger.info('MISSION_MANAGER> Rooms: {}', self.start_info.building.rooms_by_name.items())

            # create the game context and push it
            del settings.hud_chatter_msgs[:]
            self.start_info.hero_health = settings.hero_starting_health
            self.start_info.location = LOCATION_HALLWAY
            self.start_info.mission_complete = False
            self.start_info.mission_playbook.event_dispatcher = event_dispatcher
            space = SphereBinSpace2(settings.cell_size)
            client = Client(settings.music_player, space, self.start_info, event_dispatcher)
            game = GameContext(client, space, self.start_info, event_dispatcher)
            self.push(game)

        elif self.start_info.mission_complete:
            logger.info('clear_chatter')

            if self.start_info.mission_num < len(missions):
                logger.info('MISSION_MANAGER> getting next mission')

                # NEXT MISSION

                # advance the mission number
                self.start_info.mission_num += 1
                logger.info('MISSION_MANAGER> loading mission_num={}', self.start_info.mission_num)

                # load the mission map
                event_dispatcher = EventDispatcher()
                mission = missions[self.start_info.mission_num - 1]
                mission.load(self.start_info, event_dispatcher)
                logger.info('MISSION_MANAGER> Loaded {}', self.start_info)
                logger.info('MISSION_MANAGER> Rooms: {}', self.start_info.building.rooms_by_name.items())

                # create the game context and push it
                del settings.hud_chatter_msgs[:]
                self.start_info.location = LOCATION_HALLWAY
                self.start_info.mission_complete = False
                self.start_info.mission_playbook.event_dispatcher = event_dispatcher
                space = SphereBinSpace2(settings.cell_size)
                client = Client(settings.music_player, space, self.start_info, event_dispatcher)
                game = GameContext(client, space, self.start_info, event_dispatcher)
                self.push(game)

            else:
                logger.info('MISSION_MANAGER> GAME WON | LOST (TODO)')

                # GAME WON OR LOST

                # todo: game WON or LOST
                logger.info('MISSION_MANAGER> no more missions :(')
                self.pop()

        else:
            logger.info('MISSION_MANAGER> change_context={}', self.start_info.change_context)

            # CHANGING CONTEXT IN SAME MISSION

            event_dispatcher = EventDispatcher()
            space = SphereBinSpace2(settings.cell_size)
            client = Client(settings.music_player, space, self.start_info, event_dispatcher)

            si = self.start_info

            if si.change_context == LOCATION_RETURN:
                logger.info('MISSION_MANAGER> EGRESS')

                # RETURNING THROUGH AN EXIT

                if si.location == LOCATION_ROOM:
                    logger.info('MISSION_MANAGER> location=LOCATION_ROOM, returning to Hallway')
                    si.location = LOCATION_HALLWAY
                    new_context = HallwayContext(client, space, si, event_dispatcher)
                elif si.location in (LOCATION_ELEVATOR, LOCATION_STAIRWELL):
                    logger.info('MISSION_MANAGER> location in (LOCATION_ELEVATOR, LOCATION_STAIRWELL)')
                    # if si.floor_num == si.building.height:
                    #     logger.info('MISSION_MANAGER> on top floor, returning to Front')
                    #     si.location = LOCATION_FRONT
                    #     new_context = GameContext(client, space, si, event_dispatcher)
                    # else:
                    logger.info('MISSION_MANAGER> returning to Hallway')
                    si.location = LOCATION_HALLWAY
                    new_context = HallwayContext(client, space, si, event_dispatcher)
                # elif si.location == LOCATION_HALLWAY:
                #     logger.info('MISSION_MANAGER> location=LOCATION_HALLWAY, returning to Front')
                #     si.location = LOCATION_FRONT
                #     new_context = GameContext(client, space, si, event_dispatcher)
                else:
                    # default = FRONT; should never happen, but...
                    logger.error('MISSION_MANAGER> unexpected return: location={} change_context={}, returning to Front',
                                 loc_names[si.location], si.change_context)
                    si.location = LOCATION_HALLWAY
                    new_context = GameContext(client, space, si, event_dispatcher)

            else:
                logger.info('MISSION_MANAGER> INGRESS')

                # GOING IN AN EXIT

                # if isinstance(si.change_context, WindowExit):
                #     logger.info('MISSION_MANAGER> entering a Window')
                #     si.location = LOCATION_FRONT
                #     # si.floor_num = si.change_context.distant_end.floor_num
                #     # si.room_num = si.change_context.distant_end.room_num
                #     new_context = HallwayContext(client, space, si, event_dispatcher)
                if isinstance(si.change_context, ElevatorExit):
                    logger.info('MISSION_MANAGER> entering an Elevator')
                    si.location = LOCATION_ELEVATOR
                    si.floor_num = si.change_context.distant_end.floor_num
                    si.room_num = si.change_context.distant_end.room_num
                    new_context = ElevatorContext(client, space, si, event_dispatcher)
                elif isinstance(si.change_context, StairwellExit):
                    logger.info('MISSION_MANAGER> entering a Stairwell')
                    si.location = LOCATION_STAIRWELL
                    si.floor_num = si.change_context.distant_end.floor_num
                    si.room_num = si.change_context.distant_end.room_num
                    new_context = StairwellContext(client, space, si, event_dispatcher)
                elif isinstance(si.change_context, _Room):
                    logger.info('MISSION_MANAGER> entering a Room')
                    si.location = LOCATION_ROOM
                    if isinstance(si.change_context, QuestRoom):
                        new_context = QuestRoomContext(client, space, si, event_dispatcher)
                    elif isinstance(si.change_context, AmbushRoom):
                        new_context = AmbushRoomContext(client, space, si, event_dispatcher)
                    elif isinstance(si.change_context, HealthRoom):
                        new_context = HealthRoomContext(client, space, si, event_dispatcher)
                    else:
                        new_context = TreasureRoomContext(client, space, si, event_dispatcher)
                else:
                    logger.info('MISSION_MANAGER> unexpected entry: location={} change_context={}',
                                 loc_names[si.location], si.change_context)
                    # default = FRONT; should never happen, but...
                    si.location = LOCATION_HALLWAY
                    new_context = GameContext(client, space, si, event_dispatcher)

            si.change_context = None
            si.mission_playbook.event_dispatcher = event_dispatcher
            fn, rn = si.floor_num, si.room_num
            room = si.building.get_room(fn, rn)
            logger.info('MISSION_MANAGER> hero map_pos={} room={}', (fn, rn), room)

            self.push(new_context)
            logger.info('MISSION_MANAGER> update_mission FINISHED')

    def enter(self):
        logger.info('MISSION_MANAGER> enter -> FIRST_MISSION')
        self.update_mission()

    def resume(self):
        if self.start_info.game_quit:
            logger.info('MISSION_MANAGER> resumed -> USER_QUIT')
            self.pop()
        else:
            logger.info('MISSION_MANAGER> resumed -> UPDATE_MISSION')
            self.update_mission()


logger.debug("imported")
