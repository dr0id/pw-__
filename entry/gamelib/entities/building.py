# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'client.py' is part of pw---__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The building module.

.. versionchanged:: 0.0.0.0
    initial version

This module contains classes and utilities for creating, managing, and query a building.

Building and Floor classes use sparse array.

Floor 1 will be the lowest floor, increasing towards the top.

Slot 1 will be the left slot on a floor, increasing to the right.

See example code in __main__.


Sample Building Data
====================

building_data = '''
  | v  _  _  _  _  _  _  _  _  _10_  _  _  _  _  _  _  _  _20_  _  _  _  _  _  _  _  _  _30_  _  _  _  _  _  _  _  _  _40
04| S1    E1          Q1                      E2       S2
03|          Q2
02|
01| S1    E1                   L1             E2    Q3 S2
  | ^  _  _  _  _  _  _  _  _  _10_  _  _  _  _  _  _  _  _20_  _  _  _  _  _  _  _  _  _30_  _  _  _  _  _  _  _  _  _40
'''

Explanation of building data
----------------------------
Ruler rows:
- Rows 0 & 5: rulers for your convenience. "v" and "^" mark the start of first data. "_" marks the start of each data
  triplet.
Data rows:
- Columns 0 & 1: ruler labels for your convenience. Floor number is derived from the line position, NOT the labels.
- Column 2: a pipe ("|") delimits the ruler labels from the data.
- Column 3: the first space, if present, is thrown out. The data looks nicer with a space, though.
- When the rest of the string is parsed into triplets it will yield substrings like "   ", "E1 ", etc. Whitespace is
  trimmed to yield "", "E1", etc.
- For room data (e.g. "Q1") the alpha part maps to the class in room_type_map["Q"]. The numeric part is stored as an
  integer in the _Room object as map_id, which will be useful for connecting the room with a specific mission element,
  e.g. "Q1" -> Quest 1, "T9" -> Treasure 9.
- Q1 is always the drop point. It must be on the rooftop.
- For elevator and stairs data, e.g. "E1", the alpha part maps to the class in room_type_map["E"]. The numeric part
  links the _Passage object to the other end which has the same map data "E1". These two objects can be differentiated
  by obj.floor_num.

"""
from __future__ import print_function

import logging
import re

from gamelib import settings
from gamelib.settings import KIND_BUILDING, KIND_FLOOR, \
    KIND_ROOM, KIND_QUEST_ROOM, KIND_AMBUSH_ROOM, KIND_HEALTH_ROOM, KIND_TREASURE_ROOM, \
    KIND_PASSAGE_EXIT, KIND_STAIRWELL_EXIT, KIND_ELEVATOR_EXIT, KIND_WINDOW_EXIT, KIND_LOBBY_EXIT

# from pyknic.mathematics import Vec3 as Vec, Point3 as Point
# from pyknic.tweening import Tweener

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class RoomNumOccupiedException(Exception):
    pass


class BadArgsException(Exception):
    pass


class _Room(object):
    kind = KIND_ROOM

    def __init__(self, floor_num, room_num, name, map_id=0):
        """

        :param floor_num: int
        :param room_num: int
        :param name: str ("Q1", "E1", ...)
        :param map_id: int
        """
        self.floor_num = floor_num
        self.room_num = room_num
        self.name = name
        self.map_id = map_id

    def add_to(self, ent):
        assert isinstance(ent, (Building, Floor))
        ent.add_room(self)

    def is_quest(self):
        return self.kind == KIND_QUEST_ROOM

    def is_ambush(self):
        return self.kind == KIND_AMBUSH_ROOM

    def is_health(self):
        return self.kind == KIND_HEALTH_ROOM

    def is_treasure(self):
        return self.kind == KIND_TREASURE_ROOM

    def is_stairwell_exit(self):
        return self.kind == KIND_STAIRWELL_EXIT

    def __str__(self):
        return '{}() <{}>'.format(self.__class__.__name__, self.name)


class QuestRoom(_Room):
    """quest objective in here"""
    kind = KIND_QUEST_ROOM


class AmbushRoom(_Room):
    """ambush in here"""
    kind = KIND_AMBUSH_ROOM


class HealthRoom(_Room):
    """health in here"""
    kind = KIND_HEALTH_ROOM


class TreasureRoom(_Room):
    """treasure in here"""
    kind = KIND_TREASURE_ROOM


class _PassageExit(_Room):
    """to / from a passage, links to other end"""
    kind = KIND_PASSAGE_EXIT
    distant_end = None

    def link_with(self, other):
        """sets up linkage both ways between this end and the other"""
        assert isinstance(other, (StairwellExit, ElevatorExit))
        self.distant_end = other
        other.distant_end = self


class StairwellExit(_PassageExit):
    kind = KIND_STAIRWELL_EXIT


class ElevatorExit(_PassageExit):
    kind = KIND_ELEVATOR_EXIT


class WindowExit(_Room):
    kind = KIND_WINDOW_EXIT


class LobbyExit(_Room):
    kind = KIND_LOBBY_EXIT


room_type_map = {
    KIND_QUEST_ROOM: QuestRoom,
    KIND_AMBUSH_ROOM: AmbushRoom,
    KIND_HEALTH_ROOM: HealthRoom,
    KIND_TREASURE_ROOM: TreasureRoom,
    KIND_ELEVATOR_EXIT: ElevatorExit,
    KIND_STAIRWELL_EXIT: StairwellExit,
    KIND_WINDOW_EXIT: WindowExit,
    QuestRoom: KIND_QUEST_ROOM,
    AmbushRoom: KIND_AMBUSH_ROOM,
    HealthRoom: KIND_HEALTH_ROOM,
    TreasureRoom: KIND_TREASURE_ROOM,
    ElevatorExit: KIND_ELEVATOR_EXIT,
    StairwellExit: KIND_STAIRWELL_EXIT,
    WindowExit: KIND_WINDOW_EXIT,
    'E': ElevatorExit,
    'S': StairwellExit,
    'Q': QuestRoom,
    'A': AmbushRoom,
    'H': HealthRoom,
    'T': TreasureRoom,
    'L': QuestRoom,
    'W': WindowExit,
}


def make_stairwell(floor_num1, room_num1, floor_num2, room_num2, name, map_id):
    """create and link two stairwell exits

    The returned objects still need to be added to the Building or Floor.

    :param floor_num1: int
    :param room_num1: int
    :param floor_num2: int
    :param room_num2: int
    :return: tuple(obj1, obj2)
    """
    return make_passage(KIND_STAIRWELL_EXIT, floor_num1, room_num1, floor_num2, room_num2, name, map_id)


def make_elevator(floor_num1, room_num1, floor_num2, room_num2, name, map_id):
    """create and link two elevator exits

    The returned objects still need to be added to the Building or Floor.

    :param floor_num1: int
    :param room_num1: int
    :param floor_num2: int
    :param room_num2: int
    :return: tuple(obj1, obj2)
    """
    return make_passage(KIND_ELEVATOR_EXIT, floor_num1, room_num1, floor_num2, room_num2, name, map_id)


def make_passage(kind, floor_num1, room_num1, floor_num2, room_num2, name, map_id):
    """create exits of a kind, and link them

    The returned objects still need to be added to the Building or Floor.

    :param kind: KIND_STAIRWELL_EXIT or KIND_ELEVATOR_EXIT
    :param floor_num1: int
    :param room_num1: int
    :param floor_num2: int
    :param room_num2: int
    :return: tuple(obj1, obj2)
    """
    obj1 = room_type_map[kind](floor_num1, room_num1, name, map_id)
    obj2 = room_type_map[kind](floor_num2, room_num2, name, map_id)
    return link_passage(obj1, obj2)


def link_passage(obj1, obj2):
    """link two exits

    The returned objects still need to be added to the Building or Floor.

    :param obj1:
    :param obj2:
    :return: tuple(obj1, obj2)
    """
    assert hasattr(obj1, 'link_with') and hasattr(obj2, 'link_with')
    obj1.link_with(obj2)
    return obj1, obj2


def _room_sort_key(o):
    return o.room_num


class Floor(object):
    kind = KIND_FLOOR

    def __init__(self, floor_num, building):
        self.floor_num = floor_num
        self.building = building
        self.rooms = {}

    def add_rooms(self, *args):
        if args:
            seq = args if isinstance(args[0], _Room) else args[0]
            for r in seq:
                self.add_room(r)

    def add_room(self, room):
        if room.room_num in self.rooms:
            raise RoomNumOccupiedException
        room.building = self.building
        self.rooms[room.room_num] = room

    def get_room(self, room_num):
        return self.rooms.get(room_num, None)

    def get_rooms_as_dict(self):
        return dict(self.rooms)

    def get_rooms_as_list(self):
        if self.rooms:
            return list(sorted(self.rooms.values(), key=_room_sort_key))
        else:
            return []

    def is_rooftop(self):
        return self.floor_num == self.building.height

    def is_lobby(self):
        return self.floor_num == 1


class Building(object):
    kind = KIND_BUILDING

    def __init__(self, num_floors, num_rooms):
        self.height = num_floors
        self.width = num_rooms
        self.floors = {}

        # Only available when map data and loader are used: {"Q1": QuestRoom(...), "E1": ElevatorExit(...)}
        # Must be manually maintained if API is used to create and populate the building.
        self.rooms_by_name = {}

    def create_room(self, floor_num, room_num, kind, map_id=0):
        """create a room and add it to the floor

        See add_room() for more detail.

        :Parameters:
            floor_num : int
            room_num : int
            kind : int
        """
        room_type = room_type_map[kind]
        room = room_type(floor_num, room_num, map_id)
        self.add_room(room)

    def add_rooms(self, *args):
        """add rooms as varargs or a sequence

        :param args: varargs or sequence of rooms
        :return: None
        """
        if args:
            seq = args if isinstance(args[0], _Room) else args[0]
            for r in seq:
                self.add_room(r)

    def add_room(self, room):
        """add a room

        A Room is a generic exit or space, e.g. QuestRoom, ElevatorExit, WindowExit. Only one room per room_num is
        allowed. Trying to add a room to an occupied room_num will result in RoomNumOccupiedException.

        For Stairwells and Elevators no check is done to insure they have the same room_num. It's unrealistic to have
        stairs run from top-right to bottom-left, for example, but you may wish to do some crazy thing like that.

        :Parameters:
            room: _Room
            A _Room object
        :return:
        """
        floor_num = room.floor_num
        room_num = room.room_num
        assert floor_num >= 1
        assert room_num >= 1
        assert floor_num <= self.height
        assert room_num <= self.width
        floor = self.get_floor(floor_num)
        if floor is None:
            # print('>>> Creating floor {}'.format(floor_num))
            floor = Floor(floor_num, self)
            self.floors[floor_num] = floor
        floor.add_room(room)

    def create_floor(self, floor_num):
        if floor_num in self.floors:
            logger.warning('Duplicate add: floor={}', floor_num)
        else:
            floor = Floor(floor_num, self)
            self.floors[floor.floor_num] = floor

    def add_floor(self, floor):
        if floor.floor_num in self.floors:
            logger.warning('Duplicate add: floor={}', floor.floor_num)
        else:
            self.floors[floor.floor_num] = floor

    def get_floor(self, floor_num):
        return self.floors.get(floor_num, None)

    def get_room(self, floor_num, room_num):
        logger.debug('get_room> floor_num={} room_num={}', floor_num, room_num)
        logger.debug('get_room> floor_num in self.floors: {}', floor_num in self.floors)
        if floor_num in self.floors:
            logger.debug('get_room> floors.rooms: {}', self.floors[floor_num].rooms.items())
            return self.floors[floor_num].get_room(room_num)
        else:
            return None

    def get_room_by_name(self, name):
        # Only available when map data and loader are used: {"Q1": QuestRoom(...), "E1": ElevatorExit(...)}
        # Must be manually maintained if API is used to create and populate the building.
        return self.rooms_by_name.get(name, None)

    def get_rooms_as_dict(self, floor_num):
        if floor_num in self.floors:
            return self.floors[floor_num].get_rooms_as_dict()
        else:
            return {}

    def get_rooms_as_list(self, floor_num):
        if floor_num in self.floors:
            return self.floors[floor_num].get_rooms_as_list()
        else:
            return []


def create_and_load_building(num_floors, num_rooms, building_data):
    # Parse the building_data:
    # 1. parse the docstring into lines
    # 2. keep lines that start with "NN|" or " NN| "; toss the rest
    # 3. split the line into 3-character substrings; strip whitespace off the ends, store in parsed_data
    logger.info('MAP> Parsing map')
    building = Building(num_floors, num_rooms)
    parsed_data = []
    for row in building_data.split('\n'):
        match_obj = re.match(r'^\s*(\d+)\|\s?(.*)$', row)
        if not match_obj:
            logger.info('MAP> toss: "{}"', row)
            continue
        logger.info('MAP> KEEP: "{}"', row)
        rooms = []
        rooms_str = match_obj.group(2)
        logger.info('MAP> rooms_str: "{}"', rooms_str)
        for room_num in range(0, len(rooms_str), 3):
            token = rooms_str[room_num:room_num + 3]
            logger.info('MAP> room_num {}: "token={}"', room_num, token)
            token = token.lstrip(' ').rstrip(' ')
            rooms.append(token)
        parsed_data.insert(0, rooms)
        logger.info('MAP> rooms: len={} {}', len(rooms), rooms)
    # For each floor:
    # 1. Interpret the room data
    # 2. Defer elevator and stairs until a matching room is found
    # 3. When elevator or stairs matching room is found, create and link them, add to the building
    # 4. When any other room type is found, create it and add to the building
    deferred = {}  # {"E1": (floor_num, room_num)}
    logger.info('MAP> Creating floors and rooms')
    for floor_num, row in reversed(list(enumerate(parsed_data))):
        # print('{:02d}| {}'.format(floor_num + 1, row))
        building.create_floor(floor_num + 1)
        for room_num, r in enumerate(row):
            if r == '':
                continue
            if r[0] in 'ES':
                # Elevator, Stairs
                logger.info('MAP> Passage read: {}', r)
                if r in deferred:
                    logger.info('MAP> Passage matched: {}', r)
                    floor_num2, room_num2 = deferred.pop(r)
                    map_id = int(r[1:3])
                    if r[0] == 'E':
                        logger.info('MAP> Making Elevator: {}', r)
                        elevs = make_elevator(floor_num + 1, room_num + 1, floor_num2 + 1, room_num2 + 1, r, map_id)
                        building.add_rooms(elevs)
                        building.rooms_by_name[r] = elevs
                    elif r[0] == 'S':
                        logger.info('MAP> Making Stairwell: {}, r')
                        stairs = make_stairwell(floor_num + 1, room_num + 1, floor_num2 + 1, room_num2 + 1, r, map_id)
                        building.add_rooms(stairs)
                        building.rooms_by_name[r] = stairs
                else:
                    logger.info('MAP> Passage deferred: {}', r)
                    deferred[r] = floor_num, room_num
            elif r[0] in 'QAHTWLW':
                # Quest, Ambush, Health, Treasure, Window, Lobby, Window
                logger.info('MAP> Room read: {}', r)
                map_id = int(r[1:3])
                room_type = room_type_map[r[0]]
                logger.info('MAP> Making {}: {}', room_type.__name__, r)
                logger.info('MAP> floor_num={} room_num={}', floor_num, room_num)
                room = room_type(floor_num + 1, room_num + 1, r, map_id)
                if r == 'Q1':
                    room.location = settings.LOCATION_FRONT
                else:
                    room.location = settings.LOCATION_ROOM
                building.add_room(room)
                logger.info('MAP> Storing room by name: {}', r)
                building.rooms_by_name[r] = room
    logger.info('MAP> Rooms: {}', building.rooms_by_name.items())
    return building


if __name__ == '__main__':
    def test1():
        b = Building(10, 20)

        # add to building via Room
        r1 = QuestRoom(3, 1, "Q1")
        r1.add_to(b)

        # add to floor via Room
        r2 = QuestRoom(3, 2, "Q2")
        f = b.get_floor(3)
        r2.add_to(f)

        # add directly to Floor
        r3 = QuestRoom(3, 3, "Q3")
        f.add_room(r3)

        # add via Building
        r4 = QuestRoom(3, 4, "Q4")
        b.add_room(r4)
        b.create_room(3, 5, KIND_QUEST_ROOM)

        # create a passages
        # these return a tuple: exit1, exit2
        p1 = make_passage(KIND_ELEVATOR_EXIT, 10, 1, 1, 1, "E1", "1")
        p2 = make_passage(KIND_STAIRWELL_EXIT, 10, 2, 1, 2, "S2", "2")
        p3 = make_elevator(10, 3, 1, 3, "E3", "3")
        p4 = make_stairwell(10, 4, 1, 4, "S4", "4")
        # add them - test as list and as varargs
        b.add_rooms(p1 + p2)
        b.add_rooms(*(p3 + p4))


    def test2():
        building_data = """
  | v  _  _  _  _  _  _  _  _  _10_  _  _  _  _  _  _  _  _20_  _  _  _  _  _  _  _  _  _30_  _  _  _  _  _  _  _  _  _40
04| S1    E1              W4                  E2       S2
03| #S    #E Q1    W3                         #E       #S
02| #S    #E                       W2         #E       #S
01| S1    E1          W1       L1             E2    Q2 S2                                                       Q39
  | ^  _  _  _  _  _  _  _  _  _10_  _  _  _  _  _  _  _  _20_  _  _  _  _  _  _  _  _  _30_  _  _  _  _  _  _  _  _  _40
        """

        b = create_and_load_building(4, 40, building_data)

        print('Walking the building, top to bottom...')
        print('Building : height={} width={} floor_count={}'.format(b.height, b.width, len(b.floors)))
        for floor_num in range(b.height, 0, -1):
            # print('Floor {}'.format(floor_num))
            floor = b.get_floor(floor_num)
            rooms = b.get_rooms_as_dict(floor_num)
            print('  Floor {}: count={}'.format(floor.floor_num, len(rooms)))
            for room_num, room in sorted(rooms.items()):
                print('    Room {}: {} (map_id: {})'.format(room_num, room.__class__.__name__, room.map_id))
                if room.kind in (KIND_ELEVATOR_EXIT, KIND_STAIRWELL_EXIT):
                    other_end = room.distant_end
                    print('      Other_end: floor_num={} room_num={}'.format(other_end.floor_num, other_end.room_num))

        q1 = b.get_room_by_name('Q1')
        print('Q1: floor_num={} room_num={}'.format(q1.floor_num, q1.room_num))


    test2()

logger.debug("imported")
