# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'hero.py' is part of pw-__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import sys
from collections import defaultdict

import pygame

from gamelib import settings
from gamelib.entities.entities import MovingEntity
from gamelib.entities.weapons import WeaponSystem, Blaster, RocketLauncher, GrenadeLauncher, RopeLauncher
from gamelib.settings import KIND_HERO, EVT_HERO_STATE_CHANGED
from pyknic.ai.statemachines import StateDrivenAgentBase, BaseState
from pyknic.mathematics import Vec3, Point3

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []    # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")

logger.debug("imported")


class _HeroBaseState(BaseState):

    @staticmethod
    def handle_action(owner, action, extra):
        pass

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        # owner.velocity = owner.action_velocity  # apply velocity from action
        # logger.debug("hero force {0}", owner.force)
        MovingEntity.update(owner, dt, sim_t)


class Standing(_HeroBaseState):

    @staticmethod
    def enter(owner):
        owner.velocity.x = 0
        owner.velocity.y = 0
        owner.velocity.z = 0

    @staticmethod
    def handle_action(owner, action, extra):
        if action == settings.ACTION_HERO_JUMP:
            owner.state_machine.switch_state(Jump)
            return
        elif action == settings.ACTION_HERO_CROUCH:
            owner.state_machine.switch_state(Cowering, action=_shrink)
            return
        elif owner.action_state[1] - owner.action_state[0] != 0 or owner.action_state[3] - owner.action_state[2] != 0:
            owner.state_machine.switch_state(Walking)


class Cowering(_HeroBaseState):

    @staticmethod
    def enter(owner):
        owner.velocity.x = 0
        owner.velocity.y = 0
        owner.velocity.z = 0

    @staticmethod
    def handle_action(owner, action, extra):
        if action == settings.ACTION_HERO_JUMP:
            owner.state_machine.switch_state(Jump, action=_grow)
            return
        elif action == settings.ACTION_HERO_STAND:
            owner.state_machine.switch_state(Standing, action=_grow)
            return
        elif owner.action_state[1] - owner.action_state[0] != 0 or owner.action_state[3] - owner.action_state[2] != 0:
            owner.state_machine.switch_state(Crouching)


class Jump(_HeroBaseState):
    gravity = Vec3(0.0, 0.0, 1000.0)  # todo move this to settings?

    @staticmethod
    def enter(owner):
        owner.force.z -= 15000 * 1.5
        owner.max_speed = sys.maxsize

    @staticmethod
    def exit(owner):
        owner.force = Vec3.zero()
        owner.max_speed = settings.hero_max_speed
        owner.position.z = 0.0
        owner.force = Vec3.zero()
        owner.velocity.z = 0.0

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.force += Jump.gravity
        owner.velocity.x = (owner.action_state[1] - owner.action_state[0]) * settings.hero_max_speed
        logger.debug("hero force {3}{0}, v: {1}, p: {2}", owner.force, owner.velocity, owner.position,
                     owner.action_state)
        MovingEntity.update(owner, dt, sim_t)

        if owner.position.z > 0:  # todo: is 0 always the ground level?
            if owner.velocity.x != 0:
                owner.state_machine.switch_state(Walking)
            else:
                owner.state_machine.switch_state(Standing)


class _Moving(_HeroBaseState):
    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.velocity.x = (owner.action_state[1] - owner.action_state[0])
        owner.velocity.y = (owner.action_state[3] - owner.action_state[2])
        owner.velocity.length = owner.max_speed
        MovingEntity.update(owner, dt, sim_t)


class Walking(_Moving):
    @staticmethod
    def handle_action(owner, action, extra):
        if not (owner.action_state[1] - owner.action_state[0] != 0 or owner.action_state[3] - owner.action_state[
            2] != 0):
            owner.state_machine.switch_state(Standing)
            return
        elif action == settings.ACTION_HERO_CROUCH:
            owner.state_machine.switch_state(Crouching, action=_shrink)
            return
        elif action == settings.ACTION_HERO_JUMP:
            owner.state_machine.switch_state(Jump)
            return


def _grow(owner):
    owner.height *= 2


def _shrink(owner):
    owner.height /= 2


class Crouching(_Moving):

    @staticmethod
    def handle_action(owner, action, extra):
        if action == settings.ACTION_HERO_JUMP:
            owner.state_machine.switch_state(Jump, action=_grow)
            return
        elif action == settings.ACTION_HERO_STAND:
            owner.state_machine.switch_state(Walking, action=_grow)
            return
        elif not (owner.action_state[1] - owner.action_state[0] != 0 or owner.action_state[3] - owner.action_state[
            2] != 0):
            owner.state_machine.switch_state(Cowering)


class Climbing(_Moving):

    @staticmethod
    def enter(owner):
        owner.max_speed = settings.hero_climb_speed
        owner.velocity.x = 0
        owner.velocity.y = 0

    @staticmethod
    def exit(owner):
        owner.max_speed = settings.hero_max_speed

    @staticmethod
    def handle_action(owner, action, extra):
        if action == settings.ACTION_HERO_JUMP:
            owner.state_machine.switch_state(Falling)
            return

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        # owner.velocity.x = (owner.action_state[1] - owner.action_state[0])
        owner.velocity.z = (owner.action_state[3] - owner.action_state[2])
        owner.velocity.length = owner.max_speed
        MovingEntity.update(owner, dt, sim_t)


class Falling(_HeroBaseState):
    gravity = Vec3(0.0, 0.0, 1000.0)  # todo move this to settings?

    @staticmethod
    def enter(owner):
        owner.max_speed = sys.maxsize
        # # owner_y = int(owner.position.z + owner.position.y)
        # # r = pygame.Rect(int(owner.position.x), owner_y, 1, 1)
        # # for y in range(owner_y, owner_y + settings.floor_height_px*2, 20):
        # #     r.y = y
        # #     idx = r.collidelist(owner.world.walkable_areas)
        # #     if idx < 0:
        # #         continue
        # #     owner.walk_are = owner.world.walkable_areas[idx]
        # #     break
        # pz = owner.position.z + owner.position.y
        # areas = [_a for _a in owner.world.walkable_areas if pz < _a.z]
        # owner.walk_area = areas[-1]



    @staticmethod
    def exit(owner):
        owner.force = Vec3.zero()
        owner.max_speed = settings.hero_max_speed
        # owner.position.y = owner.walk_area.rect.centery
        owner.position.z = 0.0
        owner.force = Vec3.zero()
        owner.velocity.z = 0.0

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.force += Jump.gravity
        owner.velocity.x = (owner.action_state[1] - owner.action_state[0]) * settings.hero_max_speed
        logger.debug("hero force {3}{0}, v: {1}, p: {2}", owner.force, owner.velocity, owner.position,
                     owner.action_state)
        MovingEntity.update(owner, dt, sim_t)

        if owner.position.z > 0:  # todo: is 0 always the ground level?
        # if owner.position.z + owner.position.y > owner.walk_area.z:  # todo: is 0 always the ground level?
            owner.state_machine.switch_state(Standing)


class Hero(MovingEntity, StateDrivenAgentBase):

    def __init__(self, position, aabb, world, scheduler, initial_state, event_dispatcher):
        MovingEntity.__init__(self, KIND_HERO, position, settings.hero_max_speed, settings.hero_mass, aabb)
        StateDrivenAgentBase.__init__(self, initial_state)
        self.radius = aabb.width/2
        self.event_dispatcher = event_dispatcher
        self.health = settings.hero_starting_health
        self.height = aabb.h
        self.action_velocity = Vec3(0, 0)
        self.action_state = defaultdict(bool)  # {id: bool}
        self.state_machine.event_switched_state += self._on_state_switched
        self.hit_rect = pygame.Rect(0, 0, 1, 1)
        self.walk_area = None
        self.target = Point3(0, 0, 0)
        self.weapon_system = WeaponSystem(world, event_dispatcher)
        self.weapon_system.add_weapon(Blaster(self, world, scheduler, event_dispatcher))
        self.weapon_system.add_weapon(RocketLauncher(self, world, scheduler, event_dispatcher))  # todo: remove?
        self.weapon_system.add_weapon(GrenadeLauncher(self, world, scheduler, event_dispatcher))  # todo: remove?
        # self.weapon_system.add_weapon(RopeLauncher(self, world, scheduler, event_dispatcher))  # todo: remove?
        self.facing_left = False
        self.world = world
        self.map_floor_num = 0
        self.map_room_num = 0
        self.arm_id = id(self)

    def handle_action(self, action, extra):
        if self.health <= 0:
            return
        if action == settings.ACTION_HERO_MOVE_LEFT:
            self.action_state[0] = True
        elif action == settings.ACTION_HERO_STOP_LEFT:
            self.action_state[0] = False
        elif action == settings.ACTION_HERO_MOVE_RIGHT:
            self.action_state[1] = True
        elif action == settings.ACTION_HERO_STOP_RIGHT:
            self.action_state[1] = False
        elif action == settings.ACTION_HERO_MOVE_UP:
            self.action_state[2] = True
        elif action == settings.ACTION_HERO_STOP_UP:
            self.action_state[2] = False
        elif action == settings.ACTION_HERO_MOVE_DOWN:
            self.action_state[3] = True
        elif action == settings.ACTION_HERO_STOP_DOWN:
            self.action_state[3] = False

        self.state_machine.current_state.handle_action(self, action, extra)

        if action == settings.ACTION_HERO_AIM:
            pos, rel, buttons, world_pos = extra
            world_pos.z -= self.height / 2.0  ## todo : assume you shoot at this height
            world_pos.y -= world_pos.z
            self.target = world_pos
            self.weapon_system.aim_at(self.target)
        elif action == settings.ACTION_HERO_FIRE:
            self.weapon_system.fire()
        elif action == settings.ACTION_HERO_HOLD_FIRE:
            self.weapon_system.hold_fire()
        elif action == settings.ACTION_HERO_SWITCH:
            self.weapon_system.switch_primary_weapon()
        elif action == settings.ACTION_HERO_SWITCH_SECONDARY:
            self.weapon_system.switch_secondary_weapon()
        elif action == settings.ACTION_HERO_FIRE_SECONDARY:
            self.weapon_system.fire_secondary()
        elif action == settings.ACTION_HERO_HOLD_SECONDARY:
            self.weapon_system.hold_secondary()
        # elif action == settings.ACTION_HERO_SHOOT_GRAPPLING:
        #     pass
        elif action == settings.ACTION_HERO_USE:
            obj = self.world.get_usable_object_near(self.position)
            if obj:
                obj.use(self)

    def update(self, dt, sim_time):
        self.state_machine.current_state.update(self, dt, sim_time)
        self.target.x += self.position.x - self.old_position.x
        self.weapon_system.aim_at(self.target.clone())

        new_facing = self.target.x < self.position.x
        if new_facing != self.facing_left:
            self.facing_left = new_facing
            self.event_dispatcher.fire(settings.EVT_HERO_FACING_CHANGED, self)
            logger.debug("hero {0} facing left: {1}, {2}->{3}", self.kind, self.facing_left, self.position, self.target)

        self.weapon_system.update()

    @staticmethod
    def _on_state_switched(owner, previous, sm):
        owner.event_dispatcher.fire(EVT_HERO_STATE_CHANGED, owner)
        logger.debug("hero state change {0} -> {1}", previous, sm.current_state)

    def hit(self, bullet):
        self.explosion_damage(bullet.params.bullet_damage)
        self.event_dispatcher.fire(settings.EVT_HIT, bullet, self)

    def explosion_damage(self, damage):
        self.health -= damage
        if self.kind == KIND_HERO:
            settings.start_info.hero_health = self.health
        if self.health <= 0:
            self.event_dispatcher.fire(settings.EVT_DIED, self)
