# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'weapons.py' is part of pw-__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

from gamelib import settings
from gamelib.settings import EVT_BULLET_REMOVED, EVT_BULLET_CREATED, KIND_WEAPON_NONE
from pyknic.entity import BaseEntity, TypedEntity
from pyknic.timing import Timer

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class WeaponSystem(object):
    def __init__(self, world, event_dispatcher):
        self.event_dispatcher = event_dispatcher
        self.world = world
        self.current_weapon = NoneWeapon(_Weapon.SLOT1)
        self.current_weapon_secondary = NoneWeapon(_Weapon.SLOT2)
        self._is_shooting = False
        self._is_shooting_secondary = False
        self.arsenal = [self.current_weapon, self.current_weapon_secondary]
        self.target = None  # fixme?

    def aim_at(self, world_pos):
        self.target = world_pos

    def fire(self):
        self._is_shooting = True
        self.current_weapon.fire_at(self.target)

    def hold_fire(self):
        self._is_shooting = False

    def switch_primary_weapon(self):
        prev_weapon = self.current_weapon
        self.current_weapon = self._get_next_weapon(prev_weapon, self.current_weapon_secondary, _Weapon.SLOT1)
        self.event_dispatcher.fire(settings.EVT_WEAPON_SLO1_SWITCHED, prev_weapon, self.current_weapon)
        logger.debug("switched weapon {0} -> {1}", prev_weapon, self.current_weapon)

    def switch_secondary_weapon(self):
        prev_weapon = self.current_weapon_secondary
        self.current_weapon_secondary = self._get_next_weapon(prev_weapon, self.current_weapon, _Weapon.SLOT2)
        self.event_dispatcher.fire(settings.EVT_WEAPON_SLO2_SWITCHED, prev_weapon, self.current_weapon_secondary)
        logger.debug("switched weapon {0} -> {1}", prev_weapon, self.current_weapon_secondary)

    def _get_next_weapon(self, prev_weapon, other_slot_weapon, slot_compatibility):
        idx = self.arsenal.index(prev_weapon)
        next_weapon = other_slot_weapon
        while next_weapon == other_slot_weapon or next_weapon.slot_compatibility & slot_compatibility == 0:
            idx += 1
            idx %= len(self.arsenal)
            next_weapon = self.arsenal[idx]
        return next_weapon

    def add_weapon(self, weapon):
        if weapon not in self.arsenal:
            self.arsenal.append(weapon)
            self.arsenal.sort(key=lambda w: w.kind)
            if weapon.slot_compatibility & _Weapon.SLOT1:
                prev_weapon = self.current_weapon
                if prev_weapon.kind == KIND_WEAPON_NONE:
                    self.switch_primary_weapon()
                    self.arsenal.remove(prev_weapon)
            else:
                prev_weapon = self.current_weapon_secondary
                if prev_weapon.kind == KIND_WEAPON_NONE:
                    self.switch_secondary_weapon()
                    self.arsenal.remove(prev_weapon)

    def update(self):
        if self._is_shooting and self.current_weapon.is_automatic:
            self.current_weapon.fire_at(self.target)

        if self._is_shooting_secondary and self.current_weapon_secondary.is_automatic:
            self.current_weapon_secondary.fire_at(self.target)

    def fire_secondary(self):
        self._is_shooting_secondary = True
        self.current_weapon_secondary.fire_at(self.target)

    def hold_secondary(self):
        self._is_shooting_secondary = False


class _Weapon(TypedEntity):
    SLOT1 = 1 << 0
    SLOT2 = 1 << 1
    SLOTS_BOTH = SLOT1 | SLOT2

    def __init__(self,event_dispatcher, owner, world, scheduler, params, kind, is_automatic=True, slot=None):
        TypedEntity.__init__(self, kind)
        self.event_dispatcher = event_dispatcher
        self.slot_compatibility = (1 << 0) if slot is None else slot
        self.is_automatic = is_automatic
        self.is_ready = True
        self.timer = Timer(scheduler, 1.0 / params.firing_frequency, False)
        self.timer.event_elapsed += self._set_is_ready
        self.world = world
        self.owner = owner
        self.params = params

    def fire_at(self, target):
        if self.is_ready:
            self.timer.start()
            self.is_ready = False

            position = self.owner.position.clone()
            position.z -= self.owner.height / 2.0  # source position of bullet
            position.x += settings.hero_bullet_offset * (-1 if self.owner.facing_left else 1)
            bullet = Bullet(self.owner, target, self.params, position, Timer(self.timer.scheduler, repeat=False), self.event_dispatcher)
            self.event_dispatcher.fire(EVT_BULLET_CREATED, bullet)

    def _set_is_ready(self, *args):
        self.is_ready = True


class NoneWeapon(TypedEntity):

    def __init__(self, slot):
        TypedEntity.__init__(self, KIND_WEAPON_NONE)
        self.is_automatic = False
        self.slot_compatibility = slot

    def fire_at(self, target):
        pass


class Blaster(_Weapon):

    def __init__(self, owner, world, scheduler, event_dispatcher):
        params = settings.BlasterParams
        _Weapon.__init__(self, event_dispatcher, owner, world, scheduler, params, params.kind, slot=_Weapon.SLOT1)


class Bullet(BaseEntity):

    def __init__(self, owner, target, params, position, timer, event_dispatcher):
        BaseEntity.__init__(self, params.bullet_kind, position, params.bullet_radius)
        self.event_dispatcher = event_dispatcher
        self.owner = owner
        self.target = target
        self.timer = timer
        self.timer.interval = params.bullet_range / params.bullet_speed
        self.timer.event_elapsed += self._time_up
        self.timer.start()
        self.velocity = target - self.position
        self.velocity.length = params.bullet_speed
        self.params = params

    def _time_up(self, *args):
        self.event_dispatcher.fire(EVT_BULLET_REMOVED, self)


class RocketLauncher(_Weapon):

    def __init__(self, owner, world, scheduler, event_dispatcher):
        params = settings.RocketParams
        _Weapon.__init__(self,event_dispatcher, owner, world, scheduler, params, params.kind)


class GrenadeLauncher(_Weapon):

    def __init__(self, owner, world, scheduler, event_dispatcher):
        params = settings.GrenadeParams
        _Weapon.__init__(self,event_dispatcher, owner, world, scheduler, params, params.kind, False, _Weapon.SLOT2)
        self.is_ready = True

    def fire_at(self, target):
        if self.is_ready:
            self.is_ready = False
            self.timer.start()
            position = self.owner.position.clone()
            position.z -= self.owner.height / 2.0  # source position of bullet
            position.x += settings.hero_bullet_offset * (-1 if self.owner.facing_left else 1)
            bullet = Bullet(self.owner, target, self.params, position, Timer(self.timer.scheduler, repeat=False), self.event_dispatcher)
            direction = bullet.target - bullet.position
            distance = direction.length * 0.8
            if distance > self.params.bullet_range:
                distance = self.params.bullet_range  # clamp to range
            # v_initial = (distance * g ) ** 0.5    for a 45° angle
            direction.normalize()
            direction.z = -1  # 45° angle
            bullet.velocity = direction
            bullet.velocity.length = (distance * self.params.gravity.z) ** 0.5
            self.event_dispatcher.fire(EVT_BULLET_CREATED, bullet)


class RopeLauncher(_Weapon):

    def __init__(self, owner, world, scheduler, event_dispatcher):
        params = settings.RopeLauncherParams
        _Weapon.__init__(self,event_dispatcher, owner, world, scheduler, params, params.kind, is_automatic=False, slot=_Weapon.SLOT2)


logger.debug("imported")
