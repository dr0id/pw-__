# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'entities.py' is part of pw-__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

import pygame

from gamelib import settings
from gamelib.settings import KIND_WALKABLE_AREA, KIND_USE, KIND_ROPE, KIND_USE_OBJECT, EVT_OBJECT_USED
from pyknic.entity import BaseEntity, TypedEntity
from pyknic.mathematics import Vec3
from pyknic.timing import Timer

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class MovingEntity(BaseEntity):

    def __init__(self, kind, position, max_speed, mass, aabb):
        BaseEntity.__init__(self, kind, position, aabb=aabb)
        self.velocity = Vec3(0, 0)
        self.force = Vec3(0, 0)
        self.max_speed = max_speed
        self.mass = mass

    def update(self, dt, sim_time):
        self.old_position = self.position.clone()

        acc = self.force / self.mass
        self.velocity += acc * dt
        if self.velocity.length_sq > self.max_speed * self.max_speed:
            self.velocity.length = self.max_speed
        self.position += self.velocity * dt

        self.force = Vec3.zero()  # clear forces
        # logger.debug("player at: {0}, force {1}", self.position, self.force)


class WalkableArea(TypedEntity):

    def __init__(self, x, y, w, h, z):
        TypedEntity.__init__(self, KIND_WALKABLE_AREA)
        self.z = z
        self.rect = pygame.Rect(x, y, w, h)

    def __str__(self):
        return str(self.rect)

class AiRocketeer(object):
    interval = 1.0
    actions = [
        (settings.ACTION_HERO_CROUCH, 0.3),
        (settings.ACTION_HERO_FIRE, 1.0),
        (settings.ACTION_HERO_HOLD_FIRE, 0.1),
        (settings.ACTION_HERO_STAND, 0.3),
        (settings.ACTION_HERO_MOVE_UP, 0.5),
        (settings.ACTION_HERO_STOP_UP, 0.1),
        (settings.ACTION_HERO_CROUCH, 0.3),
        (settings.ACTION_HERO_FIRE, 1.0),
        (settings.ACTION_HERO_HOLD_FIRE, 0.1),
        (settings.ACTION_HERO_STAND, 0.3),
        (settings.ACTION_HERO_MOVE_DOWN, 0.5),
        (settings.ACTION_HERO_STOP_DOWN, 0.1),
    ]
    initial_actions = [settings.ACTION_HERO_SWITCH]  # only use switch weapon or so

class AiBulletMan(object):
    interval = 1.0
    actions = [
        (settings.ACTION_HERO_FIRE, 1.0),
        (settings.ACTION_HERO_HOLD_FIRE, 2.0),
    ]
    initial_actions = []  # only use switch weapon or so

class AiGrenadier(object):
    interval = 1.0
    actions = [
        (settings.ACTION_HERO_FIRE, 1.0),
        (settings.ACTION_HERO_HOLD_FIRE, 2.0),
        (settings.ACTION_HERO_FIRE_SECONDARY, 0.5),
        (settings.ACTION_HERO_HOLD_SECONDARY, 2.0),
        (settings.ACTION_HERO_MOVE_LEFT, 1.0),
        (settings.ACTION_HERO_STOP_LEFT, 1.0),
    ]
    initial_actions = []  # only use switch weapon or so



class Ai(object):

    def __init__(self, npc, hero, scheduler, world, params):
        self.initial_actions = list(params.initial_actions)
        self.actions = list(params.actions)
        self.world = world
        self.scheduler = scheduler
        self.hero = hero
        self.npc = npc
        self.timer = Timer(scheduler, params.interval, True)
        self.timer.event_elapsed += self.check_behavior
        self.timer.start()

    def check_behavior(self, *args):
        for a in self.initial_actions:
            self.npc.handle_action(a, None)
        self.initial_actions = []

        # move towards hero
        self.npc.handle_action(settings.ACTION_HERO_STOP_LEFT, None)
        self.npc.handle_action(settings.ACTION_HERO_STOP_RIGHT, None)
        direction = self.hero.position - self.npc.position
        bullet_range = self.npc.weapon_system.current_weapon.params.bullet_range * 0.8
        max_view_dist = settings.screen_width // 2 - 20
        if max_view_dist < bullet_range:
            bullet_range = max_view_dist
        if direction.length_sq > bullet_range * bullet_range:
            move_action = settings.ACTION_HERO_MOVE_LEFT if direction.x < 0 else settings.ACTION_HERO_MOVE_RIGHT
            self.npc.handle_action(move_action, None)
        else:
            # combat
            pos = self.hero.position.clone()
            pos.y -= self.npc.height / 2  # target chest
            pos.z = 0
            extra = 0, 0, 0, pos  # todo: add variance?
            self.npc.handle_action(settings.ACTION_HERO_AIM, extra)

            action = self.actions.pop(0)
            a, t = action
            self.timer.interval = t
            self.npc.handle_action(a, None)
            self.actions.append(action)


class Usable(TypedEntity):

    def __init__(self, kind):
        TypedEntity.__init__(self, kind | KIND_USE)

    def use(self, the_hero):
        raise NotImplementedError()


class Rope(Usable):

    def __init__(self, position, length):
        Usable.__init__(self, KIND_ROPE)
        self.position = position
        self.length = length

    def use(self, the_hero):
        from gamelib.entities import hero
        the_hero.state_machine.switch_state(hero.Climbing)
        the_hero.position.x = self.position.x
        the_hero.position.y = self.position.y
        the_hero.position.z = -10


class UseObject(Usable):

    def __init__(self, use_object_id, position, event_dispatcher):
        Usable.__init__(self, KIND_USE_OBJECT)
        self.event_dispatcher = event_dispatcher
        self.position = position
        self.use_object_id = use_object_id

    def use(self, the_hero):
        logger.error('UseObject> use_object_id={}', self.use_object_id)
        self.event_dispatcher.fire(EVT_OBJECT_USED, self.use_object_id)

    def __str__(self):
        return str(self.use_object_id)


class Explosion(TypedEntity):

    def __init__(self, position, radius, owner, damage):
        self.damage = damage
        self.owner = owner
        kind = settings.KIND_EXPLOSION
        TypedEntity.__init__(self, kind)
        self.position = position
        self.radius = radius
        self.health = 10

    def update(self, *args):
        self.health -= 10


logger.debug("imported")
