# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'game_context.py' is part of pw---__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The intro context

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import os
from itertools import chain

import pygame

from gamelib import settings
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.timing import Scheduler

chain_from_iterable = chain.from_iterable

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class IntroContext(TimeSteppedContext):

    def __init__(self):
        TimeSteppedContext.__init__(self, settings.MAX_DT, settings.STEP_DT, settings.DRAW_FPS)
        self._activables = []
        self.start_info = []
        self.game_clock = self._game_time
        self.scheduler = Scheduler()
        self.screen = None  # fixme: this is actually something of the client
        self.skyline_dx = 0.0
        self.helidx = -300
        self.count_out = 60
        self.quitting = False
        self.tick = 0
        self.hero1 = 0
        self.hero2 = 0
        self.hero3 = 0
        self.hero_jump = False
        self.hposx = 100
        self.hposy = 100

    def enter(self):
        TimeSteppedContext.enter(self)
        self.screen = pygame.display.get_surface()
        self.skyline_gfx = pygame.image.load(os.path.join('data', 'cutscene_fx', 'skyline.png')).convert()
        self.heli_gfx = pygame.image.load(os.path.join('data', 'cutscene_fx', 'helicopter.png')).convert()
        self.explode_gfx = pygame.image.load(os.path.join('data', 'cutscene_fx', 'explode.png')).convert()
        self.hero_gfx = pygame.image.load(os.path.join('data', 'cutscene_fx', 'hero.png')).convert()
        self.controller_gfx = pygame.image.load(os.path.join('data', 'cutscene_fx', 'controller.png')).convert()
        self.hero_board_gfx = pygame.image.load(os.path.join('data', 'cutscene_fx', 'hero_board.png')).convert()
        self.hero_board_gfx.set_colorkey((255,0,255))
        
        self.chopper_sound = pygame.mixer.Sound(os.path.join('data', 'sound', 'chopper1.ogg'))
        self.explode_sound = pygame.mixer.Sound(os.path.join('data', 'sound', 'grenade1.ogg'))
        self.vocal1 = pygame.mixer.Sound(os.path.join('data', 'voice', 'dialogue1.ogg'))
        self.vocal2 = pygame.mixer.Sound(os.path.join('data', 'voice', 'dialogue2.ogg'))
        self.vocal_channel = pygame.mixer.Channel(7)
    
    def resume(self):

        # Start up main game music loop
        pygame.mixer.music.set_volume(0.5)
        pygame.mixer.music.load(os.path.join('data', 'music', 'tutorial.ogg'))
        pygame.mixer.music.set_volume(settings.master_volume)
        pygame.mixer.music.play(-1)
        self.vocal_channel = pygame.mixer.Channel(7)

    def update_step(self, dt, sim_time, *args):
        self.scheduler.update(dt, sim_time, *args)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quitting = True
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                self.quitting = True
        
        if self.tick == 0:
        	self.chopper_sound.play()
        
        self.skyline_dx += 5
        self.skyline_dx = self.skyline_dx % 904
        
        if self.helidx < 100:
            self.helidx += 5
        
        if self.tick == 200:
        	self.vocal_channel.play(self.vocal1)
        
        if self.hero1 == 0 and self.tick > 200 and not self.vocal_channel.get_busy():
        	self.hero1 = self.tick
        
        if self.hero2 == 0 and self.hero1 > 0 and self.tick > (self.hero1+100):
        	self.vocal_channel.play(self.vocal2)
        	self.hero2 = self.tick
        
        if self.hero3 == 0 and self.hero2 > 0 and not self.vocal_channel.get_busy():
        	self.hero3 = self.tick
        
        if self.hero3 > 0 and self.tick > (self.hero3+50):
        	if not self.hero_jump:
        		self.explode_sound.play()
        	self.hero_jump = True
        
        if self.hero_jump:
        	self.hposx += 5
        	self.hposy += 5
        
        if self.hposx > 1024:
        	self.quitting = True
        
        self.tick += 1
        
        if self.quitting:
            self.count_out -= 1
            if self.count_out == 0:
                pygame.mixer.music.stop()
                pygame.mixer.music.set_volume(settings.master_volume)
                self.vocal_channel.stop()
                self.pop()
                return
            pygame.mixer.music.set_volume(settings.master_volume * (self.count_out / 30.0))

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        self.screen.fill((147,195,234))
        self.screen.blit(self.skyline_gfx, (-self.skyline_dx, 468))
        self.screen.blit(self.skyline_gfx, (-self.skyline_dx+904, 468))
        self.screen.blit(self.skyline_gfx, (-self.skyline_dx+1808, 468))
        if not self.hero_jump:
            self.screen.blit(self.heli_gfx, (self.helidx, 100))
        
        if self.tick > 150:
        	self.screen.blit(self.controller_gfx, (700, 50))
        if self.hero1 > 0 and self.tick > (self.hero1+100):
        	self.screen.blit(self.hero_gfx, (100, 300))
        
        if self.hero_jump and self.hposx < 1024:
        	self.screen.blit(self.explode_gfx, (80, 80))
        	sizex = self.hposx/2
        	surf2 = pygame.transform.scale(self.hero_board_gfx, (int(sizex), int((593.0/500.0)*sizex)))
        	self.screen.blit(surf2, (self.hposx, self.hposy))
        
        pygame.display.flip()

    def _on_quit(self, *args):
        self.quitting

    # def _on_action(self, action, extra):
    #     if action == ACTION_QUIT:
    #         event_dispatcher.fire(EVT_QUIT)
    #     else:
    #         self.quitting = True


logger.debug("imported")
