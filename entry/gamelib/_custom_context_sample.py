# -*- coding: utf-8 -*-
from __future__ import print_function

import logging

from gamelib.settings import EVT_QUIT
from pyknic.events import EventDispatcher
from pyknic.pyknic_pygame.context import TimeSteppedContext
from pyknic.timing import Scheduler
from pyknic.tweening import Tweener



__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class CustomContext(TimeSteppedContext):

    def __init__(self, client, space, start_info):
        TimeSteppedContext.__init__(self)
        self.start_info = start_info
        self.space = space
        self.client = client
        self.game_clock = self._game_time
        self.update_entities = set()
        self.scheduler = Scheduler()
        self.tweener = Tweener()
        self.event_dispatcher = EventDispatcher()

    def enter(self):
        TimeSteppedContext.enter(self)
        self.event_dispatcher.register_event_type(EVT_QUIT)

    def update_step(self, delta, sim_time, *args):
        self.scheduler.update(delta, sim_time, *args)

        # todo: update entities

        self.client.update_step(delta, sim_time, *args)

        self.tweener.update(delta)

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        self.client.draw(screen, do_flip, interpolation_factor)

    def _on_quit(self, *args):
        self.pop()


logger.debug("imported")
