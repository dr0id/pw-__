# -*- coding: utf-8 -*-

from gamelib.entities.building import Building
from gamelib.common_context import CommonContext


class HallwayContext(CommonContext):

    def __init__(self, client, space, start_info, event_dispatcher):
        CommonContext.__init__(self, client, space, start_info, event_dispatcher)
        # TODO

    def enter(self):
        # TODO
        CommonContext.enter(self)

    def load_location(self):
        si = self.start_info
        building = si.building
        floor_num = si.floor_num
        rooms = building.get_rooms_as_dict(floor_num)
        mission_playbook = si.mission_playbook
        for room_num, room in sorted(rooms.items()):
            # todo: create entities, sprites, triggers
            pass
