# -*- coding: utf-8 -*-

import logging

from gamelib.settings import STEP_DT, \
    OBJECTIVE_GO_TO_ROOM, OBJECTIVE_SEE_MAN, OBJECTIVE_STOP_MAN, OBJECTIVE_GET_MAN, \
    OBJECTIVE_GET_INFO, OBJECTIVE_GET_ITEM, OBJECTIVE_USE_ITEM, \
    LOCATION_FRONT, LOCATION_HALLWAY, LOCATION_ROOM, LOCATION_ELEVATOR, LOCATION_STAIRWELL, \
    OBJECTIVE_PLANNED, OBJECTIVE_IN_PROGRESS, OBJECTIVE_ABORTED, OBJECTIVE_ACCOMPLISHED, \
    EVT_COMMS_EMIT, mission_chatter_speed


logger = logging.getLogger(__name__)
logger.debug("importing...")


class Cine(object):
    def __init__(self, dialog=None, delay=2.0, speed=mission_chatter_speed):
        self.dialog = dialog if dialog is None else list(dialog)
        self.delay = delay
        self.speed = speed
        self.countdown = 0.0
        self.started = False

        self.fire = None

    def is_playing(self):
        return self.started and bool(self.dialog)

    def play(self, dt):
        self.started = True
        if self.delay > 0:
            self.delay -= dt  # fixme: this runs too slow; possibly self.speed is in the way
            return
        if self.dialog and self.countdown <= 0:
            # TODO emit to screen
            msg = self.dialog.pop(0)
            logger.info('Mission_Playbook> Cine: msg={}', msg)
            self.fire(EVT_COMMS_EMIT, msg)
            self.countdown = self.speed
        self.countdown -= dt / 2.0  # fixme: this runs twice as fast as it should


class _MissionObjective(object):
    objective_id = None

    def __init__(self, mission_playbook, room_name, need_target=None, need_instrument=None, need_kill=False, cine_a=None, cine_z=None):
        self.mission_playbook = mission_playbook
        self.start_info = mission_playbook.start_info

        self.room_name = room_name              # hero must be in this room (automatic check)
        self.need_target = need_target          # hero must use the target (manual call to trigger_use())
        self.need_instrument = need_instrument  # hero must have the instrument (manual call to trigger_instrument())
        self.need_kill = need_kill              # nero must kill or take the target (manual call to trigger_kill())

        # if these are True, the requirements are satisfied
        # hero has instrument
        # hero used target (starts cinematic_a)
        # hero has killed or taken target (starts cinematic_z)
        self.has_instrument = False if need_instrument else True
        self.has_used_target = False if need_target else True
        self.has_killed_target = False if need_kill else True

        self.cinematic_a = cine_a       # cinematic played when objective begins: "You'll never get me, copper!"
        self.cinematic_z = cine_z       # cinematic played when objective is accomplished: "I'm innocent, I tell ya!"
        self.cine_playing = None        # temp, hold the current cinematic that is playing

        self.state = OBJECTIVE_PLANNED
        self.unlocked = False           # when True, this objective has been accomplished

    def trigger_instrument(self):
        self.has_instrument = True

    def trigger_use(self):
        if self.has_instrument:
            self.has_used_target = True
        else:
            # TODO: instruct hero to acquire the instrument
            pass

    def trigger_kill(self):
        if self.has_instrument:
            if self.has_used_target:
                self.has_killed_target = True
        else:
            # TODO: instruct hero to acquire the instrument
            pass

    def is_in_the_room(self):
        si = self.start_info
        go = si.building.get_room_by_name(self.room_name)
        si_pos = si.floor_num, si.room_num
        go_pos = go.floor_num, go.room_num
        # logger.info('name={} go={} si_pos={} go_pos={}', self.room_name, go, si_pos, go_pos)
        return self.room_name == "Q1" or si.location == go.location and si_pos == go_pos

    def is_objective_fulfilled(self):
        return self.is_in_the_room() and self.has_instrument and self.has_used_target and self.has_killed_target

    def is_cine_pending(self):
        return self.cinematic_a and self.cinematic_a.dialog or \
               self.cinematic_z and self.cinematic_z.dialog

    def is_cine_playing(self):
        return self.cine_playing and self.cine_playing.is_playing()

    def is_all_concluded(self):
        return self.is_objective_fulfilled() and not self.is_cine_pending()

    def start_cine(self, cinema):
        if cinema:
            self.cine_playing = cinema
            self.cine_playing.play(0)

    def update(self, dt):
        pass


class ObjectiveGoToRoom(_MissionObjective):
    """
    si = StartInfo(b, floor_num=2, room_num=2)
    playbook = MissionPlaybook(si)
    ca = Cine(["Comms: You did it.", "Comms: Return to the roof for extraction."])
    o = ObjectiveGoToRoom(playbook, "Q1", cine_a=ca)
    playbook.add_objective(o)
    playbook.start_mission()
    si.location = settings.LOCATION_ROOM
    # no triggers, objective completes upon entering the room
    if playbook.is_mission_accomplished():
        WIN()
    """
    objective_id = OBJECTIVE_GO_TO_ROOM

    def update(self, dt):
        # this mission objective cannot be aborted
        if self.is_cine_playing():
            self.cine_playing.play()
            return
        if self.state == OBJECTIVE_PLANNED:
            if self.is_in_the_room():
                self.state = OBJECTIVE_IN_PROGRESS
                logger.info('Mission_Playbook> OBJECTIVE IN PROGRESS')
                self.start_cine(self.cinematic_a)
        elif self.state == OBJECTIVE_IN_PROGRESS:
            if self.is_objective_fulfilled():
                self.state = OBJECTIVE_ACCOMPLISHED
                logger.info('Mission_Playbook> OBJECTIVE ACCOMPLISHED')
                self.start_cine(self.cinematic_z)
        elif self.state == OBJECTIVE_ACCOMPLISHED and not self.unlocked:
            if not self.is_cine_pending():
                logger.info('Mission_Playbook> OBJECTIVE UNLOCKED')
                self.unlocked = True


class ObjectiveSeeMan(_MissionObjective):
    """
    si = StartInfo(b, floor_num=2, room_num=2)
    playbook = MissionPlaybook(si)
    ca = Cine(["Man: Good, you're here!", "Hero: Gimme the goods.", "Man: Here. Good riddance!"])
    cz = Cine(["Comms: Get to the CHOPPA!"])
    o = ObjectiveSeeMan(playbook, "Q1", target='MAN_OBJECT', cine_a=ca, cine_z=cz)
    playbook.add_objective(o)
    playbook.start_mission()
    si.location = settings.LOCATION_ROOM
    use_target = playbook.now_objective.need_target  # hero vs. this entity
    playbook.now_objective.trigger_use()
    if playbook.is_mission_accomplished():
        WIN()
    """
    objective_id = OBJECTIVE_SEE_MAN

    def update(self, dt):
        # this mission objective cannot be aborted
        if self.is_cine_playing():
            self.cine_playing.play(dt)
            return
        if self.state == OBJECTIVE_PLANNED:
            # logger.error('self.is_in_the_room() {}', self.is_in_the_room())
            # logger.error('si: floor_num={} room_num={}', self.start_info.floor_num, self.start_info.room_num)
            # room = self.start_info.building.rooms_by_name[self.room_name]
            # logger.error('go: floor_num={} room_num={}', room.floor_num, room.room_num)
            if self.is_in_the_room() and self.has_used_target:
                self.state = OBJECTIVE_IN_PROGRESS
                logger.info('Mission_Playbook> OBJECTIVE IN PROGRESS')
                self.start_cine(self.cinematic_a)
        elif self.state == OBJECTIVE_IN_PROGRESS:
            logger.info(self.has_used_target, self.has_instrument, self.has_killed_target)
            if self.is_objective_fulfilled():
                self.state = OBJECTIVE_ACCOMPLISHED
                logger.info('Mission_Playbook> OBJECTIVE ACCOMPLISHED')
                self.start_cine(self.cinematic_z)
        elif self.state == OBJECTIVE_ACCOMPLISHED:
            if not self.is_cine_pending() and not self.unlocked:
                logger.info('Mission_Playbook> OBJECTIVE UNLOCKED')
                self.unlocked = True


class ObjectiveStopMan(_MissionObjective):
    """
    si = StartInfo(b, floor_num=2, room_num=2)
    playbook = MissionPlaybook(si)
    ca = Cine(["Man: You'll never get me, copper!", "Man: Your shoelace is untied!", "Hero: What? Hold on..."])
    cz = Cine(["Comms: Get to the CHOPPA.", "Comms: Get to the CHOPPA NOWWW!"])
    o = ObjectiveStopMan(playbook, "Q1", target='MAN_OBJECT', kill=True, cine_a=ca, cine_z=cz)
    playbook.add_objective(o)
    playbook.start_mission()
    si.location = settings.LOCATION_ROOM
    use_and_kill_target = playbook.now_objective.need_target  # hero vs. this entity
    playbook.now_objective.trigger_use()
    playbook.now_objective.trigger_kill()
    if playbook.is_mission_accomplished():
        WIN()
    """
    objective_id = OBJECTIVE_STOP_MAN

    def update(self, dt):
        # this mission objective cannot be aborted
        if self.is_cine_playing():
            self.cine_playing.play(dt)
            return
        if self.state == OBJECTIVE_PLANNED:
            if self.is_in_the_room() and self.has_used_target:
                self.state = OBJECTIVE_IN_PROGRESS
                logger.info('Mission_Playbook> OBJECTIVE IN PROGRESS')
                self.start_cine(self.cinematic_a)
        elif self.state == OBJECTIVE_IN_PROGRESS:
            if self.is_objective_fulfilled():
                self.state = OBJECTIVE_ACCOMPLISHED
                logger.info('Mission_Playbook> OBJECTIVE ACCOMPLISHED')
                self.start_cine(self.cinematic_z)
        elif self.state == OBJECTIVE_ACCOMPLISHED:
            if not self.is_cine_pending() and not self.unlocked:
                logger.info('Mission_Playbook> OBJECTIVE UNLOCKED')
                self.unlocked = True


class ObjectiveGetMan(ObjectiveStopMan):
    objective_id = OBJECTIVE_GET_MAN


class ObjectiveGetInfo(ObjectiveSeeMan):
    objective_id = OBJECTIVE_GET_INFO


class ObjectiveGetItem(ObjectiveSeeMan):
    objective_id = OBJECTIVE_GET_ITEM


class ObjectiveUseItem(ObjectiveSeeMan):
    """
    si = StartInfo(b, floor_num=2, room_num=2)
    playbook = MissionPlaybook(si)
    ca = Cine(["Info: You smash the cookie.", "Comms: Leave the crumbs, get to the CHOPPA."])
    o = ObjectiveUseItem(playbook, "Q1", need_target='COOKIE_OBJECT', need_instrument='HAMMER_OBJECT', cine_a=ca)
    playbook.add_objective(o)
    playbook.start_mission()
    playbook.now_objective.trigger_instrument()  # let objective know that hero has the instrument
    si.location = settings.LOCATION_ROOM
    use_target = playbook.now_objective.need_target  # hero vs. this entity
    playbook.now_objective.trigger_use()
    playbook.now_objective.trigger_kill()
    if playbook.is_mission_accomplished():
        WIN()
    """
    objective_id = OBJECTIVE_USE_ITEM


class MissionPlaybook(object):
    def __init__(self, start_info, event_dispatcher):
        start_info.mission_playbook = self
        self.start_info = start_info
        self.event_dispatcher = event_dispatcher
        self.objectives = []            # list of objectives
        self.now_objective = None
        self._mission_accomplished = False

    def trigger_use(self):
        if self.now_objective:
            logger.info('Mission_Playbook> trigger use')
            self.now_objective.trigger_use()

    def trigger_instrument(self):
        if self.now_objective:
            logger.info('Mission_Playbook> trigger instrument')
            self.now_objective.trigger_instrument()

    def trigger_kill(self):
        if self.now_objective:
            logger.info('Mission_Playbook> trigger kill')
            self.now_objective.trigger_kill()

    def is_objective_accomplished(self):
        if not self.now_objective:
            return None
        if self.now_objective.is_all_concluded():
            return True
        return False

    def is_mission_accomplished(self):
        return self._mission_accomplished and not self.now_objective.is_cine_pending()

    def add_objective(self, obj):
        self.objectives.append(obj)

    def start_mission(self):
        # prevent multiple calls, only start once
        if not self.now_objective:
            self.advance_objective()

    def advance_objective(self):
        if self.objectives:
            logger.info('Mission_Playbook> Starting next mission objective')
            self.now_objective = self.objectives.pop(0)
            # todo: if this log msg doesn't work, just comment out the whole thing
            si = self.now_objective.start_info
            fn, rn = si.floor_num, si.room_num
            logger.info('Mission_Playbook> Coords: floor_num={} room_num={}', fn, rn)
            logger.info('Mission_Playbook> start_info={}', si)
            logger.info('Mission_Playbook> Rooms: {}', si.building.rooms_by_name.items())
            r = si.building.get_room(fn, rn)
            logger.info('Mission_Playbook> Objective: {} {}', self.now_objective.room_name, r)
            # todo: end
        else:
            logger.info('Mission_Playbook> MISSION ACCOMPLISHED')
            self.now_objective = None
            self._mission_accomplished = True

    def update(self, dt):
        if self.now_objective:
            o = self.now_objective

            # fixme: kludgy, but needed
            if o.cinematic_a:
                o.cinematic_a.fire = self.event_dispatcher.fire
            if o.cinematic_z:
                o.cinematic_z.fire = self.event_dispatcher.fire

            o.update(dt)

            if o.room_name == "Q1" and self.is_objective_accomplished():
                self.advance_objective()


if __name__ == '__main__':
    import time
    from gamelib import settings
    from gamelib.entities.building import Building, QuestRoom

    class StartInfo(object):
        def __init__(self, building=None, mission_num=1, floor_num=1, room_num=1, location=LOCATION_FRONT):
            self.building = building  # gamelib.building.Building
            self.mission_num = mission_num  # 1..
            self.floor_num = floor_num  # 1..building.height
            self.room_num = room_num  # 1..building.width
            self.location = location  # settings.LOCATION_*
            self.mission_playbook = None  # set by MissionPlaybook.__init__()

    def print_it(o):
        print('vars: room_name={} target={} instru={} kill={} has_i={} has_u={} has_k={}'.format(
            o.room_name, o.need_target, o.need_instrument, o.need_kill,
            o.has_instrument, o.has_used_target, o.has_killed_target))
        print('state: isroom={} ismission={} isplay={}\n---'.format(
            o.is_in_the_room(), o.is_objective_fulfilled(), o.is_cine_pending()))

    def test_go_to_room():
        b = Building(4, 4)
        r = QuestRoom(2, 2)
        b.add_room(r)
        b.rooms_by_name["Q1"] = r

        si = StartInfo(b, floor_num=2, room_num=2)
        playbook = MissionPlaybook(si)
        ca = Cine(["Comms: You did it.", "Comms: Return to the roof for extraction."])
        o = ObjectiveGoToRoom(playbook, "Q1", cine_a=ca)
        playbook.add_objective(o)
        playbook.start_mission()
        si.location = settings.LOCATION_ROOM
        running = True
        t = 0
        while running:
            time.sleep(0.03)
            t += 1
            if t % 10 == 0:
                print('time: {}'.format(t))

            if t == 0:
                print('START')
                playbook.update(0.03)
                print_it(o)
            elif t == 30:
                print('SET FLOOR 2')
                si.floor_num = 2
                playbook.update(0.03)
                print_it(o)
            elif t == 60:
                print('SET ROOM 2')
                si.room_num = 2
                playbook.update(0.03)
                print_it(o)
            elif t > 60:
                # print('Waiting')
                playbook.update(0.03)
                # print_it(o)

            if playbook.is_mission_accomplished():
                print('MISSION FINISHED')
                running = False

    def test_see_the_man():
        b = Building(4, 4)
        r = QuestRoom(2, 2)
        b.add_room(r)
        b.rooms_by_name["Q1"] = r

        si = StartInfo(b, floor_num=2, room_num=2)
        playbook = MissionPlaybook(si)
        ca = Cine(["Man: Good, you're here!", "Hero: Gimme the goods.", "Man: Here. Good riddance!"])
        cz = Cine(["Comms: Get to the CHOPPA!"])
        o = ObjectiveSeeMan(playbook, "Q1", need_target='MAN_OBJECT', cine_a=ca, cine_z=cz)
        playbook.add_objective(o)
        playbook.start_mission()
        si.location = settings.LOCATION_ROOM
        running = True
        t = 0
        while running:
            time.sleep(0.03)
            t += 1
            if t % 10 == 0:
                print('time: {}'.format(t))

            if t == 0:
                print('START')
                playbook.update(0.03)
                print_it(o)
            elif t == 10:
                print('SET FLOOR 2')
                si.floor_num = 2
                playbook.update(0.03)
                print_it(o)
            elif t == 20:
                print('SET ROOM 2')
                si.room_num = 2
                playbook.update(0.03)
                print_it(o)
            elif t == 30:
                print('USE MAN')
                o.trigger_use()
                print_it(o)
            elif t > 40:
                # print('Waiting')
                playbook.update(0.03)
                # print_it(o)

            if playbook.is_mission_accomplished():
                print('MISSION FINISHED')
                running = False

    def test_stop_the_man():
        b = Building(4, 4)
        r = QuestRoom(2, 2)
        b.add_room(r)
        b.rooms_by_name["Q1"] = r

        si = StartInfo(b, floor_num=2, room_num=2)
        playbook = MissionPlaybook(si)
        ca = Cine(["Man: You'll never get me, copper!", "Man: Your shoelace is untied!", "Hero: What? Hold on..."])
        cz = Cine(["Comms: Get to the CHOPPA.", "Comms: Get to the CHOPPA NOWWW!"])
        o = ObjectiveStopMan(playbook, "Q1", need_target='MAN_OBJECT', need_kill=True, cine_a=ca, cine_z=cz)
        playbook.add_objective(o)
        playbook.start_mission()
        si.location = settings.LOCATION_ROOM
        running = True
        t = 0
        while running:
            time.sleep(0.03)
            t += 1
            if t % 10 == 0:
                print('time: {}'.format(t))

            if t == 0:
                print('START')
                playbook.update(0.03)
                print_it(o)
            elif t == 10:
                print('SET FLOOR,ROOM = 2,2')
                si.floor_num = 2
                si.room_num = 2
                playbook.update(0.03)
                print_it(o)
            elif t == 20:
                print('TALK TO (USE) MAN')
                o.trigger_use()
                print_it(o)
            elif t >= 30 and not o.has_killed_target:
                o.trigger_kill()
                if o.has_killed_target:
                    print('STOP (KILL) MAN')
                    print_it(o)
            elif t > 40:
                # print('Waiting')
                playbook.update(0.03)
                # print_it(o)

            if playbook.is_mission_accomplished():
                print('MISSION FINISHED')
                running = False

    def test_use_the_item():
        b = Building(4, 4)
        r = QuestRoom(2, 2)
        b.add_room(r)
        b.rooms_by_name["Q1"] = r

        si = StartInfo(b, floor_num=2, room_num=2)
        playbook = MissionPlaybook(si)
        ca = Cine(["Info: You smash the cookie.", "Comms: Leave the crumbs, get to the CHOPPA."])
        o = ObjectiveUseItem(playbook, "Q1", need_target='COOKIE_OBJECT', need_instrument='HAMMER_OBJECT', cine_a=ca)
        playbook.add_objective(o)
        playbook.start_mission()
        playbook.now_objective.trigger_instrument()  # let objective know that hero has the instrument
        si.location = settings.LOCATION_ROOM
        running = True
        t = 0
        while running:
            time.sleep(STEP_DT)
            t += 1
            if t % 10 == 0:
                print('time: {}'.format(t))

            if t == 0:
                print('START')
                playbook.update(0.03)
                print_it(o)
            elif t == 50:
                print('SET FLOOR,ROOM = 2,2')
                si.floor_num = 2
                si.room_num = 2
                playbook.update(0.03)
                print_it(o)
            elif t == 100:
                print('GET (USE) THE COOKIE')
                o.trigger_use()
                print_it(o)
            elif t > 200:
                # print('Waiting')
                playbook.update(0.03)
                # print_it(o)

            if playbook.is_mission_accomplished():
                print('MISSION FINISHED')
                running = False

    # test_go_to_room()
    # test_see_the_man()
    # test_stop_the_man()
    test_use_the_item()


logger.debug("imported")
