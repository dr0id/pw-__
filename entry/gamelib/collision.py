# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'collision.py' is part of pw-__
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


collision_functions = {}  # {(type1, type2): func, ...}


def register_collision_func(kinds, func):
    assert kinds[0] < kinds[1], "kinds should be sorted! {0}".format(kinds)
    collision_functions[kinds] = func


def _no_collision(kinds):

    def _no_col(*args):
        print("no col function registered for kinds: {0} and args: {1}".format(kinds, args))

    return _no_col


def get_col_func(kinds):
    assert kinds[0] < kinds[1], "kinds should be sorted! {0}".format(kinds)
    return collision_functions.get(kinds, _no_collision(kinds))


def check_collisions(entities):
    kind_cache = {}
    for kinds, col_func in collision_functions.items():
        kind_a, kind_b = kinds
        entities_a = kind_cache.setdefault(kind_a, [_e for _e in entities if _e.kind & kind_a])
        entities_b = kind_cache.setdefault(kind_b, [_e for _e in entities if _e.kind & kind_b])
        # assert entities_a, "missing entities of type {0}".format(kind_a)
        # assert entities_b, "missing entities of type {0}".format(kind_b)
        col_func(entities_a, entities_b)

logger.debug("imported")
