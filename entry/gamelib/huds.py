# -*- coding: utf-8 -*-
from __future__ import print_function

import logging
import random

import pygame

from gamelib.hudlight import HUD
from gamelib import settings, pygametext


__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class IMFHUD(object):
    def __init__(self, mission_text, objective_text):
        if not isinstance(settings.title_hud_rect, pygame.Rect):
            settings.title_hud_rect = pygame.Rect(settings.title_hud_rect)
            settings.mission_hud_rect = pygame.Rect(settings.mission_hud_rect)
            settings.objective_hud_rect = pygame.Rect(settings.objective_hud_rect)
            settings.title_hud_rect = pygame.Rect(settings.title_hud_rect)
            settings.chatter_box_rect = pygame.Rect(settings.chatter_box_rect)
            settings.chatter_box_rect.bottom = settings.screen_height
            settings.tactical_box_rect = pygame.Rect(settings.tactical_box_rect)

        self.title_hud = HUD(**settings.font_themes['title'])
        self.mission_hud = HUD(**settings.font_themes['mission'])
        self.objective_hud = HUD(**settings.font_themes['mission'])
        self.tactical_hud = HUD(**settings.font_themes['tactical'])

        self.chatter_box = pygame.Surface(settings.chatter_box_rect.size)
        self.chatter_text = settings.hud_chatter_msgs
        self.chatter_text_new = list(settings.hud_chatter_msgs)
        logger.info('>>> {} {}', self.chatter_text, self.chatter_text_new)
        del self.chatter_text[:]
        self.chatter_images = []

        self.tactical_box = pygame.Surface(settings.tactical_box_rect.size)
        self.tactical_hud.add('Coords', 'Coords: floor {:0.0f} x room {:0.0f}', 0, 0)
        self.tactical_hud.add('Weapon', 'Weapon: {}', '')
        self.tactical_hud.add('Utility', 'Utility: {}', '')
        self.tactical_hud.add('Location', 'Location: {}', '')
        self.tactical_hud.add('Health', 'Health: {}%', '')

        self._set_title(settings.hud_title)
        self._set_mission(mission_text)
        self.set_objective(objective_text)

        self.update(0, 0, 'INIT')

    def _set_title(self, text):
        self.title_hud.add('text', text)
        settings.title_hud_rect.size = self.title_hud.hud_size()
        settings.title_hud_rect.centerx = settings.screen_width // 2
        self.title_hud.x = settings.title_hud_rect.x
        self.title_hud.y = settings.title_hud_rect.y

    def _set_mission(self, text):
        self.mission_hud.add('label', 'Mission:')
        self.mission_hud.add('text', text)
        settings.mission_hud_rect.size = self.mission_hud.hud_size()
        self.mission_hud.x = settings.mission_hud_rect.x
        self.mission_hud.y = settings.title_hud_rect.bottom + 5

    def set_objective(self, text):
        if 'text' in self.objective_hud.items:
            self.objective_hud.update_item('text', text)
        else:
            self.objective_hud.add('label', 'Objective:')
            self.objective_hud.add('text', text)
        settings.objective_hud_rect.size = self.objective_hud.hud_size()
        w, h = self.objective_hud.hud_size()
        self.objective_hud.x = settings.screen_width - 10 - w
        self.objective_hud.y = settings.title_hud_rect.bottom + 5

    def set_coords(self, floor_num, room_num):
        self.tactical_hud.update_item('Coords', floor_num, room_num)

    def set_weapon(self, name):
        self.tactical_hud.update_item('Weapon', name)

    def set_utility(self, name):
        self.tactical_hud.update_item('Utility', name)

    def set_location(self, location_id):
        # location_id: int, e.g. settings.LOCATION_FRONT
        self.tactical_hud.update_item('Location', settings.location_labels[location_id])

    def add_chatter(self, text):
        if text:
            self.chatter_text_new.append(text)

    def clear_chatter(self):
        del self.chatter_text[:]
        del self.chatter_images[:]

    def set_health(self, health):
        self.tactical_hud.update_item('Health', int(health))

    def update(self, *args):
        if self.chatter_text_new or 'INIT' in args:
            def sum_height():
                return sum([i.get_height() + 1 for i in self.chatter_images])

            # generate new text images; check fit and toss any that scroll off
            font_theme = settings.font_themes['chatter_box']
            for s in self.chatter_text_new:
                img = pygametext.getsurf(s, width=settings.chatter_box_rect.w - 4, **font_theme)
                self.chatter_text.append(s)
                self.chatter_images.append(img)
            del self.chatter_text_new[:]
            box_height = self.chatter_box.get_height()
            while sum_height() > box_height - 4:
                self.chatter_images.pop(0)
                self.chatter_text.pop(0)

            # render to the chatter_box surface
            self.chatter_box.fill(settings.hud_fill_color)
            self.chatter_box.set_alpha(128)
            pygame.draw.rect(self.chatter_box, settings.hud_border_color, self.chatter_box.get_rect(), 1)
            x = 2
            y = 2
            for img in self.chatter_images:
                self.chatter_box.blit(img, (x, y))
                y += img.get_height() + 1

        # update tactical hud
        flip_scolor = random.random() < 0.05
        flip_shadow = random.random() < 0.005
        if not settings.fx_enable_tactical_hud_flicker:
            flip_scolor = False
            flip_shadow = False
        if self.tactical_hud.dirty or flip_scolor or flip_shadow:
            self.tactical_box.fill((0, 0, 0))
            if settings.fx_enable_tactical_hud_flicker:
                if flip_scolor:
                    default = settings.font_themes['tactical']['scolor']
                    if self.tactical_hud.get_scolor() is default:
                        self.tactical_hud.set_scolor('grey10')
                    else:
                        self.tactical_hud.set_scolor(default)
                if flip_shadow:
                    shadow = list(self.tactical_hud.get_shadow())
                    i = int(random.random() * 2)
                    shadow[i] = -shadow[i]
                    self.tactical_hud.set_shadow(shadow)
            else:
                self.tactical_hud.set_scolor(None)
            self.tactical_hud.draw(self.tactical_box)
            w, h = self.tactical_box.get_size()
            pygame.draw.line(self.tactical_box, pygame.Color('grey50'), (0, 0), (w, 0), 2)
            pygame.draw.line(self.tactical_box, pygame.Color('grey50'), (0, 0), (0, h), 2)
            pygame.draw.line(self.tactical_box, pygame.Color('grey80'), (w-1, 0), (w-1, h-1))
            pygame.draw.line(self.tactical_box, pygame.Color('grey80'), (0, h-1), (w-1, h-1))
            self.tactical_box.set_alpha(196)

    def draw(self, surf, *args, **kwargs):
        self.title_hud.draw(surf)
        self.mission_hud.draw(surf)
        self.objective_hud.draw(surf)
        surf.blit(self.chatter_box, settings.chatter_box_rect)
        surf.blit(self.tactical_box, settings.tactical_box_rect)


logger.debug("imported")
