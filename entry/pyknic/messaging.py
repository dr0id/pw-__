# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'messaging.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module is about messaging. Passing messages from one entity to another.
"""
from __future__ import print_function, division

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["MessageDispatcher"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class _DummyReceiver(object):
    """
    The dummy receiver. Used internally by the MessageDispatcher for all messages sent to an unknown receiver.
    Unknown by the entity registry. See: MessageDispatcher.
    """

    def __init__(self, logger_to_use):
        """
        Constructor.
        :param logger_to_use: The logger to use be used, can be None to disable logging.
        """
        self._logger = logger_to_use

    def handle_message(self, sender, receiver, msg_type, extra):
        """
        Handle the messages for this entity. All it does it log a warning if logging is enabled.
        :param sender: The sender id.
        :param receiver: The receiver id.
        :param msg_type: The message type.
        :param extra: Extra info for that msg type.
        :return: False, always.
        """
        if self._logger is not None:
            self._logger.warning("Unknown receiver: from %s to %s type %s extra %s", sender, receiver, msg_type, extra)
        return False


class MessageDispatcher(object):
    """
    The MessageDispatcher class. Used to send messages to other entities.
    """

    def __init__(self, registry, logger_to_use=None, dummy_receiver_to_use=None):
        """
        Constructor.
        :param registry: The entity registry to use to find the receiver entity.
        :param logger_to_use: The logger instance to use. If set to None the module logger is used. False disables it.
        :param dummy_receiver_to_use: The dummy receiver for unknown id to use. All messages with unknown receiver
            will be sent here.
        """
        self._registry = registry
        self._logger = logger if logger_to_use is None else (None if logger_to_use is False else logger_to_use)
        self._dummy_receiver = _DummyReceiver(self._logger) if dummy_receiver_to_use is None else dummy_receiver_to_use

    def send(self, sender_id, receiver_id, msg_type, extra=None):
        """
        The send method.
        :param sender_id: The sender id.
        :param receiver_id: The receiver id.
        :param msg_type: The message type.
        :param extra: Extra info for a message type.
        :return: True if the receiver has accepted or similar, otherwise False.
        """
        entity = self._registry.by_id.get(receiver_id, self._dummy_receiver)
        if self._logger is not None and self._logger.level <= logging.DEBUG:
            self._logger.debug("send <Message %s from %s to %s extra: %s>", msg_type, sender_id, receiver_id, extra)
        return_value = entity.handle_message(sender_id, receiver_id, msg_type, extra)
        assert return_value is not None, "Receiver should return True of False: <Message %s from %s to %s extra: %s>" \
                                         % (msg_type, sender_id, receiver_id, extra)
        return return_value


# class _MessageBus(object):
#     _exchanges = {}  # {exchange_id: ...}
#
#     @staticmethod
#     def _publish(exchange_id, message):
#         exchanges = _MessageBus._exchanges
#         exchange = exchanges.get(exchange_id, None)
#         if exchange is None:
#             return  # TODO: let the publisher know no one was interested in this message?
#
#     @staticmethod
#     def _declare_queue(queue_id):
#         pass
#
#     @staticmethod
#     def _declare_exchange(exchange_id):
#         pass
#
#     @staticmethod
#     def _queue_bind(exchange_id, queue_id):
#         pass
#
#
# class Publisher(_MessageBus):
#
#     def __init__(self):
#         _MessageBus.__init__(self)
#
#     def publish(self, message, exchange_id=None):
#         pass
#
#
# class Subscriber(_MessageBus):
#     def __init__(self):
#         _MessageBus.__init__(self)
#
#     def subscribe(self, exchange_id, callback):
#         pass


logger.debug("imported")
