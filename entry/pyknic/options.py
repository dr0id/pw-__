# -*- coding: utf-8 -*-
"""
This module is about the options of pyknic... might be obsolete.
"""
from __future__ import print_function, division

import logging
from optparse import OptionParser

logger = logging.getLogger(__name__)
logger.debug("importing...")


parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
                  help="write report to FILE", metavar="FILE")
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")

mydict = {'mydictsetting': 1}

(opts, args) = parser.parse_args(["--file=outfile", "-q", "this is an argument"])

mydict.update(dict(opts.__dict__))

logger.debug("imported")

# just for testing manually
if __name__ == '__main__':
    print(opts, opts.__class__)
    print(mydict)
    print(args)



