# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'sfx.py' is part of pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Module for handling sfx and music.

Versioning scheme based on: http://en.wikipedia.org/wiki/Versioning#Designating_development_stage

::

      +-- api change, probably incompatible with older versions
      |     +-- enhancements but no api change
      |     |
    major.minor[.build[.revision]]                    # For instance:
                   |                                  * 1.2.0.1 instead of 1.2-a
                   +-|* 0 for alpha (status)          * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
                     |* 1 for beta (status)           * 1.2.2.3 instead of 1.2-rc (release candidate)
                     |* 2 for release candidate       * 1.2.3.0 instead of 1.2-r (commercial distribution)
                     |* 3 for (public) release        * 1.2.3.5 instead of 1.2-r5 (commercial dist with many bug fixes)


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

import pygame

from pyknic.events import WeakSignal

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2017"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["MusicData", "SoundData", "MusicPlayer", "SoundPlayer"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class SoundData(object):
    def __init__(self, full_path_to_file, volume, reserved_channel_id=None):
        self.filename = full_path_to_file
        self.volume = volume
        self.reserved_channel_id = reserved_channel_id
        self.channel = None
        self.sound = None


class MusicData(object):
    def __init__(self, full_path_to_file, volume):
        self.filename = full_path_to_file
        self.volume = volume


class MusicPlayer:
    def __init__(self, volume, music_ended_pygame_event_id):
        self.music_carousel = []
        self.music_paused = False
        self.music_data = {}
        assert volume >= 0.0, "{0} volume negative, should be in range [0, 1]".format(volume)
        assert volume <= 1.0, "{0} volume too big, should be in range [0, 1]".format(volume)
        self._volume = volume
        self.music_ended_pygame_event_id = music_ended_pygame_event_id
        self._is_muted = False
        self.event_muted = WeakSignal()
        self.event_volume_changed = WeakSignal()
        self.position = 0.0
        self.keep_last_position = 0.0
        self.keep_last_musictype = 'main'

    def change_volume(self, value):
        """
        Changes the volume by ge given amount.
        :param value: positive value to increase the volume, negative value to lower the the volume. The volume
            will be clamped to the [0, 1] range.
        """
        if pygame.mixer.get_init() is None:
            logger.error("Can't change music volume: mixer not yet initialized!")
            return
        new_volume = pygame.mixer.music.get_volume() + value
        if new_volume > 1.0:
            new_volume = 1.0
        if new_volume < 0:
            new_volume = 0.0
        self._set_mute(False)
        self._set_volume(new_volume)

    @property
    def is_muted(self):
        return self._is_muted

    def _set_mute(self, true_or_false):
        self._is_muted = true_or_false
        self.event_muted.fire(self._is_muted, self)

    def toggle_mute(self):
        if self.is_muted:
            self._set_mute(False)
            self._set_volume(self._volume)
        else:
            self._set_volume(0)
            self._set_mute(True)

    def _set_volume(self, new_volume):
        if new_volume > 1.0:
            new_volume = 1.0
        if new_volume < 0:
            new_volume = 0.0
        self._volume = new_volume
        self.event_volume_changed.fire(self._volume, self)
        if not self.is_muted:
            pygame.mixer.music.set_volume(new_volume)

    def fill_music_carousel(self, music_data):
        self.music_carousel = list(music_data)

    def clear_music_carousel(self):
        """Clears the carousel but will play the currently playing song to end."""
        del self.music_carousel[:]

    def stop_music_carousel(self):
        """Stops the music (without fadeout)."""
        pygame.mixer.music.set_endevent()  # prevent call to _update on song end
        pygame.mixer.music.fadeout(1000)  # fixme: is this a blocking call (according to docs yes)? -> use self.fadeout()
        pygame.mixer.music.stop()

    def start_music_carousel(self, start=0.0):
        logger.info("start music carousel!")
        self.position = start
        if pygame.mixer.get_init() is None:
            logger.error("Can't start music carousel: mixer not yet initialized!")
            return
        pygame.mixer.music.fadeout(1000)  # fixme: is this a blocking call (according to docs yes)? -> use self.fadeout()
        self._update_music(start)

    def pause_music(self):
        pygame.mixer.music.pause()
        self.music_paused = True

    def unpause_music(self):
        pygame.mixer.music.unpause()
        self.music_paused = False

    def _update_music(self, start=0.0):
        logger.info("updating music")
        if self.music_paused:
            logger.info("music paused!")
            return
        if not self.music_carousel:
            logger.warning("no music in carousel!")
            return
        if pygame.mixer.music.get_busy():
            logger.warning("music still busy!")
            return

        logger.info("play next music in queue")
        next_song = self.music_carousel.pop(0)
        #if len(self.music_carousel) == 0: # updated to only repeat on final track in carousel
        #    self.music_carousel.append(next_song)

        pygame.mixer.music.load(next_song.filename)
        new_volume = next_song.volume * self._volume
        self._set_volume(new_volume)
        pygame.mixer.music.set_endevent(self.music_ended_pygame_event_id)
        if len(self.music_carousel) == 0:
            pygame.mixer.music.play(-1, start=start) # loop infinitely if final track in carousel
            # this loops much more smoothly than having to start it again after detecting it's no longer playing
        else:
            pygame.mixer.music.play(start=start)

    def fadeout(self, timeout):
        pygame.mixer.music.set_endevent()  # prevent call to _update on song end
        pygame.mixer.music.fadeout(timeout)  # fixme: hmm, this is a blocking call according to docs? Not blocking!
        while pygame.mixer.music.get_busy():
            logger.warning("wait 100ms for music fadeout")
            pygame.time.wait(100)

    def on_music_ended(self):
        self._update_music(self.position)


class SoundPlayer(object):
    def __init__(self, volume):
        self.sound_data = {}
        self._volume = volume
        self._num_channels = 0
        self._num_reserved_channels = 0

    def setup_channels(self, num_channels, num_reserved_channels):
        if num_reserved_channels > num_channels:
            raise ValueError("cannot reserve more channels as channel count")

        pygame.mixer.set_num_channels(num_channels)
        pygame.mixer.set_reserved(num_reserved_channels)
        self._num_channels = num_channels
        self._num_reserved_channels = num_reserved_channels

    def change_volume(self, change):
        new_volume = self._volume + change
        self._set_volume(new_volume)

    def _set_volume(self, new_volume):
        if new_volume > 1.0:
            new_volume = 1.0
        if new_volume < 0.0:
            new_volume = 0.0
        self._volume = new_volume
        for key, data in self.sound_data.items():
            # use sound volume instead of channel volume because it is persistent to the sound
            if data.sound is None:
                continue
            data.sound.set_volume(data.volume * self._volume)

    def fadeout(self, sfx_id, ms):
        sound_data = self.sound_data.get(sfx_id, None)
        if sound_data is not None:
            if sound_data.channel is not None:
                sound_data.channel.fadeout(ms)
            else:
                sound_data.sound.fadeout(ms)

    def load(self, data_map): # fixme: this should go into the resource loader?
        if pygame.mixer.get_init() is None:
            return

        reserved_channel_ids = set()
        for sound_id, data in data_map.items():
            if isinstance(data, SoundData):
                data.sound = pygame.mixer.Sound(data.filename)
                data.sound.set_volume(data.volume)
                if data.reserved_channel_id is not None:
                    reserved_channel_ids.add(data.reserved_channel_id)
                    if len(reserved_channel_ids) > self._num_reserved_channels:
                        raise OverflowError("loading more reserved channels as configured reserved channels!")
                    data.channel = pygame.mixer.Channel(data.reserved_channel_id)
                self.sound_data[sound_id] = data
            else:
                raise TypeError("cannot load other than SoundData instances, but it is " + type(data))

    def handle_message(self, event_type, msg_type, extra=None):
        """play a sound or song

        If msg_type is MusicData then the song is queued (note that the queueing is done in Python; unlike pygame, the
        queued song will start playing if the current song is faded out).

        If msg_type is SoundData, the object (at instantiation) determines whether the sound plays on a reserved
        channel. If the extra argument is 'queue', then the sound is queued (this is ideal for looping).

        # :param sender: None; not used
        # :param receiver_id: None; not used
        :param msg_type: MusicData or SoundData
        :param extra: None, 'mutex', or 'queue' ('queue' is ignored unless SoundData uses a reserved channel)
        :return:
        """
        if pygame.mixer.get_init() is None:
            return
        # todo correct naming of msg_type
        # todo: introduce methods for the different ways a sfx can be played
        data = self.sound_data.get(msg_type)
        if isinstance(data, SoundData):
            if data.channel is not None:
                if extra == 'queue':
                    data.sound.set_volume(data.volume * self._volume)
                    data.channel.queue(data.sound)
                elif extra == 'mutex':
                    if not data.channel.get_busy():
                        data.sound.set_volume(data.volume * self._volume)
                        data.channel.play(data.sound)
                else:
                    data.sound.set_volume(data.volume * self._volume)
                    data.channel.play(data.sound)
            elif extra == 'queue':
                channel = pygame.mixer.find_channel()  # <<< use True to preempt if all channels are busy
                if channel:
                    data.sound.set_volume(data.volume * self._volume)
                    channel.play(data.sound)
            else:
                data.sound.set_volume(data.volume * self._volume)
                data.sound.play()
        # elif isinstance(data, MusicData):
        #     pygame.mixer.music.fadeout(1000)
        #     self.queued_music = data
        else:
            logger.debug('sfx msg_type not found: {}', msg_type)

    # def update(self, dt, sim_t):
    #     self._update_music()
    #
    #     # This is called from the scheduler
    #     return settings.sfx_step


logger.debug("imported")
