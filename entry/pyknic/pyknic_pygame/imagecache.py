# -*- coding: utf-8 -*-
"""imagecache.py - Economic image cache

ImageCache is a cache that organizes entries under two keys: one for the
bucket and one for the image's transformation details. It is suitable for
storing images that are computationally transformed frequently during the
runtime.

Because the images are cached, they conserve memory when they are shared
between visual objects, and they conserve CPU because the transformations
do not need to be computed again.

ImageCache provides two cache management schemes:

#. Memory management. Memory management performs a simple removal of entries
   until the usage is beneath the chosen max. Entries are removed from the
   oldest accessed to the newest. This feature is disabled by default.

#. Age management. Age management performs a simple removal of entries that
   that have not been accessed in longer than the chosen age. Entries are
   removed from the oldest accessed to the newest. By default, this feature
   is enabled, and max_memory is 2 GB.

`update()` handles the detection and use of the enabled scheme.

One intelligent strategy is to set max_memory to some high cap like 2 GB to
prevent out-of-memory crashes, and set age tuning to auto-tune around 90 or
95 percent. This will set a hard limit to purge the oldest images if the 2 GB
cap is reached, otherwise use auto-tuned aging.

One does not have to use update(), particularly as stats maagement adds a
little overhead. The bare minimum methods are add(), remove(), get(), and
optionally one of the two cache management methods, purge_old() and
purge_memory().

Tuning
------

The following items affect the aggressiveness of the cache's algorithm::


    at_most

    update() passes this setting to purge_old() and purge_memory(). If it is
    set too high, the game may experience freezes; if it is set too small the
    cache might grow faster than it is purged. A fair starting value is 50
    or 1/2 of the number of images on screen at once.

    max_memory

    This is the maximum memory that the cache is permitted to use. Memory
    computation is approximate, based on image size and depth only.

    default_expiration

    This is the fallback lifespan of entries which have -1 as their individual
    lifespan. It represents the lifespan in seconds. This can be changed on the
    fly. It is used by update() and purge_old(). It is also adjusted in
    update() in observance of the tune_* attributes when auto_tune_aging is
    True and cache management by age is enabled. By default it is set to None:
    no expiration.

    auto_tune_aging
    tune_min_percent
    tune_max_percent
    tune_step

    Auto tuning of aging is done by update(). auto_tune_aging is a boolean:
    True is on, False if off. This feature attempts to keep the hit_ratio
    between a min and max. tune_min_percent is the hit_ratio below which the
    default_expiration will be adjusted gradually upward. tune_max_percent is
    the hit_ratio above which the default_expiration will be adjusted gradually
    downward. tune_step is the adjustment per tick. If a lot of entries are
    removed over several ticks, this feature will cause some swing until it
    levels out. default_expiration must be a positive integer in order for this
    feature to have an effect. auto_tune_aging is True be default.

Geek Notes
----------

Two keys means the hash buckets are smaller, which theoretically results in
shorter hash buckets (typically implemented as lists) where the hashing
function clashes. Shorter lists means less sequential searching to find the
elements whose keys clash. This is a geek tidbit, knowledge of which is nice
but not necessary to use or enefit most from the cache. One big flat hash
would result in longer lists where the hashing function computes the same
hash for different keys, and this can significantly harm performance as the
buckets get deeper.

Example
-------

.. code:: python

    # This example assumes a simple sprite that uses images with varying angles of
    # rotation.

    megabyte = 2 ** 20
    default_expiration = 5 * 60  # 5 minutes
    max_memory = 250 * megabyte  # 250 MB
    image_cache = imagecache.ImageCache(default_expiration, max_memory)

    # ImageCache entries take two keys: one for the bucket, and one for the
    # image's transformation details. The base image is the image from which all
    # transforms are computed: this is a best practice. A vanilla_key is provided
    # by this module for convenience: but any immutable type can be used, such as
    # a tuple, string or an integer.

    # Add base images which never expire; image name and alpha are used as the key;
    # base images should not be allowed to expire.
    # name: e.g. arbitrary name, Python class name, or filename
    # rot: using 0 as the default for base images
    # image: pygame surface
    # expiration: None

    def add_sprite_image(name, rot, image):
        image_cache.add(name, rot, image, None)
    add_sprite_image(sprite.__class__.__name__, 0, pygame.image.load('joe.png'))

    # Later, the program computes rotation angles to spin joe. Rotated images are
    # cached because this transformation is computationally expensive.
    #
    # This function checks for a cached image. If the image is not cached, it
    # generates a new image and adds it to the cache.

    def get_sprite_image(name, rot):
        image = image_cache.get(name, rot)
        if image is None:
            image = image_cache.get(name, 0)  # base image uses rot=0
            image = pygame.transform.rotozoon(image, angle, 1.0)
            image_cache.add(name, rot)
        return image

    # Here is a fancy sprite class that interacts with the cache when it is used
    # as: screen.blit(sprite.image, sprite.rect):

    class MySprite(pygame.sprite.Sprite):
        def __init__(self, rot):
            pygame.sprite.Sprite.__init__(self)
            self._image = get_sprite_cache(self.__class__.__name__, 0)
            self._rect = self._image.get_rect()
            self.rot = rot
        def getrot(self):
            return self._rot
        def setrot(self, val):
            if val != self._rot:
                self._rot = val
                self._image = get_sprite_cache(self.__class__.__name__, int(self._rot))
                center = self.rect.center
                self.rect = self._image.get_rect(center=center)
        rot = property(getrot, setrot)

"""


import time
import collections
import logging

from pygame import SRCALPHA

logger = logging.getLogger(__name__)
logger.debug("importing...")


__version__ = '0.2.0'
__vernum__ = __version__.split('.')


# This is a key that can be used as a default for the initially created image.
# It represents an image without any rotation, flipy, flipx, zoom, and alpha.
vanilla_key = (0, False, False, 1.0, 255)


class _Entry(object):
    __slots__ = ('image', 'created', 'accessed', 'count', 'lifespan', 'keys', 'size')

    def __init__(self, image, expire, *keys):
        now = time.time()
        self.image = image
        self.created = now
        self.accessed = now
        self.count = 0
        self.lifespan = expire
        self.keys = keys
        self.size = 0

    def __cmp__(self, other):
        if self.accessed > other.accessed:
            return 1
        elif self.accessed < other.accessed:
            return -1
        else:
            return 0


class ImageCache(object):

    def __init__(self, default_expiration, max_memory=2 * 2 ** 30):
        """make a image cache

        An image  cache stores images by an identity for the image, and an
        arbitrary key (transforms) which represent how the image was
        transformed from the base image. Use this for images that are processed
        or transformed on the fly.

        Cache entries are reaped from oldest to newest access time. Access time is set by
        add(), and updated when by get().

        :param default_expiration: max lifes in seconds; default is None (no default expiration)
        :param max_memory: max size in bytes; default is None (no max)
        :return: ImageCache
        """
        self._cache = {}
        self._size = 0
        self._count = 0
        self._eligible = collections.OrderedDict()

        # tunables
        self.at_most = 50
        self.default_expiration = default_expiration
        self.max_memory = max_memory
        self.auto_tune_aging = True
        self.tune_min_percent = 89.0
        self.tune_max_percent = 91.0
        self.tune_step = 0.01

        # counters
        self.old_purged = 0
        self.mem_purged = 0

        self._aging_enabled = False
        self._memory_enabled = True
        self._hits = 0
        self._misses = 0
        self._hits_history = []
        self._misses_history = []
        self._history = 60
        self._hitting_max_memory = False

    def add(self, ident, transforms, image, expire=None):
        """add an entry

        :param ident: bucket key
        :param transforms: transforms key
        :param image: pygame surface
        :param expire: float; lifespan in seconds; None=no expiration; -1=use default_expiration
        :return:
        """
        self.remove(ident, transforms)
        cache = self._cache
        if ident in cache:
            images = cache[ident]
        else:
            images = {}
            cache[ident] = images
        if transforms not in images:
            entry = _Entry(image, expire, ident, transforms)
            images[transforms] = entry
            if expire is not None:
                self._eligible[entry] = 1
            w, h = image.get_size()
            d = image.get_bitsize()
            srcalpha = 1.5 if image.get_flags() & SRCALPHA else 1.0
            entry.size = w * h * d * srcalpha / 8.0
            self._size += entry.size
            self._count += 1

    def get(self, ident, transforms):
        """get an entry

        :param ident: bucket key
        :param transforms: transforms key
        :return: image, or None if not found
        """
        try:
            entry = self._cache[ident][transforms]
            entry.accessed = time.time()
            entry.count += 1
            try:
                del self._eligible[entry]
                self._eligible[entry] = 1
            except KeyError:
                pass
            self._hits += 1
            return entry.image
        except KeyError:
            self._misses += 1
            return None

    def remove(self, ident, transforms):
        """remove an entry

        :param ident: bucket key
        :param transforms: transforms key
        :return: None
        """
        # retrieve the image and delete the cache entry
        cache = self._cache
        try:
            entry = cache[ident][transforms]
        except KeyError:
            return
        del cache[ident][transforms]
        if not len(cache[ident]):
            del cache[ident]
        # adjust size and count
        self._size -= entry.size
        self._count -= 1
        # remove eligible
        try:
            del self._eligible[entry]
        except KeyError:
            pass

    def update(self):
        """process the purge if enabled, and update counters

        :return: None
        """
        self._hits_history.append(self._hits)
        if len(self._hits_history) > self._history:
            self._hits_history.pop(0)
        self._misses_history.append(self._misses)
        if len(self._misses_history) > self._history:
            self._misses_history.pop(0)
        self._hits = 0
        self._misses = 0

        if self.memory_cap_is_enabled():
            self.purge_memory(at_most=self.at_most)

        if self.age_cap_is_enabled():
            self.purge_old(at_most=self.at_most)

            if not self._hitting_max_memory and \
                    self.auto_tune_aging and \
                    self.default_expiration is not None:
                hit_ratio = self.hit_ratio()
                if hit_ratio < self.tune_min_percent:
                    self.default_expiration += self.tune_step
                elif hit_ratio > self.tune_max_percent:
                    self.default_expiration -= self.tune_step

    def clear(self):
        """remove all entries in the cache

        :return: None
        """
        # self._cache.clear()
        for i in tuple(self._cache):
            for t in tuple(self._cache[i]):
                self.remove(i, t)

    def purge_memory(self, at_most=0):
        """bring the size of the cache down to self.max_memory

        If a memory cap is not desired, there is no need to call this method.

        The at_most argument throttles the call to remove at most the number of
        entries during this tick. If at_most is 0, the program may freeze while
        a large number of entries is removed. In order to reduce the size of
        the cache, at_most should be larger than the number of images being
        stored per tick.

        :param at_most: positive integer; max number of entries to remove at once
        :return:
        """
        max_memory = self.max_memory
        if max_memory is None or max_memory <= 0.0 or self._size < max_memory:
            return

        self._hitting_max_memory = True

        num_removed = 0
        for entry in self._eligible:
            self.remove(*entry.keys)
            num_removed += 1
            if self._size <= max_memory:
                break
            if num_removed == at_most:
                break
        self.mem_purged = num_removed

    def purge_old(self, at_most=0):
        """purge old images

        Call once every so often to purge images that are past expiration.

        If expiration is not desired, there is no need to call this method.

        The at_most argument throttles the call to remove at most the number of
        entries during this tick. If at_most is 0, the program may freeze while
        a large number of entries is removed. In order to reduce the size of
        the cache, at_most should be larger than the number of images being
        stored per tick.

        :param at_most: positive integer; max number of entries to remove at once
        :return: None
        """
        now = time.time()
        default_expiration = self.default_expiration
        num_removed = 0
        for entry in self._eligible:
            expire = entry.lifespan
            if expire < 0 and default_expiration is not None:
                expire = default_expiration
            if now - entry.accessed > expire:
                self.remove(*entry.keys)
                num_removed += 1
            else:
                break
            if num_removed == at_most:
                break
        self.old_purged = num_removed

    def size(self):
        """approximate size in bytes

        Size measures only the surface dimansions width * height * depth. It
        does not account for Python bytecode or pyd memory.
        """
        return self._size

    def count(self):
        """number of entries in the cache

        :return: int; number of entries
        """
        return self._count

    def hit_ratio(self):
        """return the hit ration as percentage of hits

        This reports accurately only if using update() to feed hits and misses
        each tick.

        :return: int; hit ratio
        """
        avg_hits = sum(self._hits_history)
        avg_misses = sum(self._misses_history)
        hit_ratio = 0
        if avg_hits:
            avg_hits /= float(len(self._hits_history))
        if avg_misses:
            avg_misses /= float(len(self._misses_history))
        if avg_hits or avg_misses:
            hit_ratio = int((avg_hits / (avg_hits + avg_misses) * 100.0))
        return hit_ratio

    def reset_counters(self):
        """reset old and mem counters

        No need to call this if using a purge method directly (i.e. not using
        update()) and you don't care about reading the counters.

        :return: None
        """
        self.mem_purged = 0
        self.old_purged = 0

    def enable_age_cap(self, boolean):
        """enable age cap logic in update()

        No need to call this if using a purge method directly (i.e. not using
        update()).

        :param boolean: True or False
        :return: None
        """
        if boolean != self._aging_enabled:
            self._aging_enabled = boolean
            # self._memory_enabled = False
            self.reset_counters()

    def enable_memory_cap(self, boolean):
        """enable memory cap logic in update()

        No need to call this if using a purge method directly (i.e. not using
        update()).

        :param boolean: True or False
        :return: None
        """
        if boolean != self._memory_enabled:
            self._memory_enabled = boolean
            # self._aging_enabled = False
            self.reset_counters()
            self._hitting_max_memory = False

    def age_cap_is_enabled(self):
        """indicate if age cap logic in update() is enabled

        :return: True or False
        """
        return self._aging_enabled

    def memory_cap_is_enabled(self):
        """indicate if memory cap logic in update() is enabled

        :return: True or False
        """
        return self._memory_enabled

logger.debug("imported")
