# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'context_timestep.py' is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

import pygame

from pyknic import timing
from pyknic.context import Context

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["TimeSteppedContext"]  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class TimeSteppedContext(Context):

    def __init__(self, max_dt, step_dt, draw_fps):
        """
        Context that will update the logic using fixed time steps and independent draw calls (draw fps is capped).

        :param max_dt: max. delta time of a update loop.
        :param step_dt: the normal delta time step size.
        :param draw_fps: the draw fps.
        """
        Context.__init__(self)
        self._clock = pygame.time.Clock()

        self._lockStepper = timing.LockStepper(max_dt=max_dt, timestep=step_dt, max_steps=100000)
        self._lockStepper.event_integrate += self.update_step

        self._cap = timing.FrameCap(draw_fps)
        self._cap.event_update += self._draw

        self._game_time = timing.GameTime()
        self._game_time.max_real_delta_t = max_dt
        self._game_time.event_update += self._lockStepper.update
        self._game_time.event_update += self._cap.update

    def update(self, *args):
        """
        The update method called from the context stack, don't override it.
        User the update_step and draw methods.
        """
        dt = self._clock.tick()
        dt /= 1000.0  # convert to seconds
        self._game_time.update(dt)

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        raise NotImplementedError("draw to screen here")

    def update_step(self, delta, sim_time, *args):
        raise NotImplementedError("update logic here")

    def _draw(self, dt, sim_t, *args):
        self.draw(dt, sim_t, True, self._lockStepper.alpha)


logger.debug("imported")
