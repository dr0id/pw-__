# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The context stack module.


It holds the stack of context and its basic manipulation methods.
"""
from __future__ import print_function, division

import logging

from pyknic.context import transitions

logger = logging.getLogger(__name__)
logger.debug("importing...")

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = ["ContextStack",
           "pop",
           "push",
           "exchange",
           "length",
           "top",
           "print_stack",
           "set_deferred_mode",
           "is_deferred_mode_active",
           "update",
           ]  # list of public visible parts of this module


class ContextStack(object):
    """
    The ContextStack class. It holds the stack of contexts and has convenient methods to work with the stack.
    """

    def __init__(self):
        self._stack = []
        self._is_deferred_mode_active = False
        self._deferred_commands = []

    def push(self, cont, effect=None, update_old=False, update_new=False):
        """
        Pushes a new context on the stack. The enter method of the new context
        will be called when pushing it. If the stack is not empty, suspend will
        be called on the topmost context on the stack before pushing the new
        context on top of it.

        :type effect: see :py:class:`pyknic.context.transitions.TransitionEffect`
        :param effect: an instance of TransitionEffect or None. Defaults to None.
        :param update_old: Default is False. Only relevant if an effect is given.
            If True, the old context will be updated when the effect is updated.
        :param update_new: Default is False. Only relevant if an effect is given.
            If True, the new context will be updated when the effect is updated.
        """
        if self._is_deferred_mode_active:
            self._append_deferred_command((self._push, (cont, effect, update_old, update_new)))
        else:
            self._push(cont, effect, update_old, update_new)

    def _append_deferred_command(self, cmd):
        """
        Appends a command to the deferred command list.
        :param cmd: A command as a tuple of (cmd, args) where cmd either _push or _pop, args a tuple or list of args.
        """
        logger.debug("CONTEXT deferred: appending command: " + str(cmd))
        if cmd[0] != self._push and cmd[0] != self._pop and cmd[0] != self._exchange:
            raise ValueError("command should be either _push or _pop or _exchange but is " + str(cmd[0].__name__))

        self._deferred_commands.append(cmd)

    def _push(self, cont, effect, update_old, update_new):
        """
        Pushes a new context on the stack. The enter method of the new context
        will be called when pushing it. If the stack is not empty, suspend will
        be called on the topmost context on the stack before pushing the new
        context on top of it.
        """
        if effect is not None:
            cont = transitions.PushTransition(cont, effect, update_old, update_new)
        cont.process_stack_push(self._stack)
        if __debug__:
            logger.debug("CONTEXT: pushed " + str(cont))
            print_stack()

    def pop(self, count=1, do_resume=True, effect=None, update_old=False, update_new=False):
        """
        Pops a context from the stack. The exit method of the removed context will
        be called. If a there is another context on the stack, the resume method of
        it will be called.

        :Parameters:
            count : int
                defaults to 1, number of contextes to pop
            do_resume : bool
                (default: True) This has only an effect if poping more than one context at once (count>1).
                If set to False then only the last context is resumed.
            effect : Effect
                (default: None) This is the effect to use in the PopTransition. If an effect is given, the the
                pop is limited to count=1 context at once and it will resume the next context.
            update_old : bool
                (default: False) If set, the old context is updated. Otherwise not.
            update_new : bool
                (default: False) If set, the new context is updated. Otherwise not.
        """
        if self._is_deferred_mode_active:
            self._append_deferred_command((self._pop, (count, do_resume, effect, update_old, update_new)))
        else:
            self._pop(count, do_resume, effect, update_old, update_new)

    def _pop(self, count, do_resume, effect, update_old, update_new):
        """
        Pops a context from the stack. The exit method of the removed context will
        be called. If there is another context on the stack, the resume method of
        it will be called.

        :Parameters:
            count : int
                defaults to 1, number of contexts to pop
            do_resume : bool
                (default: True) This has only an effect if poping more than one context at once (count>1).
                If set to False then only the last context is resumed.
            effect : Effect
                (default: None) This is the effect to use in the PopTransition. If an effect is given, the the
                pop is limited to count=1 context at once and it will resume the next context.
            update_old : bool
                (default: False) If set, the old context is updated. Otherwise not.
            update_new : bool
                (default: False) If set, the new context is updated. Otherwise not.
        """
        if effect is not None:
            cont = transitions.PopTransition(effect, update_old, update_new)
            self._push(cont, None, False, False)
            return

        while count:
            count -= 1
            cont = None
            if self._stack:
                cont = self._stack.pop(-1)
                cont.process_stack_pop(self._stack, (do_resume or count == 0))
                if __debug__:
                    logger.debug("CONTEXT: poped " + str(cont))
                    print_stack()

    def exchange(self, cont, effect=None, update_old=False, update_new=False):
        """
        Exchanges the top context with the given one. The top one will exit and the new one will enter.
        No context will be resumed.
        :param effect: The effect to use to transition to new context. Default: None.
        :param update_old: If set, the old context is updated. Default: False.
        :param update_new: If set, the new context is updated. Default: False.
        :param cont: the new context that replaces the top one.
        """
        if self._is_deferred_mode_active:
            self._append_deferred_command((self._exchange, (cont, effect, update_old, update_new)))
        else:
            self._exchange(cont, effect, update_old, update_new)

    def _exchange(self, cont, effect, update_old, update_new):
        if effect is None:
            if self._stack:
                self._pop(1, False, None, update_old, update_new)
                self._push(cont, None, update_old, update_new)
        else:
            cont = transitions.Transition(cont, effect, update_old, update_new)
            self._push(cont, None, False, False)

    def top(self, idx=0):
        """
        The active context.

        :Parameters:
            idx : int
                default: 0 (topmost), the item at index position idx of the stack.

        :returns:
            default: the topmost context of the stack or None
            otherwise the the item at index position idx or None if idx > length()
        """
        idx += 1
        if idx > len(self._stack):
            return None
        return self._stack[-1 * idx]

    def length(self):
        """
        The number of items in the stack.

        :returns:
            number of items, integer >= 0
        """
        return len(self._stack)

    def set_deferred_mode(self, is_deferred):
        """
        Set or un-set the deferred mode. In deferred mode the pop() and push() methods are executed when the
        update() method is called. It is essential to call update(), otherwise nothing will happen.

        When disabling the deferred mode all operations still awaiting the update() call will be executed
        immediately. Afterwards calls to update() will do nothing.

        :param is_deferred: Boolean to indicate if deferred mode should be activated or not.
        """
        if not is_deferred:
            logger.debug("CONTEXT: executing any deferred commands before disabling deferred mode!")
            self.update()

        logger.debug("CONTEXT: setting deferred mode to: " + str(is_deferred))
        self._is_deferred_mode_active = is_deferred

    def is_deferred_mode_active(self):
        """
        Check if deferred mode is active or not.

        :return: Boolean indicating if deferred mode is active.
        """
        return self._is_deferred_mode_active

    def update(self):
        """
        Update method to execute all deferred pop() and push() operations. This method has to be called
        in deferred mode. When the deferred mode is inactive this method does nothing.
        """
        if self._is_deferred_mode_active:
            for cmd, args in self._deferred_commands:
                logger.debug("CONTEXT deferred mode: executing command: {0}({1})".format(cmd, args))
                cmd(*args)
            # logger.debug("CONTEXT deferred mode: clearing deferred commands")
            self._deferred_commands = []

    def print_stack(self):
        """
        Prints the current stack to the console. Should only be used for
        debugging purposes.
        """
        if __debug__:
            logger.debug("CONTEXT STACK:")
            for idx in range(0, self.length()):
                logger.debug("    %s %s" % (idx, self.top(idx)))
            logger.debug("")


_CONTEXT_STACK = ContextStack()
pop = _CONTEXT_STACK.pop
push = _CONTEXT_STACK.push
exchange = _CONTEXT_STACK.exchange
length = _CONTEXT_STACK.length
top = _CONTEXT_STACK.top
print_stack = _CONTEXT_STACK.print_stack
set_deferred_mode = _CONTEXT_STACK.set_deferred_mode
is_deferred_mode_active = _CONTEXT_STACK.is_deferred_mode_active
update = _CONTEXT_STACK.update

logger.debug("imported")
