# -*- coding: utf-8 -*-
"""
Module about timing classes and helpers.

This module provides some helpful classes that relate to timing.

First there ist the GameTime class. Its purpose is to track the simulation time of the game. It also offers
to make the game time pass slower or faster than the real time.

If physics (even simple like position += vel * dt) are involved, then normally a fixed time step is used. For this
purpose there is the LockStepper class. Define the fixed timestep and it handles it.

To cap the fps of the drawing to a max fps and not just burn extra cpu cycles there is the FrameCap class. It will
update with max fps.

Most important for games is timing. The Scheduler class is designed for it. To make the handling of the callbacks even
simpler, there is also the Timer class (it depends on a Scheduler instance).

The update methods are designed to work together. The idea is to connect them as follow: a GameTime instance taking the
real dt. It will fire its event and drive the LockStepper for the simulation and the FrameCap for drawing (yes, they
have different update frequencies). The LockStepper instance might drive a Scheduler which is used by many timers
within the game logic. And the FrameCap may also drive another Scheduler instance for animations as an example.

Lets try that in code::

        # simulation
        self.game_time = GameTime()
        self.lock_stepper = LockStepper()
        self.scheduler = Scheduler()

        # graphics
        self.frame_cap = FrameCap()
        self.animation_scheduler = Scheduler()

        # connect
        #
        #                   +-> lock_stepper --> scheduler ----------------> timer
        #                   |
        #   game_time ------+
        #                   |
        #                   +-> frame_cap -----> animation_scheduler -----> animation(s)
        #
        #
        self.game_time.event_update += self.lock_stepper.update
        self.game_time.event_update += self.frame_cap.update
        self.lock_stepper.event_integrate += self.scheduler.update
        self.frame_cap.event_update += self.animation_scheduler.update


As reference see 'ChainedTimingClassesIntegrationTests' and 'ChainGameTimeSchedulerLockStepperIntegrationTests' in
the module test_timing.py!

"""
from __future__ import print_function, division

import atexit
import heapq
import logging
import sys
import warnings
from heapq import heappop, heappush

from pyknic import events

logger = logging.getLogger(__name__)
logger.debug("importing...")

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id [at] gmail [dot] com"
__copyright__ = "DR0ID @ 2016"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"


class GameTime(object):
    """
    GameTime allows you to manipulate the virtual time in the game. You can
    slow down or speed up the time in your game. Also you can pause any time
    dependent code by setting the dilatation factor to 0. Trying to set a
    negative factor value will raise a NegativeFactorException.
    Each call to update will fire the (public) 'event_update' with the
    arguments: gdt
    (GameTime instance, game delta time, game time, real delta time, real time)

    Through the attribute time the current game time can be read (both in [ms]).

        :Variables:
            event_update : Event
                update event that is fired every time update is called,
                it passes gdt, the delta time passed in the game
    """

    def __init__(self, *args, **kwargs):
        """
        Init the GameTime with tick_speed=1.0.
        """
        # noinspection PyArgumentList
        super(GameTime, self).__init__(*args, **kwargs)
        self.max_real_delta_t = sys.maxsize
        self._factor = 1.0
        self._sim_time = 0
        self._real_time = 0
        self.event_update = events.Signal("GameTime" + str(id(self)))

    def _set_factor(self, factor):
        """
        For use in the factor property only. You get a warning when setting a
        negative value, but it is permitted.

        :Parameters:
            factor : float
              time dilatation factor, 0.0 pauses, negative values run the time
              backwards (scheduling does not work when running backwards).
        """
        if __debug__:
            if 0 > factor:
                warnings.warn('Using negative time factor!!')
        self._factor = factor

    def _get_factor(self):
        """
        Returns the dilation factor.
        """
        return self._factor

    tick_speed = property(_get_factor, _set_factor, doc="""
        Set the dilatation factor, 0.0 pauses, negative raise a warning and let
        the time run backwards (scheduling does not work with negative values)
        """)

    def _get_time(self):
        """
        Returns the game time.
        """
        return self._sim_time

    time = property(_get_time, doc=""" game time since start, read-only""")

    def _get_real_time(self):
        """
        Returns the real time.
        """
        return self._real_time

    real_time = property(_get_real_time, doc=""" real time since start, read-only""")

    # noinspection PyUnusedLocal
    def update(self, real_delta_t, *args):
        """
        Should be called each frame to update the game time.
        real_delta_t is the time passes in the last 'frame'.
        It fires the event_update with the arguments: gdt

            gdt : float
                game delta time

        :Parameters:
            real_delta_t : int
                real delta time (same as input)
            args: ???
                additional, ignored arguments

        The game time is only advanced if update is called.
        """
        # # prevent spiral of death, cap at max_dt
        real_delta_t = real_delta_t if real_delta_t < self.max_real_delta_t else self.max_real_delta_t

        self._real_time += real_delta_t
        gdt = self._factor * real_delta_t
        self._sim_time += gdt
        self.event_update.fire(gdt, self._sim_time, self)


class LockStepper(object):
    """
    Based on: `<http://gafferongames.com/game-physics/fix-your-timestep/>`_

    This will advance its 'sim_time' in a 'lock step' mode.

    To register a integrate method of your simulation::

        stepper = LockStepper()
        def integrate(delta_seconds, sim_time): pass
        ...
        stepper.event_integrate += integrate


    :param max_steps: max number of steps taken in this frame, needed for
        the under sampling case, this will slow down the simulation
    :type max_steps: int
    :param max_dt: The max dt in seconds to cap, if a dt is > max_dt then max_dt is used
        instead (slowing down the virtual time progression in the simulation).
    :type max_dt: int
    :param logger_to_use: The logger instance to use. If set to None it will use the module logger instance.
    :type logger_to_use: logging.Logger
    :param timestep: the step size of the lock step, defaults to LockStepper.TIMESTEP_SECONDS which is 0.01 seconds.
    :type timestep: float

    :attr alpha: alpha in range [0, 1.0], is the remainder and means how
            far between the current step and the next step the simulation is.
            This should be used to interpolate the position of the current and
            previous state so the graphics are smooth.

    """

    TIMESTEP_SECONDS = 0.01  # 100 times per second

    def __init__(self, max_steps=8, max_dt=sys.maxsize, timestep=None, logger_to_use=None):
        """
        LockStepper constructor.
        """
        self._accumulator = 0.0
        self._sim_time = 0
        self.event_integrate = events.Signal("LockStepper" + str(id(self)))
        self.max_steps = max_steps
        self.max_dt = max_dt
        self.logger = logger if logger_to_use is None else logger_to_use
        self.timestep = LockStepper.TIMESTEP_SECONDS if timestep is None else timestep
        self.alpha = 0.0

    # noinspection PyUnusedLocal
    def update(self, dt_seconds, sim_time, *args):
        """
        Updates the simulation using a in discrete time deltas (lock step).

        This triggers the 'event_integrate' signal

        :param dt_seconds: time in seconds passed since last frame
            make sure to clamp this value to a maximum value (because
            if a large value is passed in you don't want to wait until
            all steps have been calculated)
        :type dt_seconds: float
        :param sim_time: The simulation time. It is unused because the time steps produce differ.

        """
        # prevent spiral of death, cap at max_dt
        dt_seconds = self.max_dt if dt_seconds > self.max_dt else dt_seconds

        self._accumulator += dt_seconds
        num_steps = 0
        while self._accumulator >= self.timestep and num_steps < self.max_steps:
            self._sim_time += self.timestep
            self._accumulator -= self.timestep
            num_steps += 1
            self.event_integrate.fire(self.timestep, self._sim_time, self)

        if __debug__:
            # about it don't know how to achieve it in a good way
            if num_steps >= self.max_steps:
                self.logger.info("LockStepper::update() took max_steps" + str(self.max_steps))
            if self._accumulator > 200 * self.timestep:
                self.logger.warning("LockStepper::update() is way to slow to keep up!")

        _alpha = self._accumulator / self.timestep
        self.alpha = _alpha - int(_alpha)  # because of max_steps _alpha can be > 1.0


class Scheduler(object):
    """
    This is a scheduler that works on a frame basis. Call update() on each
    frame and all scheduled methods (== callbacks) will be called.

    The return value of the callback decides if the callback is re-scheduled.
    If a callback returns 0 the callback will be removed and no further
    re-scheduling is done (the callback is called once).

    To re-schedule the callback it should return a integer number describing
    the next frame it should be called back.

    ::

        def callback(*args, **kwargs):
            # code
            return 10 # this callback is called in 10 frames again

    """

    STOP_REPEAT = 0.0
    _ADD = 1
    _REMOVE = 2

    def __init__(self):
        self._heap = []
        self._next_time = sys.maxsize
        self.current_time = 0
        self._next_id = 999
        self._sync = []

    def schedule(self, func, interval, *args, **kwargs):
        """
        Schedule a function or method to be called back a time later.

        :param func: function or method to call back in the future
        :type func: method
        :param interval: time that should pass until next call of func,
            has to be >= 0 (a value of 0 means it is called next time
            the update method is called)
        :type interval: float
        :param args: arguments of the scheduled function
        :type args: list
        :param kwargs: keyword arguments of the scheduled function
        :type kwargs: dict

        :returns id: the schedule id, use it to stop the schedule
        """
        return self.schedule_with_offset(func, interval, 0, *args, **kwargs)

    def schedule_with_offset(self, func, interval, offset, *args, **kwargs):
        """
        Schedule a function or method to be called back a time later.

        :param func: function or method to call back in the future
        :type func: method
        :param interval: time that should pass until next call of func,
            has to be >= 0 (a value of 0 means it is called next time
            the update method is called)
        :type interval: float
        :param offset: the start offset. This helps to distribute repeating
            callbacks evenly over time manually to prevent a high number
            of callbacks in a single frame.
        :type offset: float
        :param args: arguments of the scheduled function
        :type args: list
        :param kwargs: keyword arguments of the scheduled function
        :type kwargs: dict

        :returns id: the schedule id, use it to stop the schedule
        """
        assert interval >= 0
        self._next_id += 1
        _id = self._next_id
        next_time = self.current_time + interval + offset
        self._sync.append((self._ADD, (next_time, _id, _id, func, args, kwargs)))
        logger.debug("!!! id {0} new schedule entry: {2} | {1}", _id, self, func)
        return _id

    # noinspection PyUnusedLocal
    def update(self, delta_time, sim_time, *args):
        """
        Call this every frame. This will call any callbacks which time has passed.

        :param delta_time: delta time passed, can be whatever time unit is used
            (even a frame count)
        :type delta_time: float, int
        :param sim_time: The simulation time.
        """
        if self.current_time >= sim_time and self.current_time > 0:
            raise Exception("time cannot go backwards, current time {0} >= {1}".format(self.current_time, sim_time))

        self.current_time = sim_time
        assert self.current_time < sys.maxsize  # can this ever happened?
        heap = self._heap

        # sync
        while self._sync:
            cmd, payload = self._sync.pop(0)
            if cmd == self._ADD:
                heappush(self._heap, payload)
                if payload[0] < self._next_time:
                    self._next_time = payload[0]
            elif cmd == self._REMOVE:
                for entry in self._heap:
                    next_time, sc, entry_id, entry_func, args, kwargs = entry
                    if entry_id == payload:
                        self._heap.remove(entry)
                        heapq.heapify(self._heap)  # make sure that the heap invariants are still valid
                        self._next_time = self._heap[0][0] if self._heap else sys.maxsize  # else is the break part
                        break
            else:
                raise Exception("unknown command " + cmd)


        # process passed callbacks
        next_time = self._next_time
        while next_time <= self.current_time:
            next_time, schedule_id, _id, func, args, kwargs = heappop(heap)

            interval = func(*args, **kwargs)
            # logger.debug("!!! id {1} calling func: {0} (intern: {2}) | {3})", func, _id, schedule_id, self)

            assert interval is not None, "Scheduled method '{0}' should return interval!".format(func.__name__)

            if interval > 0.0:
                # logger.debug("!!! id {1} rescheduling func: {0} (intern: {2}) | {3})", func, _id, self._next_id, self)
                heappush(heap, (next_time + interval, self._next_id, _id, func, args, kwargs))
                self._next_id += 1
            # else:
            #     logger.debug("!!! id {1} un-scheduled func: {0} (intern: {2}) | {3})", func, _id, self._next_id, self)

            next_time = heap[0][0] if heap else sys.maxsize  # else is the break part

        self._next_time = next_time
        # logger.debug("<-heap: {0}", self._heap)

    def clear(self):
        """
        removes all callbacks.
        """
        self._heap[:] = []
        self._next_time = sys.maxsize
        self._sync[:] = []

    def remove(self, schedule_id):
        """
        Remove a callback function.

        :param schedule_id: the id of the function to remove, does nothing if id is not
            in schedule
        :type schedule_id: int
        """
        logger.debug("remove scheduleId {1} from heap: {0}", self._heap, schedule_id)
        self._sync.append((self._REMOVE, schedule_id))


class Timer(object):
    """
    Timer is a convenience class to use a :class:`Scheduler` or one of its
    derived classes. By default it uses a :class:`Scheduler`. If
    another scheduler should be used, assign another instance of a
    Scheduler to Timer.scheduler *before* **any** use of the Timer class.

    Once a frame you need to call the schedulers update method:

    ::

        # once a frame in your mainloop
        Timer.scheduler.update(delta_time)

    You could assign different schedulers to instances of the Timer class
    by overriding the class attribute scheduler with an instance attribute

    ::

        my_scheduler = Scheduler()
        timer = Timer(10)
        timer.scheduler = my_scheduler

    Don't forget to update the used scheduler once per frame.

    The timer class has an event_elapsed attribute which is a
    :py:class:`pyknic.events.Signal` instance. It is used like this:

    ::

        # my_callback should have following signature
        def my_callback(self, timer_that_fired):
            pass

        # register my_callback at the timer
        timer = Timer(0.5, False)
        timer.event_elapsed += my_callback



    :classattr scheduler: a Scheduler or a derived class

    :attr interval: the interval to be used
    :attr repeat: repeated firing of the elapsed event

    :param interval: the time to wait (>=0) until the event_elapsed is fired. It needs to be set later if None (default)
    :type interval: float
    :param repeat: defaults to False, if the elapsed event should be fired repeatedly with the given interval
    :type repeat: bool
    :param name: to name th timer so one can distinguish it in the logs
    :type name: string
    :param scheduler: the scheduler to use. If None is passed then the Timer.scheduler is used.
    :type scheduler: Scheduler
    :param logger_to_use: The logger instance to use. If set to None the module loggers instance is used.
    :type logger_to_use: logging.Logger

    """
    # todo: it would be nice to be able to pass in some extra args to be returned in the callback see Explosion in pw28
    def __init__(self, scheduler, interval=None, repeat=False, name=None, logger_to_use=None):
        """
        Constructor.
        """
        self._interval = -1
        if interval is not None:
            self.interval = interval
        self.repeat = repeat
        self._name = str(id(self)) if name is None else name
        self.event_elapsed = events.Signal("Timer" + self._name)
        self._is_running = False
        self._cb_id = -1
        self.logger = logger if logger_to_use is None else logger_to_use
        self.logger.debug("Timer {0} created", self._name)

        if scheduler is None:
            raise Exception("pass in a scheduler!")
        self.scheduler = scheduler

        self._is_in_callback = False
        atexit.register(self._at_exit_stop)

    @property
    def interval(self):
        return self._interval

    @interval.setter
    def interval(self, value):
        self._is_interval_valid(value)
        self._interval = value

    def __del__(self):
        if self._is_running:
            print("Timer should not be running anymore if garbage collected! ", self._name,
                  self.event_elapsed._observers, file=sys.stderr)
            self.scheduler.remove(self._cb_id)

    def start(self, interval=None, repeat=None):
        """
        Starts the timer to fire the elapsed events (internally: it schedules
        a callback method to the scheduler).

        :param interval: defaults to None, overwrites the interval otherwise
        :param repeat: defaults to None, overwrites the repeat attribute if
            set to a boolean value, defaults to False if not set in the constructor
        :type repeat: bool
        """
        if self._is_running is True:
            self.logger.debug("timer '{0}' already started doing nothing", self._name)
            return

        self._is_running = True

        if interval is not None:
            self._interval = interval

        self._is_interval_valid(self._interval)

        if repeat is not None:
            self.repeat = repeat

        if not self._is_in_callback:
            self._cb_id = self.scheduler.schedule(self._callback, self._interval)
        self.logger.debug("timer '{0}' started", self._name)

    @staticmethod
    def _is_interval_valid(interval):
        if interval < 0:
            raise ValueError("Timer interval < 0 is not supported. Set an interval.")

    def _at_exit_stop(self):
        print("atexit stop of timer", self._name)
        self.stop()

    def stop(self):
        """
        Stops the timer from firing the elapsed event (internally: it removes
        all callbacks from the scheduler).
        """
        if self._is_running is False:
            self.logger.debug("timer '{0}' already stopped doing nothing.", self._name)
            return

        self._is_running = False
        self.scheduler.remove(self._cb_id)
        self.logger.debug("timer '{0}' stopped", self._name)

    def _callback(self):
        """
        Callback method that gets called from the scheduler.
        """
        self._is_in_callback = True
        if self._is_running is False:
            self.logger.warning("timer {0} callback called even if it is not running!", self._name)
        self.logger.debug("timer '{0}' callback...", self._name)

        self._is_running = None

        self.event_elapsed.fire(self)

        if self._is_running is None:
            self._is_running = self.repeat

        if self._is_running is True:
            self.logger.debug("timer '{0}' re-scheduling after callback", self._name)
            self._is_in_callback = False
            return self._interval
        else:
            self._is_running = False
            self.logger.debug("timer '{0}' callback done and timer stopped", self._name)
            self._is_in_callback = False
            return 0

        # if you make this line of code reachable then don't forget this: self._is_in_callback = True


class FrameCap(object):
    """
    The FrameCap class. It makes sure, that something isn't updated with a higher fps as the specified max fps.
    This means::

                1       2         3      4         5  6   7          8          9       10
                v       v         v      v         v  v   v          v          v       v
        ----#------------------------#------------------------#------------------------#----> t
            1                        2                        3                        4

    The upper number over the 'v' are the points in time where the update method is called. It might be the internal
    frame rate of the game logic (note: the interval between them varies depending on many factors).

    The points in time represented by '#' are where a update should happen. The interval
    between #n and #(n+1) is according to the max fps it should update. So in this diagram it would update at following
    points in time: v1, v4, v8, v10

    How is to use it::

        def draw_to_screen(fc, dt):
            ...
            pass

        frame_cap = FrameCap()
        frame_cap.max_fps = 50
        frame_cap.event_update += draw_to_screen
        ...
        # main loop
        while running:
            ...
            frame_cap.update(dt)
            ...


    """

    def __init__(self, max_fps=60):
        """
        FrameCap constructor.

        :param max_fps: the max fps it should update.
        """
        self.event_update = events.Signal("FrameCap" + str(id(self)))
        self.max_fps = max_fps
        self._last_time = 0

    @property
    def max_fps(self):
        return 1.0 / self.frame_duration

    @max_fps.setter
    def max_fps(self, value):
        self.frame_duration = 1.0 / value

    # noinspection PyUnusedLocal
    def update(self, dt, sim_time, *args):
        """
        The update method to drive the entire thing.
        :param dt: The delta time. The unit is not so important (but make sure it is consistent through your code.
        :param sim_time: The simulation time.
        :param args: Any additional number of arguments. Its mainly here for the sender of the event signature.
        """
        if (sim_time - self._last_time) >= self.frame_duration:
            self._last_time = sim_time
            self.event_update.fire(dt, sim_time, self)


logger.debug("imported")
